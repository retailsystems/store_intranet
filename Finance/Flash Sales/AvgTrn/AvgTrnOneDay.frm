VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'********************************************************************************
' AvgTrnOneDay
' Description:  This is for Autorun (one day of data)
'
'               Required Microsoft ActiveX Data Objects 2.6 library
'
'               Creates # of Transactions, # of Units and Dept Trans
'               extract file "avgtrn.txt"
'               Text file is "," delimited
'               Data come from POS_TRN_LN
'********************************************************************************
Option Explicit

Private Sub Form_Load()
On Error GoTo ErrorLogging

Dim strUserID As String
Dim strPasswd As String
Dim strConn As String
Dim strProvider As String
Dim sINIFile As String
Dim strAmountLimit As String
Dim cn As ADODB.Connection
Dim strcn1, strSql, strSql1, strSql2, sSpc, strAvgTrnDept1  As String
Dim strAvgTrnMeas1, strAvgTrnMeas2, strAvgTrnMeas3 As String
Dim str_fname, strFname_sls   As String
Dim ConvrtFile As Integer
Dim v_filename As Variant
Dim rs As ADODB.Recordset
Dim strAvgTrnDateFrom As String
Dim strAvgTrnDateTo As String

'read ini and open logs, if necessary
InitializeLogging (App.Path & "\AvgTrnOneDay.ini")

'location of the ini file
sINIFile = App.Path & "\AvgTrnOneDay.ini"

LogTransaction ("Application Started")

strAvgTrnDateFrom = Format(Now - 1, "Medium date")
strAvgTrnDateTo = Format(Now - 1, "Medium date")

'Read the GERS user login name from ini file
strUserID = sGetINI(sINIFile, "DATABASE SETTING", "Username", "?")
If strUserID = "?" Then
    'No username
    strUserID = InputBox$("Enter GERS user login name, please :")
    writeINI sINIFile, "DATABASE SETTING", "USERNAME", strUserID
End If

'Read the GERS password from ini file
strPasswd = sGetINI(sINIFile, "DATABASE SETTING", "PASSWORD", "?")
If strPasswd = "?" Then
    'No password
    strPasswd = InputBox$("Enter GERS user password, please :")
    writeINI sINIFile, "DATABASE SETTING", "PASSWORD", strPasswd
End If

'Read the GERS database connection from ini file
strConn = sGetINI(sINIFile, "DATABASE SETTING", "CONNECTION", "?")
If strConn = "?" Then
    'No DATABASE
    strConn = InputBox$("Enter your database server, please :")
    writeINI sINIFile, "DATABASE SETTING", "CONNECTION", strConn
End If

'Read the GERS database provider from ini file
strProvider = sGetINI(sINIFile, "DATABASE SETTING", "PROVIDER", "?")
If strProvider = "?" Then
    'No PROVIDER
    strProvider = InputBox$("Enter your database provider, please :")
    writeINI sINIFile, "DATABASE SETTING", "PROVIDER", strProvider
End If

'Read the transaction directory location from ini file
strFname_sls = sGetINI(sINIFile, "EXTRACT FILE LOCATION", "TRANS DIR", "?")
If strFname_sls = "?" Then
    'No password
    strFname_sls = InputBox$("Enter Transaction File Location as C: , please :")
    writeINI sINIFile, "EXTRACT FILE LOCATION", "TRANS DIR", strFname_sls
End If

'Read the amount limit from ini file
strAmountLimit = sGetINI(sINIFile, "DATA", "AMOUNT LIMIT", "?")
If strAmountLimit = "?" Then
    'No amount limit
    strAmountLimit = InputBox$("Enter Amount Limit: , please :")
    writeINI sINIFile, "DATA", "AMOUNT LIMIT", strAmountLimit
End If

LogTransaction ("Changing mouse pointer to 11")
Form1.MousePointer = 11
LogTransaction ("Connecting to GERS")
Set cn = New ADODB.Connection

strcn1 = "Provider=" & strProvider & ";Password=" & strPasswd
strcn1 = strcn1 & ";User ID=" & strUserID
strcn1 = strcn1 & ";Data Source=" & strConn
strcn1 = strcn1 & ";Persist Security Info=True "
cn.Open strcn1

LogTransaction ("Connection string: " & strcn1)

'Get stores' trans counts
strSql = " SELECT GR_UTIL.WEEK_NO(trn_dt),   "
strSql = strSql & " GR_UTIL.YEAR_NO(trn_dt), "
strSql = strSql & " TO_CHAR(trn_dt,'Day'),   "
strSql = strSql & " store_cd, COUNT(*) FROM (  "
strSql = strSql & " SELECT DISTINCT P.trn_dt trn_dt, P.store_cd store_cd,       "
strSql = strSql & " P.trn_time trn_time, P.trn_num trn_num, P.term_num term_num "
strSql = strSql & " FROM pos_trn_ln P, gm_itm I, gm_sku S                       "
strSql = strSql & " WHERE P.ln_tp IN ('SAL','RET') AND P.sku_num = S.sku_num    "
strSql = strSql & " AND S.itm_cd = I.itm_cd             "
strSql = strSql & " AND P.void IS NULL AND P.stat_cd = 'V'  "
strSql = strSql & " AND (ABS(P.eff_AMT)/decode(P.qty, 0, 1, P.qty)) < " & strAmountLimit & " "
strSql = strSql & " AND (I.subclass_cd <> '999999' OR S.sku_num = '999990-000') "
strSql = strSql & " AND P.trn_dt BETWEEN '" & strAvgTrnDateFrom
strSql = strSql & "' AND '" & strAvgTrnDateTo & "' )  "
strSql = strSql & " GROUP BY trn_dt, store_cd         "
'Get Units
strSql1 = " SELECT GR_UTIL.WEEK_NO(trn_dt),    "
strSql1 = strSql1 & " GR_UTIL.YEAR_NO(trn_dt), "
strSql1 = strSql1 & " TO_CHAR(trn_dt,'Day'),   "
strSql1 = strSql1 & " I.dept_cd dept_cd,  "
strSql1 = strSql1 & " P.store_cd store_cd, SUM(P.qty)    "
strSql1 = strSql1 & " FROM pos_trn_ln P, gm_itm I, gm_sku S  "
strSql1 = strSql1 & " WHERE P.sku_num = S.sku_num        "
strSql1 = strSql1 & " AND S.itm_cd = I.itm_cd            "
strSql1 = strSql1 & " AND P.ln_tp IN ('SAL','RET')       "
strSql1 = strSql1 & " AND P.void IS NULL AND P.stat_cd = 'V' "
strSql1 = strSql1 & " AND trn_dt BETWEEN '" & strAvgTrnDateFrom
strSql1 = strSql1 & "' AND '" & strAvgTrnDateTo & "'  "
strSql1 = strSql1 & " AND (I.subclass_cd <> '999999' OR S.sku_num = '999990-000') "
strSql1 = strSql1 & " AND (ABS(P.eff_AMT)/decode(P.qty, 0, 1, P.qty)) < " & strAmountLimit & " "
strSql1 = strSql1 & " GROUP BY trn_dt, dept_cd, store_cd  "
'Get stores' and dept trans counts
strSql2 = " SELECT GR_UTIL.WEEK_NO(trn_dt),   "
strSql2 = strSql2 & " GR_UTIL.YEAR_NO(trn_dt), "
strSql2 = strSql2 & " TO_CHAR(trn_dt,'Day'),   "
strSql2 = strSql2 & " dept_cd,                 "
strSql2 = strSql2 & " store_cd, COUNT(*) FROM (  "
strSql2 = strSql2 & " SELECT DISTINCT P.trn_dt trn_dt, P.store_cd store_cd, I.dept_cd dept_cd,"
strSql2 = strSql2 & " P.trn_time trn_time, P.trn_num trn_num, P.term_num term_num "
strSql2 = strSql2 & " FROM pos_trn_ln P, gm_itm I, gm_sku S                       "
strSql2 = strSql2 & " WHERE P.ln_tp IN ('SAL','RET') AND P.sku_num = S.sku_num    "
strSql2 = strSql2 & " AND S.itm_cd = I.itm_cd             "
strSql2 = strSql2 & " AND P.void IS NULL AND P.stat_cd = 'V'  "
strSql2 = strSql2 & " AND (ABS(P.eff_AMT)/decode(P.qty, 0, 1, P.qty)) < " & strAmountLimit & " "
strSql2 = strSql2 & " AND (I.subclass_cd <> '999999' OR S.sku_num = '999990-000') "
strSql2 = strSql2 & " AND P.trn_dt BETWEEN '" & strAvgTrnDateFrom
strSql2 = strSql2 & "' AND '" & strAvgTrnDateTo & "' )  "
strSql2 = strSql2 & " GROUP BY trn_dt, dept_cd, store_cd"


Dim strSpace As String
Dim strAvgTrnWeek As String
Dim strAvgTrnDept As String
Dim strAvgTrnStore As String
Dim strAvgTrnFY As String
Dim strAvgTrnActual As String
Dim strAvgTrnSalesInp As String

sSpc = " " 'a single space
strAvgTrnWeek = "Week "
strAvgTrnDept = "No Dept"
strAvgTrnDept1 = "Dept "
strAvgTrnStore = "Store "
strAvgTrnFY = "FY"
strAvgTrnActual = "Actual"
strAvgTrnMeas1 = "Transactions"
strAvgTrnMeas2 = "Units sold"
strAvgTrnMeas3 = "Dept Trans"

ConvrtFile = FreeFile

LogTransaction ("Opening output file: " & strFname_sls)
Open strFname_sls For Output As #ConvrtFile
LogTransaction ("Executing SQL to extract transaction counts: " & strSql)
Set rs = cn.Execute(strSql)
LogTransaction ("Writing results to file")
While Not rs.EOF
 Print #ConvrtFile, _
   strAvgTrnWeek & rs.Fields(0) & sSpc & Trim(rs.Fields(2)) & "," & strAvgTrnDept; _
   "," & strAvgTrnStore & rs.Fields(3) & "," & strAvgTrnMeas1 & "," & strAvgTrnFY & rs.Fields(1); _
   "," & strAvgTrnActual & "," & rs.Fields(4)
 rs.MoveNext
Wend
LogTransaction ("Executing SQL to extract unit counts: " & strSql1)
Set rs = cn.Execute(strSql1)
While Not rs.EOF
 Print #ConvrtFile, _
   strAvgTrnWeek & rs.Fields(0) & sSpc & Trim(rs.Fields(2)) & "," & strAvgTrnDept1 & rs.Fields(3); _
   "," & strAvgTrnStore & rs.Fields(4) & "," & strAvgTrnMeas2 & "," & strAvgTrnFY & rs.Fields(1); _
   "," & strAvgTrnActual & "," & rs.Fields(5)
 rs.MoveNext
Wend
Set rs = cn.Execute(strSql2)
While Not rs.EOF
 Print #ConvrtFile, _
   strAvgTrnWeek & rs.Fields(0) & sSpc & Trim(rs.Fields(2)) & "," & strAvgTrnDept1 & rs.Fields(3); _
   "," & strAvgTrnStore & rs.Fields(4) & "," & strAvgTrnMeas3 & "," & strAvgTrnFY & rs.Fields(1); _
   "," & strAvgTrnActual & "," & rs.Fields(5)
 rs.MoveNext
Wend
LogTransaction ("Writing results to file")
LogTransaction ("Closing output file")
Close #ConvrtFile                'close file
'Set rs = Nothing
LogTransaction ("Closing query result set")
rs.Close
LogTransaction ("Closing GERS connection")
cn.Close
LogTransaction ("Returning mouse cursor to default")
Form1.MousePointer = Default

'close form
LogTransaction ("Unloading form")
EndLogging
Unload Form1

Exit Sub

'Error Handling
ErrorLogging:
    LogError (Err.Number & " " & Err.Description)
    LogTransaction (Err.Number & " " & Err.Description)
    EndLogging
    Unload Me
    Err.Raise Err.Number, Err.Source, Err.Description, Err.HelpFile, Err.HelpContext 'create runtime error
End Sub

