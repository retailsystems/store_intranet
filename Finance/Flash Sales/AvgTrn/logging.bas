Attribute VB_Name = "Module2"
'Global Variables
Dim LogErrorFlag As Boolean
Dim LogTransactionFlag As Boolean
Dim ErrorFilename As String
Dim TransactionFilename As String
Dim errorlogfile As Integer
Dim transactionlogfile As Integer

Public Sub LogError(message As String)
    'Write error to Windows Event Log
    App.LogEvent message, vbLogEventTypeError
    'If Error Logging is On, write error to Log file
    If LogErrorFlag Then
        Print #errorlogfile, Now & " (" & App.EXEName & ") " & message
    End If
End Sub

Public Sub LogTransaction(message As String)
    'If Transaction Logging is On, write action to Log file
    If LogTransactionFlag Then
        Print #transactionlogfile, Now & " (" & App.EXEName & ") " & message
    End If
End Sub

Public Sub InitializeLogging(inifilename As String)
    'Get flags and filenames from ini file
    LogErrorFlag = sGetINI(inifilename, "Logging", "ErrorLogging", False)
    LogTransactionFlag = sGetINI(inifilename, "Logging", "TransactionLogging", False)
    ErrorFilename = sGetINI(inifilename, "Logging", "ErrorLogFile", "c:\" & App.EXEName & "_errorlog.log")
    TransactionFilename = sGetINI(inifilename, "Logging", "TransactionLogFile", "c:\" & App.EXEName & "_transactionlog.log")

    'If Error Logging is ON, Open Error File; Creates files if it doesn't exist
    If LogErrorFlag Then
        errorlogfile = FreeFile
        Open ErrorFilename For Append As #errorlogfile
    End If

    'If Transaction Logging is ON, Open Transaction File; Creates files if it doesn't exist
    If LogTransactionFlag Then
        transactionlogfile = FreeFile
        Open TransactionFilename For Append As #transactionlogfile
    End If
End Sub

Public Sub EndLogging()
    'If Error Logging is ON, Close Error File
    If LogErrorFlag Then
        Close #errorlogfile
    End If
    'If Transaction Logging is ON, Close Transaction File
    If LogTransactionFlag Then
        Close #transactionlogfile
    End If
End Sub
