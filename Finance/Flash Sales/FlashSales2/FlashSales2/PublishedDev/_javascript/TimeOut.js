/*
Auth: Sri Bajjuri
Date: 10/07/2003
Desc: Javascript Timer function.
	Set the min,sec initial values in timeIt() function
	call timeIt() function to initiate the timer
	
*/
// Client will be redirected to this URL after timeout
var timedouturl = "logout.aspx";

function Display(min, sec) {
var disp;
if (min <= 9) disp = " 0";
else disp = " ";
disp += min + ":";
if (sec <= 9) disp += "0" + sec;
else disp += sec; 

return (disp);
}
function Down() { 
sec--;      
if (sec == -1) { sec = 59; min--; }

if(min <= 10 ){
	window.status = "Session will time out in: " + Display(min, sec);
}
if (min == 0 && sec == 0) {
//window.top.close();	
window.top.location = timedouturl;

}
else down = setTimeout("Down()", 1000);
}
function timeIt() {
//Timer setings
min = 10;
sec = 0;
Down();
}

//window.onload = timeIt;




