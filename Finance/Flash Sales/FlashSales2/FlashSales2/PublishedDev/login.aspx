<%@ Page Language="vb" AutoEventWireup="false" Codebehind="login.aspx.vb" Inherits="FlashSales2.login"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>login</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<link rel="stylesheet" type="text/css" href="_css/styles.css">
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table width="100%" height="100%">
				<tr>
					<td valign="center">
						<table width="318" cellpadding="0" cellspacing="0" style="BORDER-RIGHT: #3366ff 3px solid; BORDER-TOP: #3366ff 3px solid; BORDER-LEFT: #3366ff 3px solid; BORDER-BOTTOM: #3366ff 3px solid" align="center">
							<tr bgcolor="#3366ff">
								<td colspan="4" height="25" class="pageCaptionWhite">
									Flash Sales Login
								</td>
							</tr>
							<tr>
								<td colspan="4">
									<img src="_images/login_top.jpg">
								</td>
							</tr>
							<tr>
								<td colspan="4" align="middle">
									<asp:Label Runat="server" ID="winApiErr" Visible="False" CssClass="ErrStyle"></asp:Label><asp:Label ID="lblErr" Runat="server" CssClass="errStyle"></asp:Label>
								</td>
							</tr>
							<tr>
								<td colspan="4" height="5"></td>
							</tr>
							<tr>
								<td width="35"></td>
								<td class="cellvaluecaption" width="90">Domain:&nbsp;<font color="red"><sup>*</sup></font></td>
								<td width="168"><asp:TextBox ID="txtDomain" Runat="server" CssClass="cellvalueleft" text="sunny" Width="150px"></asp:TextBox>
									<asp:RequiredFieldValidator id="RequiredFieldValidator1" runat="server" CssClass="ErrStyle" ErrorMessage="Domain name is required." Display="Dynamic" ControlToValidate="txtDomain"></asp:RequiredFieldValidator></td>
								<td width="25"></td>
							</tr>
							<tr>
								<td></td>
								<td class="cellvaluecaption">User Name:&nbsp;<font color="red"><sup>*</sup></font></td>
								<td width="168"><asp:TextBox ID="txtUserName" Runat="server" CssClass="cellvalueleft" Width="150px"></asp:TextBox>
									<asp:RequiredFieldValidator id="Requiredfieldvalidator2" runat="server" CssClass="ErrStyle" ErrorMessage="User name is required." Display="Dynamic" ControlToValidate="txtUserName"></asp:RequiredFieldValidator></td>
								<td></td>
							</tr>
							<tr>
								<td></td>
								<td class="cellvaluecaption">Password:&nbsp;<font color="red"><sup>*</sup></font></td>
								<td width="168"><asp:TextBox ID="txtPwd" Runat="server" TextMode="Password" CssClass="cellvalueleft" Width="150px"></asp:TextBox>
									<asp:RequiredFieldValidator id="Requiredfieldvalidator3" runat="server" CssClass="ErrStyle" ErrorMessage="Password is required." Display="Dynamic" ControlToValidate="txtPwd"></asp:RequiredFieldValidator></td>
								<td></td>
							</tr>
							<tr>
								<td colspan="4" height="10"></td>
							</tr>
							<tr>
								<td colspan="4" align="middle">
									<asp:Button Runat="server" ID="btnSubmit" Text="Submit" CssClass="btnSmallGrey"></asp:Button>
								</td>
							</tr>
							<tr>
								<td colspan="4" height="30"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
