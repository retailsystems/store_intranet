

Imports FlashSales2.LogonUser

Public Class login
    Inherits System.Web.UI.Page
    Protected WithEvents txtDomain As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtUserName As System.Web.UI.WebControls.TextBox
    Protected WithEvents txtPwd As System.Web.UI.WebControls.TextBox
    Protected WithEvents btnSubmit As System.Web.UI.WebControls.Button
    Protected WithEvents lblErr As System.Web.UI.WebControls.Label
    Protected WithEvents RequiredFieldValidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents Requiredfieldvalidator3 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents winApiErr As System.Web.UI.WebControls.Label
    Protected WithEvents Requiredfieldvalidator2 As System.Web.UI.WebControls.RequiredFieldValidator

    Dim token As IntPtr
    Dim isInRole As Boolean = False

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        lblErr.Text = ""
        winApiErr.Text = ""

        '***************************************************************************
        ' change the .net machine.config processmodel attribute to userName="SYSTEM"
        '***************************************************************************
    End Sub
    Private Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Try
            '***** New code ***********
            Dim LogonUserObj As New LogonUser
            If LogonUserObj.impersonateValidUser(txtUserName.Text, txtDomain.Text, txtPwd.Text) Then
                'LogonUserObj.impersonateValidUser(txtUserName.Text, txtDomain.Text, txtPwd.Text)
                ' Working code here
                Dim MyIdentity As System.Security.Principal.WindowsIdentity = System.Security.Principal.WindowsIdentity.GetCurrent
                Dim MyPrincipal As System.Security.Principal.WindowsPrincipal = New System.Security.Principal.WindowsPrincipal(MyIdentity)
                'Response.Write(MyIdentity.Name & "<br>")

                Dim roleArr() As String = Split(Application("UserRoles"), ",")

                Dim i As Integer = 0
                For i = 0 To roleArr.Length - 1
                    If roleArr(i) = "*" Then
                        isInRole = True
                        Exit For
                    Else
                        'Response.Write(MyPrincipal.IsInRole(System.Security.Principal.WindowsBuiltInRole.Administrator))
                        'Response.Write(roleArr(i).ToString & "-" & MyPrincipal.IsInRole(roleArr(i).ToString) & "<br>")
                        If MyPrincipal.IsInRole(roleArr(i).ToString) Then
                            isInRole = True
                            Exit For
                        End If
                    End If
                Next
                'Response.Write(MyPrincipal.IsInRole(System.Security.Principal.WindowsBuiltInRole.User))
                LogonUserObj.undoImpersonation()
                LogonUserObj = Nothing

                If isInRole Then
                    System.Web.Security.FormsAuthentication.RedirectFromLoginPage(txtUserName.Text, False)
                    If IsShUser(txtUserName.Text) Then
                        Response.Redirect("Index_SH.aspx")
                    Else
                        Response.Redirect("Index.aspx")
                    End If
                Else
                    'lblErr.Text = "You have no authorization to access this application."
                    lblErr.Text &= "If you typed an incorrect User Name or Password please try again <br><br> If they are correct your Flash account has been disabled due to lack of use. Please contact your supervisor to be reactivated."
                End If
                'Response.Redirect("Index.aspx")
            Else
                'lblErr.Text &= "<br>" & txtDomain.Text & "\" & txtUserName.Text & "\" & txtPwd.Text & "<br>"
                lblErr.Text &= "Invalid userName/Password"
                'lblErr.Text &= "If you typed an incorrect User Name or Password please try again <br><br> If they are correct your Flash account has been disabled due to lack of use. Please contact your supervisor to be reactivated."
            End If
            'If ValidateLogin(txtUserName.Text, txtPwd.Text, txtDomain.Text) Then
            '    System.Web.Security.FormsAuthentication.RedirectFromLoginPage(txtUserName.Text, False)
            '    Response.Redirect("Index.aspx")
            'Else
            '    'lblErr.Text &= "<br>" & txtDomain.Text & "\" & txtUserName.Text & "\" & txtPwd.Text & "<br>"
            '    lblErr.Text &= "Invalid userName/Password"
            'End If
        Catch ex As Exception
            lblErr.Text = ex.ToString
        End Try
    End Sub
    Private Function IsShUser(ByVal userName As String) As Boolean
        Dim userFound As Boolean = False
        Dim shUserArr() As String = Split(Application("SHUserList"), ",")
        If shUserArr.Length > 0 Then
            Dim i As Integer = 0
            For i = 0 To shUserArr.Length - 1
                If (shUserArr(i).ToString.ToUpper.Equals(userName.ToUpper)) Then
                    userFound = True
                    Exit For
                End If
            Next
        End If
        Return userFound
    End Function





End Class
