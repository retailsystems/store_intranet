
Imports System.Web.Security
Imports System.Security.Principal
Imports System.Runtime.InteropServices
Public Class LogonUser
    Dim LOGON32_LOGON_INTERACTIVE As Integer = 2
    Dim LOGON32_LOGON_NETWORK As Integer = 3
    Dim LOGON32_PROVIDER_DEFAULT As Integer = 0
    Public impersonationContext As WindowsImpersonationContext
    Dim originalUserIdentity As WindowsIdentity

    Declare Auto Function RevertToSelf Lib "advapi32.dll" () As Integer

    Declare Auto Function LogonUser Lib "advapi32.dll" (ByVal lpszUsername As String, _
        ByVal lpszDomain As String, _
        ByVal lpszPassword As String, _
        ByVal dwLogonType As Integer, _
        ByVal dwLogonProvider As Integer, _
        ByRef phToken As IntPtr) As Integer
    Declare Auto Function DuplicateToken Lib "advapi32.dll" _
        (ByVal ExistingTokenHandle As IntPtr, _
        ByVal ImpersonationLevel As Integer, _
        ByRef DuplicateTokenHandle As IntPtr) As Integer

    Public Sub New()
    End Sub

    Function impersonateValidUser(ByVal userName As String, ByVal domain As String, _
        ByVal password As String) As Boolean

        Dim tempWindowsIdentity As WindowsIdentity
        Dim token As IntPtr
        Dim tokenDuplicate As IntPtr

        ' Since this site runs using impersonation of the user (<identity impersonate="true"/>
        ' in the Web.config file), we need to store the user's identity so when we're done
        ' switching identities around we can eventually revert back to the *user's* identity.
        ' Just calling impersonationContext.Undo() would only get us back to the SYSTEM account!
        originalUserIdentity = WindowsIdentity.GetCurrent()

        ' Get back to the SYSTEM account so we have authority to impersonate a user
        If RevertToSelf() Then
            ' tempid2 is for debug purposes - you can examine that  you() 're back to SYSTEM acct
            Dim tempid2 As WindowsIdentity = WindowsIdentity.GetCurrent()

            ' i=1 if success
            Dim i As Integer = LogonUser(userName, domain, password, System.Web.HttpContext.Current.Application("LOGON32_LOGON"), _
                       System.Web.HttpContext.Current.Application("LOGON32_PROVIDER"), token)
            'If i = 1 Then
            '    impersonateValidUser = True
            'Else
            '    impersonateValidUser = False
            'End If
            'Duplicate the token and impersonate
            If DuplicateToken(token, 2, tokenDuplicate) <> 0 Then
                tempWindowsIdentity = New WindowsIdentity(tokenDuplicate)
                impersonationContext = tempWindowsIdentity.Impersonate()
                If impersonationContext Is Nothing Then
                    impersonateValidUser = False
                Else
                    impersonateValidUser = True
                End If
            Else
                impersonateValidUser = False
            End If
        End If

    End Function

    Function undoImpersonation()
        ' Back to SYSTEM account
        impersonationContext.Undo()
        ' Back to user's account
        originalUserIdentity.Impersonate()
    End Function

End Class
