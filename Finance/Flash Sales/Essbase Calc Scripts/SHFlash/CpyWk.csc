SET UPDATECALC OFF;

/* Copy NOV */
DATACOPY FY2006->"Week 41 Sunday" TO BkUp2006->"Week 41 Sunday"; 
DATACOPY FY2006->"Week 41 Monday" TO BkUp2006->"Week 41 Monday"; 
DATACOPY FY2006->"Week 41 Tuesday" TO BkUp2006->"Week 41 Tuesday"; 
DATACOPY FY2006->"Week 41 Wednesday" TO BkUp2006->"Week 41 Wednesday"; 
DATACOPY FY2006->"Week 41 Thursday" TO BkUp2006->"Week 41 Thursday"; 
DATACOPY FY2006->"Week 41 Friday" TO BkUp2006->"Week 41 Friday"; 
DATACOPY FY2006->"Week 41 Saturday" TO BkUp2006->"Week 41 Saturday";

DATACOPY FY2006->"Week 42 Sunday" TO BkUp2006->"Week 42 Sunday"; 
DATACOPY FY2006->"Week 42 Monday" TO BkUp2006->"Week 42 Monday"; 
DATACOPY FY2006->"Week 42 Tuesday" TO BkUp2006->"Week 42 Tuesday"; 
DATACOPY FY2006->"Week 42 Wednesday" TO BkUp2006->"Week 42 Wednesday"; 
DATACOPY FY2006->"Week 42 Thursday" TO BkUp2006->"Week 42 Thursday"; 
DATACOPY FY2006->"Week 42 Friday" TO BkUp2006->"Week 42 Friday"; 
DATACOPY FY2006->"Week 42 Saturday" TO BkUp2006->"Week 42 Saturday"; 

DATACOPY FY2006->"Week 43 Sunday" TO BkUp2006->"Week 43 Sunday"; 
DATACOPY FY2006->"Week 43 Monday" TO BkUp2006->"Week 43 Monday"; 
DATACOPY FY2006->"Week 43 Tuesday" TO BkUp2006->"Week 43 Tuesday"; 
DATACOPY FY2006->"Week 43 Wednesday" TO BkUp2006->"Week 43 Wednesday"; 
DATACOPY FY2006->"Week 43 Thursday" TO BkUp2006->"Week 43 Thursday"; 
DATACOPY FY2006->"Week 43 Friday" TO BkUp2006->"Week 43 Friday"; 
DATACOPY FY2006->"Week 43 Saturday" TO BkUp2006->"Week 43 Saturday"; 

/* Copy DEC. */
DATACOPY FY2006->"Week 44 Sunday" TO BkUp2006->"Week 44 Sunday"; 
DATACOPY FY2006->"Week 44 Monday" TO BkUp2006->"Week 44 Monday"; 
DATACOPY FY2006->"Week 44 Tuesday" TO BkUp2006->"Week 44 Tuesday"; 
DATACOPY FY2006->"Week 44 Wednesday" TO BkUp2006->"Week 44 Wednesday"; 
DATACOPY FY2006->"Week 44 Thursday" TO BkUp2006->"Week 44 Thursday"; 
DATACOPY FY2006->"Week 44 Friday" TO BkUp2006->"Week 44 Friday"; 
DATACOPY FY2006->"Week 44 Saturday" TO BkUp2006->"Week 44 Saturday"; 

DATACOPY FY2006->"Week 45 Sunday" TO BkUp2006->"Week 45 Sunday"; 
DATACOPY FY2006->"Week 45 Monday" TO BkUp2006->"Week 45 Monday"; 
DATACOPY FY2006->"Week 45 Tuesday" TO BkUp2006->"Week 45 Tuesday"; 
DATACOPY FY2006->"Week 45 Wednesday" TO BkUp2006->"Week 45 Wednesday"; 
DATACOPY FY2006->"Week 45 Thursday" TO BkUp2006->"Week 45 Thursday"; 
DATACOPY FY2006->"Week 45 Friday" TO BkUp2006->"Week 45 Friday"; 
DATACOPY FY2006->"Week 45 Saturday" TO BkUp2006->"Week 45 Saturday"; 

DATACOPY FY2006->"Week 46 Sunday" TO BkUp2006->"Week 46 Sunday"; 
DATACOPY FY2006->"Week 46 Monday" TO BkUp2006->"Week 46 Monday"; 
DATACOPY FY2006->"Week 46 Tuesday" TO BkUp2006->"Week 46 Tuesday"; 
DATACOPY FY2006->"Week 46 Wednesday" TO BkUp2006->"Week 46 Wednesday"; 
DATACOPY FY2006->"Week 46 Thursday" TO BkUp2006->"Week 46 Thursday"; 
DATACOPY FY2006->"Week 46 Friday" TO BkUp2006->"Week 46 Friday"; 
DATACOPY FY2006->"Week 46 Saturday" TO BkUp2006->"Week 46 Saturday"; 

DATACOPY FY2006->"Week 47 Sunday" TO BkUp2006->"Week 47 Sunday"; 
DATACOPY FY2006->"Week 47 Monday" TO BkUp2006->"Week 47 Monday"; 
DATACOPY FY2006->"Week 47 Tuesday" TO BkUp2006->"Week 47 Tuesday"; 
DATACOPY FY2006->"Week 47 Wednesday" TO BkUp2006->"Week 47 Wednesday"; 
DATACOPY FY2006->"Week 47 Thursday" TO BkUp2006->"Week 47 Thursday"; 
DATACOPY FY2006->"Week 47 Friday" TO BkUp2006->"Week 47 Friday"; 
DATACOPY FY2006->"Week 47 Saturday" TO BkUp2006->"Week 47 Saturday"; 

DATACOPY FY2006->"Week 48 Sunday" TO BkUp2006->"Week 48 Sunday"; 
DATACOPY FY2006->"Week 48 Monday" TO BkUp2006->"Week 48 Monday"; 
DATACOPY FY2006->"Week 48 Tuesday" TO BkUp2006->"Week 48 Tuesday"; 
DATACOPY FY2006->"Week 48 Wednesday" TO BkUp2006->"Week 48 Wednesday"; 
DATACOPY FY2006->"Week 48 Thursday" TO BkUp2006->"Week 48 Thursday"; 
DATACOPY FY2006->"Week 48 Friday" TO BkUp2006->"Week 48 Friday"; 
DATACOPY FY2006->"Week 48 Saturday" TO BkUp2006->"Week 48 Saturday"; 

/* Copy JAN. */
DATACOPY FY2006->"Week 49 Sunday" TO BkUp2006->"Week 49 Sunday"; 
DATACOPY FY2006->"Week 49 Monday" TO BkUp2006->"Week 49 Monday"; 
DATACOPY FY2006->"Week 49 Tuesday" TO BkUp2006->"Week 49 Tuesday"; 
DATACOPY FY2006->"Week 49 Wednesday" TO BkUp2006->"Week 49 Wednesday"; 
DATACOPY FY2006->"Week 49 Thursday" TO BkUp2006->"Week 49 Thursday"; 
DATACOPY FY2006->"Week 49 Friday" TO BkUp2006->"Week 49 Friday"; 
DATACOPY FY2006->"Week 49 Saturday" TO BkUp2006->"Week 49 Saturday"; 

DATACOPY FY2006->"Week 50 Sunday" TO BkUp2006->"Week 50 Sunday"; 
DATACOPY FY2006->"Week 50 Monday" TO BkUp2006->"Week 50 Monday"; 
DATACOPY FY2006->"Week 50 Tuesday" TO BkUp2006->"Week 50 Tuesday"; 
DATACOPY FY2006->"Week 50 Wednesday" TO BkUp2006->"Week 50 Wednesday"; 
DATACOPY FY2006->"Week 50 Thursday" TO BkUp2006->"Week 50 Thursday"; 
DATACOPY FY2006->"Week 50 Friday" TO BkUp2006->"Week 50 Friday"; 
DATACOPY FY2006->"Week 50 Saturday" TO BkUp2006->"Week 50 Saturday"; 

DATACOPY FY2006->"Week 51 Sunday" TO BkUp2006->"Week 51 Sunday"; 
DATACOPY FY2006->"Week 51 Monday" TO BkUp2006->"Week 51 Monday"; 
DATACOPY FY2006->"Week 51 Tuesday" TO BkUp2006->"Week 51 Tuesday"; 
DATACOPY FY2006->"Week 51 Wednesday" TO BkUp2006->"Week 51 Wednesday"; 
DATACOPY FY2006->"Week 51 Thursday" TO BkUp2006->"Week 51 Thursday"; 
DATACOPY FY2006->"Week 51 Friday" TO BkUp2006->"Week 51 Friday"; 
DATACOPY FY2006->"Week 51 Saturday" TO BkUp2006->"Week 51 Saturday";

DATACOPY FY2006->"Week 52 Sunday" TO BkUp2006->"Week 52 Sunday"; 
DATACOPY FY2006->"Week 52 Monday" TO BkUp2006->"Week 52 Monday"; 
DATACOPY FY2006->"Week 52 Tuesday" TO BkUp2006->"Week 52 Tuesday"; 
DATACOPY FY2006->"Week 52 Wednesday" TO BkUp2006->"Week 52 Wednesday"; 
DATACOPY FY2006->"Week 52 Thursday" TO BkUp2006->"Week 52 Thursday"; 
DATACOPY FY2006->"Week 52 Friday" TO BkUp2006->"Week 52 Friday"; 
DATACOPY FY2006->"Week 52 Saturday" TO BkUp2006->"Week 52 Saturday"; 

