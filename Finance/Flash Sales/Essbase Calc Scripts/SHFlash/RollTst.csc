SET UPDATECALC OFF;
SET AGGMISSG OFF;

/*  Rolls-up only the last processed day &CurrDay */

/*  Calculate Comp TY & COMP LY for all the days in the Current Week - Net Sales has not been calc'd so add the components  */
FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "Week 46 Monday":"Week 46 Monday", "Current Year")
	"Comp TY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp TY" = "Gross Sales" + Returns;			
			"Comp LY" = "Gross Sales"->"Last Year" + Returns->"Last Year";
		Else						
			"Comp TY" = 0;					
			"Comp LY" = 0;					
		ENDIF)					
	"Plan Comp TY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp TY" = "Plan Sales";	
			"Plan Comp LY" = "Plan Sales"->"Last Year";		
		Else						
			"Plan Comp TY" = 0;					
			"Plan Comp LY" = 0;		
		ENDIF)			
ENDFIX;

/* Blank the Torrid Internet sales out of the Hot Topic Internet store  */
FIX("Week 46 Monday":"Week 46 Monday", "Current Year", @ICHILDREN("Division 5"))
	"Store 4490" = #missing;
ENDFIX;

/* Step 1: Roll-up Depts & Stores at day level  */
FIX(Returns, "Gross Sales", Transactions, "Units Sold", "Comp TY", "Plan Comp TY", "Comp LY", "Plan Comp LY", "Comp Status", "Week 46 Monday":"Week 46 Monday", "Current Year")
	CALC DIM(Dept, Market);
ENDFIX;

/* Step 2: Calculate Net Sales at day level at all levels of Dept & Market  */
FIX(@IDESCENDANTS(Dept), @IDESCENDANTS(Market), "Week 46 Monday":"Week 46 Monday", "Current Year")
	"Net Sales";
ENDFIX;

/* Step 3: Roll-up Market for Dept Trans Count*/
FIX("Current Year", "Dept Trans", "Week 46 Monday":"Week 46 Monday")
 CALC DIM(Market);
ENDFIX;

/*  Step 4: Roll-up affected time levels for all levels of Dept & Market  */
FIX("Current Year","Dept Trans", Returns, "Gross Sales", "Net Sales", Transactions, "Units Sold", "Comp TY", "Plan Comp TY", "Comp LY", "Plan Comp LY", "Comp Status", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"December Week 3":"December Week 3";
	"December":"December";
	"Qtr4":"Qtr4";
	"Total Year";
ENDFIX