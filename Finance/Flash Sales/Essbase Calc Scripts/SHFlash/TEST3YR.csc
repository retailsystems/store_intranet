SET UPDATECALC OFF;
SET AGGMISSG OFF;


/*  Step 1:  Roll-up Depts & Stores at day level  */
FIX("3 Years Ago", Transactions, "Units Sold",  @IDESCENDANTS(Qtr4))
	CALC DIM(Dept, Market);
ENDFIX;


/*  Step 3:  Roll-up affected time levels for all levels of Dept & Market  */
FIX("3 Years Ago", Transactions, "Units Sold", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	qtr4;
	"Total Year";
ENDFIX;
