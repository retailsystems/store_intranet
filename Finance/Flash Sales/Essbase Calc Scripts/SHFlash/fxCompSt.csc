SET UPDATECALC OFF;
SET AGGMISSG OFF;

/*  Rolls-up only the last processed day &CurrDay */

FIX("Current Year", "Comp Status", "Week 44 Sunday":"Week 48 Saturday")
	CALC DIM(Dept, Market);
ENDFIX;

FIX("Current Year", "Comp Status", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"December Week 1":"December Week 5";
	December;
	Qtr4;
	"Total Year";
ENDFIX;


