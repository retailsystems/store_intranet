SET UPDATECALC OFF;
SET AGGMISSG OFF;


FIX("Last Year", Returns, "Gross Sales", "Week 21 Monday":"Week 23 Friday")
	CALC DIM(Dept, Market);
ENDFIX;

FIX("Last Year", @IDESCENDANTS(Dept), @IDESCENDANTS(Market), "Week 21 Monday":"Week 23 Friday")
	"Net Sales";
ENDFIX;

/*  Step 3:  Roll-up affected time levels for all levels of Dept & Market  */
FIX("Last Year", Returns, "Gross Sales", "Net Sales", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"June Week 4":"July Week 1";
	June:July;
	qtr2;
	"Total Year";
ENDFIX;
