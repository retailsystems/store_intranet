SET UPDATECALC OFF;
SET AGGMISSG OFF;


/*  Step 2:  Calculate Net Sales at day level at all levels of Dept & Market  */
FIX("2 Years Ago", @IDESCENDANTS(Dept), @IDESCENDANTS(Market), "Week 1 Sunday":"Week 52 Saturday")
	"Net Sales";
ENDFIX;

/*  Step 3:  Roll-up affected time levels for all levels of Dept & Market  */
FIX("2 Years Ago", Returns, "Gross Sales", "Net Sales", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"February Week 1":"January Week 5";
	February:January;
	Qtr1:Qtr4;
	"Total Year";
ENDFIX;

/*  Run this after the Current Year has been changed to the NEW YEAR  */
FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "Week 1 Sunday":"Week 52 Saturday", "Last Year")
	"Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp LY" = "Gross Sales"->"2 years ago" + Returns->"2 years ago";
		Else						
			"Comp LY" = 0;					
		ENDIF)					
	"Plan Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp LY" = "Plan Sales"->"2 years ago";		
		Else						
			"Plan Comp LY" = 0;		
		ENDIF)			
ENDFIX;


FIX("Last Year", "Comp LY", "Plan Comp LY", "Week 1 Sunday":"Week 52 Saturday")
	CALC DIM(Dept, Market);
ENDFIX;

FIX("Last Year", "Comp LY", "Plan Comp LY", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"February Week 1":"January Week 5";
	February:January;
	Qtr1:Qtr4;
	"Total Year";
ENDFIX;
