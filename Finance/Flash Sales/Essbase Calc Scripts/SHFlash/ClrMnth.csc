SET UPDATECALC OFF;

/*  Step 1:  Clear Sales, Unit Sold and Transactions by Department Data*/
FIX("FY2010", @IDESCENDANTS("Dept"), @IDESCENDANTS("All Stores"), "Week 1 Sunday":"Week 10 Saturday")
	CLEARDATA "Gross Sales";
	CLEARDATA "Returns";
	CLEARDATA "Units Sold";
	CLEARDATA "Dept Trans";
ENDFIX;

/*  Step 2:  Clear Transactions*/
FIX("FY2010", "No Div", @IDESCENDANTS("All Stores"), "Week 1 Sunday":"Week 10 Saturday")
	CLEARDATA "Transactions";
ENDFIX;
