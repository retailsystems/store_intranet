SET UPDATECALC OFF;

/*  18m to run this calc script (after a restructured DB)  */

FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "Week 16 Sunday":"Week 26 Saturday", "Current Year")
	"Comp TY"					
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp TY" = "Net Sales";			
			"Comp LY" = "Net Sales"->"Last Year";
		Else						
			"Comp TY" = 0;					
			"Comp LY" = 0;					
		ENDIF)					
	"Plan Comp TY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp TY" = "Plan Sales";	
			"Plan Comp LY" = "Plan Sales"->"Last Year";		
		Else						
			"Plan Comp TY" = 0;					
			"Plan Comp LY" = 0;		
		ENDIF)					
ENDFIX;

/* Calculate Planned Payroll Hours for the last 8 weeks only */
/*  The time range is at the day level - however there is only data for all * Hours variables and New Store Status only at each Sunday (i.e. only at week level)  */
FIX ("No Div",  @LEVMBRS(Market,0), "Week 16 Sunday":"Week 26 Saturday", "Current Year")

"Planned Payroll Hours"
	(IF("New Store Status"==1)
		IF (@ISMBR(@CHILDREN("Torrid")))
			370;
		ELSE
			425;
		ENDIF
	ELSEIF ("Planned minimum hours" > 0  AND  "Planned minimum hours" > ("Planned selling hours" + "Planned task hours"))
		"Planned minimum hours";
	ELSE
		("Planned selling hours" + "Planned task hours");
	ENDIF)

	"Company Open Stores" = "Hot Topic Open Stores Input" + "Torrid Open Stores Input";
	"Company Open Stores MTD" = "Hot Topic Opened MTD Input" + "Torrid Opened MTD Input";
	"Company Open Stores YTD" = "Hot Topic Opened YTD Input" + "Torrid Opened YTD Input";
ENDFIX;

/*  Roll-up Depts & Stores at day level  */
FIX("Plan Sales", "Work Hours", "OT Hours", "Training Hours", "Term Hours", "Planned selling hours", "Planned task hours", "Planned minimum hours", "Planned Payroll Hours", 
      "Comp TY", "Comp LY", "Plan Comp TY", "Plan Comp LY", "Comp Status", "New Store Status", 
      "Hot Topic Open Stores Input", "Torrid Open Stores Input", "Hot Topic Opened MTD Input", "Torrid Opened MTD Input", "Hot Topic Opened YTD Input", "Torrid Opened YTD Input",    
      "Company Open Stores", "Company Open Stores MTD", "Company Open Stores YTD", "Week 16 Sunday":"Week 26 Saturday", "Current Year")
	CALC DIM(Dept, Market);
ENDFIX;


/* Roll-up affected time levels for all levels of Dept & Market  */
FIX("Plan Sales", "Work Hours", "OT Hours", "Training Hours", "Term Hours", "Planned selling hours", "Planned task hours", "Planned minimum hours", "Planned Payroll Hours", 
      "Comp TY", "Comp LY", "Plan Comp TY", "Plan Comp LY", "Comp Status", "New Store Status", 
      "Hot Topic Open Stores Input", "Torrid Open Stores Input", "Hot Topic Opened MTD Input", "Torrid Opened MTD Input", "Hot Topic Opened YTD Input", "Torrid Opened YTD Input",    
      "Company Open Stores", "Company Open Stores MTD", "Company Open Stores YTD", @IDESCENDANTS(Dept), @IDESCENDANTS(Market), "Current Year")
	"May Week 3":"July Week 4";
	"May":"July";
	"Qtr2":"Qtr2";
	"Total Year";
ENDFIX;
