SET UPDATECALC OFF;
SET AGGMISSG OFF;

/*  Run this after the Current Year has been changed to the NEW YEAR  */
FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "Week 1 Sunday":"Week 53 Saturday", "Current Year")
	"Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp LY" = "Gross Sales"->"Last Year" + Returns->"Last Year";
		Else						
			"Comp LY" = 0;					
		ENDIF)					
	"Plan Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp LY" = "Plan Sales"->"Last Year";		
		Else						
			"Plan Comp LY" = 0;		
		ENDIF)			
ENDFIX;


FIX("Current Year", "Comp LY", "Plan Comp LY", "Week 4 Sunday":"Week 5 Saturday")
	CALC DIM(Dept, Market);
ENDFIX;

FIX("Current Year", "Comp LY", "Plan Comp LY", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"February Week 1":"January Week 5";
	February:January;
	Qtr1:Qtr4;
	"Total Year";
ENDFIX;
