SET UPDATECALC OFF;
SET AGGMISSG OFF;

/*  This is a 1-time calc to calculate Plan Comp TY & Plan Comp LY and roll-up the entire DB  */

FIX("No Div", @LEVMBRS(Market, 0), @LEVMBRS("Total Year", 0), "Current Year")
	"Plan Comp TY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp TY" = "Plan Sales";	
			"Plan Comp LY" = "Plan Sales"->"Last Year";		
		Else						
			"Plan Comp TY" = 0;					
			"Plan Comp LY" = 0;		
		ENDIF)					
ENDFIX;

/*  This section is instead of a CALC ALL - can't do this because it will wipt out Shrink % Input at Total Year!!!   */

FIX("Gross Sales", Returns, "Plan Sales", "# of Transactions", "# of Units Sold", 
     @DESCENDANTS("Payroll Related"), @DESCENDANTS(Statistics), "New Store Status", "Comp Status", "Hot Topic Open Stores Input", "Torrid Open Stores Input", 
     "Hot Topic Opened MTD Input", "Torrid Opened MTD Input", "Hot Topic Opened YTD Input", "Torrid Opened YTD Input", 
     "Company Open Stores", "Company Open Stores MTD", "Company Open Stores YTD")
	CALC DIM(Dept, Market, "Total Year", "Fiscal Years");
ENDFIX;

