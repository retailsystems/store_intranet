SET UPDATECALC OFF;

/*  Step 1:  Copy Week 1 of 2006 to Week 53 of 2005 */
DATACOPY FY2006->"February Week 1" TO FY2005->"January Week 5";
DATACOPY FY2006->"Week 1 Sunday" TO FY2005->"Week 53 Sunday";
DATACOPY FY2006->"Week 1 Monday" TO FY2005->"Week 53 Monday";
DATACOPY FY2006->"Week 1 Tuesday" TO FY2005->"Week 53 Tuesday";
DATACOPY FY2006->"Week 1 Wednesday" TO FY2005->"Week 53 Wednesday";
DATACOPY FY2006->"Week 1 Thursday" TO FY2005->"Week 53 Thursday";
DATACOPY FY2006->"Week 1 Friday" TO FY2005->"Week 53 Friday";
DATACOPY FY2006->"Week 1 Saturday" TO FY2005->"Week 53 Saturday";