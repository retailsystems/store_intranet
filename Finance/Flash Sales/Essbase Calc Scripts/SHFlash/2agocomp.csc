SET UPDATECALC OFF;
SET AGGMISSG OFF;

/*  Run this after the Current Year has been changed to the NEW YEAR  */
FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "Week 1 Sunday":"Week 53 Saturday", "2 years ago")
	"Comp TY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp TY" = "Gross Sales" + Returns;			
			"Comp LY" = "Gross Sales"->"3 years ago" + Returns->"3 years ago";
		Else						
			"Comp LY" = 0;					
			"Comp TY" = 0;			
		ENDIF)					
	"Plan Comp TY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp TY" = "Plan Sales";	
			"Plan Comp LY" = "Plan Sales"->"3 years ago";		
		Else						
			"Plan Comp TY" = 0;	
			"Plan Comp LY" = 0;		
		ENDIF)			
ENDFIX;


FIX("2 years ago", "Comp TY", "Comp LY", "Plan Comp TY", "Plan Comp LY", "Week 1 Sunday":"Week 53 Saturday")
	CALC DIM(Dept, Market);
ENDFIX;

FIX("2 years ago", "Comp TY", "Comp LY", "Plan Comp TY", "Plan Comp LY", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"February Week 1":"January Week 5";
	February:January;
	Qtr1:Qtr4;
	"Total Year";
ENDFIX;
