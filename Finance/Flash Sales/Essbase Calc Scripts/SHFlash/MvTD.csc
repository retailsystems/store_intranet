SET UPDATECALC OFF;
SET AGGMISSG ON;

/*  This section was added to MOVE Torrid Internet sales to Store 5990 Torrid Internet  */
FIX("BkUp2006", "Week 14 Sunday":"Week 17 Saturday", @ICHILDREN("Division 5"))
	"Store 5990"(
	IF ("Store 4490" <> #missing and "Store 4490" <> 0)	
		"Store 5990" = "Store 4490";
	ENDIF);
ENDFIX;

/* Blank the Torrid Internet sales out of the Hot Topic Internet store  */
FIX("BkUp2006", "Week 14 Sunday":"Week 17 Saturday", @ICHILDREN("Division 5"))
	"Store 4490" = #missing;
ENDFIX;