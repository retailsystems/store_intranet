SET UPDATECALC OFF;
SET AGGMISSG OFF;


/*  Step 1:  Roll-up Depts & Stores at day level  */
FIX("Last Year", Returns, "Gross Sales", "Week 7 Tuesday")
	CALC DIM(Dept, Market);
ENDFIX;

/*  Step 2:  Calculate Net Sales at day level at all levels of Dept & Market  */
FIX("Last Year", @IDESCENDANTS(Dept), @IDESCENDANTS(Market), "Week 7 Tuesday")
	"Net Sales";
ENDFIX;

/*  Step 3:  Roll-up affected time levels for all levels of Dept & Market  */
FIX("Last Year", Returns, "Gross Sales", "Net Sales", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"March Week 3";
	March;
	Qtr1;
	"Total Year";
ENDFIX;