SET UPDATECALC OFF;
SET AGGMISSG OFF;


/*  Step 1:  Roll-up Depts & Stores at day level  */
FIX("2 Years Ago", Transactions, "Units Sold",  "Week 49 Sunday":"Week 49 Saturday")
	CALC DIM(Dept, Market);
ENDFIX;


/*  Step 3:  Roll-up affected time levels for all levels of Dept & Market  */
FIX("2 Years Ago", Transactions, "Units Sold", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"January Week 1";
	January;
	qtr1;
	"Total Year";
ENDFIX;
