SET UPDATECALC OFF;

/*  Step 1:  Clear Sales, Unit Sold and Transactions by Department Data*/
FIX("FY2010", @IDESCENDANTS("Dept"), @IDESCENDANTS("All Stores"), "Week 1 Sunday":"Week 10 Saturday")
	CLEARDATA "Gross Sales";
	CLEARDATA "Returns";
	CLEARDATA "Kiosk Digital Gross Sales";
	CLEARDATA "Kiosk Digital Returns";
	CLEARDATA "Kiosk Non-Digital Gross Sales";
	CLEARDATA "Kiosk Non-Digital Returns";
	CLEARDATA "POS Units Sold";
	CLEARDATA "Kiosk Units Sold";
	CLEARDATA "POS Dept Trans";
	CLEARDATA "Kiosk Dept Trans";
ENDFIX;

/*  Step 2:  Clear Transactions*/
FIX("FY2010", "No Div", @IDESCENDANTS("All Stores"), "Week 1 Sunday":"Week 10 Saturday")
	CLEARDATA "POS Transactions";
	CLEARDATA "Kiosk Transactions";
ENDFIX;
