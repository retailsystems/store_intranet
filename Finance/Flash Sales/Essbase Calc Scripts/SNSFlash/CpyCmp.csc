SET UPDATECALC OFF;

FIX("No Div", @IDESCENDANTS("All Stores"))
	DATACOPY "Comp Status"->"BkUp2006"->"Week 53 Sunday" TO "Comp Status"->"FY2006"->"Week 53 Sunday";
	DATACOPY "Comp Status"->"BkUp2006"->"Week 53 Monday" TO "Comp Status"->"FY2006"->"Week 53 Monday";
	DATACOPY "Comp Status"->"BkUp2006"->"Week 53 Tuesday" TO "Comp Status"->"FY2006"->"Week 53 Tuesday";
	DATACOPY "Comp Status"->"BkUp2006"->"Week 53 Wednesday" TO "Comp Status"->"FY2006"->"Week 53 Wednesday";
	DATACOPY "Comp Status"->"BkUp2006"->"Week 53 Thursday" TO "Comp Status"->"FY2006"->"Week 53 Thursday";
	DATACOPY "Comp Status"->"BkUp2006"->"Week 53 Friday" TO "Comp Status"->"FY2006"->"Week 53 Friday";
	DATACOPY "Comp Status"->"BkUp2006"->"Week 53 Saturday" TO "Comp Status"->"FY2006"->"Week 53 Saturday";
ENDFIX;
