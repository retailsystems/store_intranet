SET UPDATECALC OFF;

/*  Step 1:  Clear Sales, Unit Sold and Transactions by Department Data*/
FIX("FY2007", @IDESCENDANTS("Dept"), @IDESCENDANTS("All Stores"), "Week 34 Saturday":"Week 34 Saturday")
	CLEARDATA "Gross Sales";
	CLEARDATA "Returns";
	CLEARDATA "Units Sold";
	CLEARDATA "Dept Trans";
ENDFIX;

/*  Step 2:  Clear Transactions*/
FIX("FY2007", "No Div", @IDESCENDANTS("All Stores"), "Week 34 Saturday":"Week 34 Saturday")
	CLEARDATA "Transactions";
ENDFIX;
