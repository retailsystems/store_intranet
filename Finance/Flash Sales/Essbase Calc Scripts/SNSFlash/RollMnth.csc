SET UPDATECALC OFF;
SET AGGMISSG ON;

/*  Step 1:  Roll-up Depts & Stores at day level  */
FIX("FY2010", Returns, "Kiosk Digital Returns", "Kiosk Non-Digital Returns", "Gross Sales", "Kiosk Digital Gross Sales", "Kiosk Non-Digital Gross Sales", "POS Transactions", "Kiosk Transactions", "POS Units Sold", "Kiosk Units Sold", "POS Comp TY", "Kiosk Comp TY", "Plan Comp TY", "POS Comp LY", "Kiosk Comp LY", "Plan Comp LY", "Comp Status", "Week 1 Sunday":"Week 10 Saturday")
	CALC DIM(Dept, Market);
ENDFIX;

/*  Step 2:  Calculate Net Sales at day level at all levels of Dept & Market  */
FIX("FY2010", @IDESCENDANTS(Dept), @IDESCENDANTS(Market), "Week 1 Sunday":"Week 10 Saturday")
	"Kiosk Digital Sales";
	"Kiosk Non-Digital Sales";
	"Kiosk Sales";
	"POS Sales";
	"Net Sales";
	"Transactions";
	"Dept Trans";
	"Units Sold";
	"Comp TY";
	"Comp LY";
ENDFIX;

/*  Step 3: Roll-up Market for Dept Trans Count */
FIX("FY2010", "POS Dept Trans", "Kiosk Dept Trans", "Week 1 Sunday":"Week 10 Saturday")
	CALC DIM(Market);
ENDFIX;

/*  Step 4:  Roll-up affected time levels for all levels of Dept & Market  */
FIX("FY2010","POS Dept Trans", "Kiosk Dept Trans", "Dept Trans", Returns, "Gross Sales", "Kiosk Digital Returns", "Kiosk Digital Gross Sales", "Kiosk Digital Sales", "Kiosk Non-Digital Returns", "Kiosk Non-Digital Gross Sales", "Kiosk Non-Digital Sales", "Kiosk Sales", "POS Sales", "Net Sales", "POS Transactions", "Kiosk Transactions", Transactions, "POS Units Sold", "Kiosk Units Sold", "Units Sold", "Comp TY", "Plan Comp TY", "Comp LY", "Plan Comp LY", "Comp Status", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"February Week 1":"April Week 1";
	"February":"April";
	Qtr1;
	"Total Year";
ENDFIX;
