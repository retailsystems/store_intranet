SET UPDATECALC OFF;
SET AGGMISSG OFF;

/*  Copy CompLY and PlanCompLY at Day Level*/
FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "Week 53 Sunday":"Week 53 Saturday", "Current Year")
	"Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp LY" = "Gross Sales"->"Last Year" + Returns->"Last Year";
		Else						
			"Comp LY" = 0;					
		ENDIF)					
	"Plan Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp LY" = "Plan Sales"->"Last Year";		
		Else						
			"Plan Comp LY" = 0;		
		ENDIF)			
ENDFIX;

/*  Copy CompLY and PlanCompLY at Week Level*/
FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "January Week 5", "Current Year")
	"Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp LY" = "Gross Sales"->"Last Year" + Returns->"Last Year";
		Else						
			"Comp LY" = 0;					
		ENDIF)					
	"Plan Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp LY" = "Plan Sales"->"Last Year";		
		Else						
			"Plan Comp LY" = 0;		
		ENDIF)			
ENDFIX;