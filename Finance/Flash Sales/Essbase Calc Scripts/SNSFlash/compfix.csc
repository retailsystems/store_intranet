SET UPDATECALC OFF;

FIX(@IDESCENDANTS(Dept), "Store 8001", "Week 1 Sunday":"Week 17 Saturday", "Week 27 Sunday":"Week 30 Saturday", "Current Year")
	"Comp TY"
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp TY" = "Net Sales";			
		Else						
			"Comp TY" = 0;					
		ENDIF)					
ENDFIX;


/*  Roll-up Depts & Stores at day level  */
FIX( "Comp TY", "Comp Status", "Week 1 Sunday":"Week 17 Saturday", "Week 27 Sunday":"Week 30 Saturday", "Current Year")
	CALC DIM(Dept, Market);
ENDFIX;


/* Roll-up affected time levels for all levels of Dept & Market  */
FIX("Comp TY", "Comp Status", @IDESCENDANTS(Dept), @IDESCENDANTS(Market), "Current Year")
	"February Week 1":"May Week 4";
	"August Week 1":"August Week 4";
	February:May;
	August;
	Qtr1:Qtr3;
	"Total Year";
ENDFIX;