SET UPDATECALC OFF;
SET AGGMISSG ON;

/*  Step 1:  Roll-up Depts & Stores at day level  */
FIX("FY2007", Returns, "Gross Sales", "Units Sold", "Comp TY", "Plan Comp TY", "Comp LY", "Plan Comp LY", Transactions, "Comp Status", "Week 34 Saturday":"Week 34 Saturday")
	CALC DIM(Dept, Market);
ENDFIX;

/*  Step 2:  Calculate Net Sales at day level at all levels of Dept & Market  */
FIX("FY2007", @IDESCENDANTS(Dept), @IDESCENDANTS(Market), "Week 34 Saturday":"Week 34 Saturday")
	"Net Sales";
ENDFIX;

/*  Step 3: Roll-up Market for Dept Trans Count */
FIX("FY2007", "Dept Trans", "Week 34 Saturday":"Week 34 Saturday")
	CALC DIM(Market);
ENDFIX;

/*  Step 4:  Roll-up affected time levels for all levels of Dept & Market  */
FIX("FY2007", "Dept Trans", Returns, "Gross Sales", "Net Sales", "Units Sold", "Comp TY", "Plan Comp TY","Comp LY", "Plan Comp LY", Transactions, "Comp Status", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"September Week 4":"September Week 4";
	September;
	Qtr3;
	"Total Year";
ENDFIX;
