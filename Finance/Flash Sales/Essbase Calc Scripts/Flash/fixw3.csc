SET UPDATECALC OFF;
SET AGGMISSG OFF;


FIX("Current Year","Comp LY", @CHILDREN("February Week 3"))
	CALC DIM(Dept, Market);
ENDFIX;

FIX("Current Year", "Comp LY", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"February Week 3";
	February;
	Qtr1;
	"Total Year";
ENDFIX;

