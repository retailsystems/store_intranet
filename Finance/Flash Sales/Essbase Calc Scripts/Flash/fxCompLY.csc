SET UPDATECALC OFF;
SET AGGMISSG OFF;

/*  Rolls-up only the last processed day &CurrDay */

/*  Calculate Comp TY & COMP LY for all the days in the Current Week - Net Sales has not been calc'd so add the components  */
FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "Week 5 Sunday":"Week 9 Saturday", "Current Year")
	"Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp LY" = "Gross Sales"->"Last Year" + Returns->"Last Year";
		Else						
			"Comp LY" = 0;					
		ENDIF)					
ENDFIX;

FIX("Current Year", "Comp LY", "Week 5 Sunday":"Week 9 Saturday")
	CALC DIM(Dept, Market);
ENDFIX;

FIX("Current Year", "Comp LY", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"March Week 1":"March Week 5";
	March;
	Qtr1;
	"Total Year";
ENDFIX;


