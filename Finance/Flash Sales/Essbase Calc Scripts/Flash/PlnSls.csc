SET UPDATECALC OFF;


FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "Week 1 Sunday":"Week 1 Saturday", "Current Year")
	"Comp TY"					
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp LY" = "Net Sales"->"Last Year";
		Else						
			"Comp LY" = 0;					
		ENDIF)					
	"Plan Comp TY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp LY" = "Plan Sales"->"Last Year";		
		Else						
			"Plan Comp LY" = 0;		
		ENDIF)					
ENDFIX;


/*  Roll-up Depts & Stores at day level  */
FIX("Comp LY", "Plan Comp LY", "Week 1 Sunday":"Week 1 Saturday", "Current Year")
	CALC DIM(Dept, Market);
ENDFIX;


/* Roll-up affected time levels for all levels of Dept & Market  */
FIX("Comp LY", "Plan Comp LY", @IDESCENDANTS(Dept), @IDESCENDANTS(Market), "Current Year")
	"February Week 1":"February Week 4";
	February;
	Qtr1;
	"Total Year";

ENDFIX;