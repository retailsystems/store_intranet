SET UPDATECALC OFF;
SET AGGMISSG ON;

/*  Run this after the Shift occured for Week 7 */
FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "Week 7 Sunday", "FY2006")
	"Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp LY" = "Gross Sales"->"FY2005"->"Week 8 Sunday" + Returns->"FY2005"->"Week 8 Sunday";
		Else						
			"Comp LY" = 0;					
		ENDIF)					
	"Plan Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp LY" = "Plan Sales"->"FY2005"->"Week 8 Sunday";		
		Else						
			"Plan Comp LY" = 0;		
		ENDIF)			
ENDFIX;


FIX("FY2006", "Comp LY", "Plan Comp LY", "Week 7 Sunday")
	CALC DIM(Dept, Market);
ENDFIX;

FIX("FY2006", "Comp LY", "Plan Comp LY", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"March Week 3":"March Week 3";
	March;
	Qtr1;
	"Total Year";
ENDFIX;

/*  Run this after the Shift for Feb. Occured  */
FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "Week 7 Monday", "FY2006")
	"Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp LY" = "Gross Sales"->"FY2005"->"Week 8 Monday" + Returns->"FY2005"->"Week 8 Monday";
		Else						
			"Comp LY" = 0;					
		ENDIF)					
	"Plan Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp LY" = "Plan Sales"->"FY2005"->"Week 8 Monday";		
		Else						
			"Plan Comp LY" = 0;		
		ENDIF)			
ENDFIX;


FIX("FY2006", "Comp LY", "Plan Comp LY", "Week 7 Monday")
	CALC DIM(Dept, Market);
ENDFIX;

FIX("FY2006", "Comp LY", "Plan Comp LY", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"March Week 3":"March Week 3";
	March;
	Qtr1;
	"Total Year";
ENDFIX;

/*  Run this after the Shift for Feb. Occured  */
FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "Week 7 Tuesday", "FY2006")
	"Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp LY" = "Gross Sales"->"FY2005"->"Week 8 Tuesday" + Returns->"FY2005"->"Week 8 Tuesday";
		Else						
			"Comp LY" = 0;					
		ENDIF)					
	"Plan Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp LY" = "Plan Sales"->"FY2005"->"Week 8 Tuesday";		
		Else						
			"Plan Comp LY" = 0;		
		ENDIF)			
ENDFIX;


FIX("FY2006", "Comp LY", "Plan Comp LY", "Week 7 Tuesday")
	CALC DIM(Dept, Market);
ENDFIX;

FIX("FY2006", "Comp LY", "Plan Comp LY", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"March Week 3":"March Week 3";
	March;
	Qtr1;
	"Total Year";
ENDFIX;

/*  Run this after the Shift for Feb. Occured  */
FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "Week 7 Wednesday", "FY2006")
	"Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp LY" = "Gross Sales"->"FY2005"->"Week 8 Wednesday" + Returns->"FY2005"->"Week 8 Wednesday";
		Else						
			"Comp LY" = 0;					
		ENDIF)					
	"Plan Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp LY" = "Plan Sales"->"FY2005"->"Week 8 Wednesday";		
		Else						
			"Plan Comp LY" = 0;		
		ENDIF)			
ENDFIX;


FIX("FY2006", "Comp LY", "Plan Comp LY", "Week 7 Wednesday")
	CALC DIM(Dept, Market);
ENDFIX;

FIX("FY2006", "Comp LY", "Plan Comp LY", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"March Week 3":"March Week 3";
	March;
	Qtr1;
	"Total Year";
ENDFIX;

/*  Run this after the Shift for Feb. Occured  */
FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "Week 7 Thursday", "FY2006")
	"Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp LY" = "Gross Sales"->"FY2005"->"Week 8 Thursday" + Returns->"FY2005"->"Week 8 Thursday";
		Else						
			"Comp LY" = 0;					
		ENDIF)					
	"Plan Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp LY" = "Plan Sales"->"FY2005"->"Week 8 Thursday";		
		Else						
			"Plan Comp LY" = 0;		
		ENDIF)			
ENDFIX;


FIX("FY2006", "Comp LY", "Plan Comp LY", "Week 7 Thursday")
	CALC DIM(Dept, Market);
ENDFIX;

FIX("FY2006", "Comp LY", "Plan Comp LY", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"March Week 3":"March Week 3";
	March;
	Qtr1;
	"Total Year";
ENDFIX;

/*  Run this after the Shift for Feb. Occured  */
FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "Week 7 Friday", "FY2006")
	"Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp LY" = "Gross Sales"->"FY2005"->"Week 8 Friday" + Returns->"FY2005"->"Week 8 Friday";
		Else						
			"Comp LY" = 0;					
		ENDIF)					
	"Plan Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp LY" = "Plan Sales"->"FY2005"->"Week 8 Friday";		
		Else						
			"Plan Comp LY" = 0;		
		ENDIF)			
ENDFIX;


FIX("FY2006", "Comp LY", "Plan Comp LY", "Week 7 Friday")
	CALC DIM(Dept, Market);
ENDFIX;

FIX("FY2006", "Comp LY", "Plan Comp LY", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"March Week 3":"March Week 3";
	March;
	Qtr1;
	"Total Year";
ENDFIX;

/*  Run this after the Shift for Feb. Occured  */
FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "Week 7 Saturday", "FY2006")
	"Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp LY" = "Gross Sales"->"FY2005"->"Week 8 Saturday" + Returns->"FY2005"->"Week 8 Saturday";
		Else						
			"Comp LY" = 0;					
		ENDIF)					
	"Plan Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp LY" = "Plan Sales"->"FY2005"->"Week 8 Saturday";		
		Else						
			"Plan Comp LY" = 0;		
		ENDIF)			
ENDFIX;


FIX("FY2006", "Comp LY", "Plan Comp LY", "Week 7 Saturday")
	CALC DIM(Dept, Market);
ENDFIX;

FIX("FY2006", "Comp LY", "Plan Comp LY", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"March Week 3":"March Week 3";
	March;
	Qtr1;
	"Total Year";
ENDFIX;

