SET UPDATECALC OFF;
SET AGGMISSG ON;

/*  Step 1:  Roll-up Depts & Stores at day level  */
FIX("BkUp2006", Returns, "Gross Sales", "Units Sold", "Comp TY", "Plan Comp TY", "Comp LY", "Plan Comp LY", Transactions, "Comp Status", "Week 41 Sunday":"Week 53 Saturday")
	CALC DIM(Dept, Market);
ENDFIX;

/*  Step 2:  Calculate Net Sales at day level at all levels of Dept & Market  */
FIX("BkUp2006", @IDESCENDANTS(Dept), @IDESCENDANTS(Market), "Week 41 Sunday":"Week 53 Saturday")
	"Net Sales";
ENDFIX;

/*  Step 3: Roll-up Market for Dept Trans Count */
FIX("Current Year", "Dept Trans", "Week 41 Sunday":"Week 53 Saturday")
	CALC DIM(Market);
ENDFIX;

/*  Step 4:  Roll-up affected time levels for all levels of Dept & Market  */
FIX("BkUp2006", "Dept Trans", Returns, "Gross Sales", "Net Sales", "Units Sold", "Comp TY", "Plan Comp TY","Comp LY", "Plan Comp LY", Transactions, "Comp Status", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"November Week 2":"January Week 5";
	"November":"January";
	"Qtr4";
	"Total Year";
ENDFIX;
