SET UPDATECALC OFF;


/* Calculate Planned Payroll Hours for the last 8 weeks only */
/*  The time range is at the day level - however there is only data for all * Hours variables and New Store Status only at each Sunday (i.e. only at week level)  */
FIX ("No Div",  @LEVMBRS(Market,0), "Week 48 Sunday":"Week 52 Saturday", "Last Year")

"Planned Payroll Hours"
	(IF("New Store Status"==1)
		"Work Hours" + "OT Hours" - "Training Hours" + "Term Hours";
	ELSEIF ("Planned minimum hours" > 0  AND  "Planned minimum hours" > ("Planned selling hours" + "Planned task hours"))
		"Planned minimum hours";
	ELSE
		("Planned selling hours" + "Planned task hours");
	ENDIF)
ENDFIX;

/*  Roll-up Depts & Stores at day level  */
FIX("Plan Sales", "Work Hours", "OT Hours", "Training Hours", "Term Hours", "Planned selling hours", "Planned task hours", "Planned minimum hours", "Planned Payroll Hours", 
      "Comp TY", "Comp LY", "Plan Comp TY", "Plan Comp LY", "Comp Status", "New Store Status", "Week 48 Sunday":"Week 52 Saturday", "Last Year")
	CALC DIM(Dept, Market);
ENDFIX;


/* Roll-up affected time levels for all levels of Dept & Market  */
FIX("Plan Sales", "Work Hours", "OT Hours", "Training Hours", "Term Hours", "Planned selling hours", "Planned task hours", "Planned minimum hours", "Planned Payroll Hours", 
      "Comp TY", "Comp LY", "Plan Comp TY", "Plan Comp LY", "Comp Status", "New Store Status", @IDESCENDANTS(Dept), @IDESCENDANTS(Market), "Last Year")
	"December Week 5":"January Week 4";
	"December":"January";
	"Qtr4";
	"Total Year";
ENDFIX;