SET UPDATECALC OFF;
SET AGGMISSG OFF;


/*  Step 1:  Roll-up Depts & Stores at day level  */
FIX("4 Years Ago", Transactions, "Units Sold",  @IDESCENDANTS(Qtr4))
	CALC DIM(Dept, Market);
ENDFIX;


/*  Step 2:  Roll-up affected time levels for all levels of Dept & Market  */
FIX("4 Years Ago", Transactions, "Units Sold", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
        @idescendants(qtr4);
	"Total Year";
ENDFIX;
