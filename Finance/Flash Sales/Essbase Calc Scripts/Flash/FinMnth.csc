SET UPDATECALC OFF;
SET AGGMISSG ON;

/* Calculate Planned Payroll Hours for the last 8 weeks only */
/*  The time range is at the day level - however there is only data for all * Hours variables and New Store Status only at each Sunday (i.e. only at week level)  */
FIX ("No Div",  @LEVMBRS(Market,0), "Week 40 Sunday":"Week 53 Saturday", "FY2006")

"Planned Payroll Hours"
	(IF("New Store Status"==1)
		IF (@ISMBR(@CHILDREN("Torrid")))
			425;
		ELSE
			425;
		ENDIF
	ELSEIF ("Planned minimum hours" > 0  AND  "Planned minimum hours" > ("Planned selling hours" + "Planned task hours"))
		"Planned minimum hours";
	ELSE
		("Planned selling hours" + "Planned task hours");
	ENDIF)

	"Company Open Stores" = "Hot Topic Open Stores Input" + "Torrid Open Stores Input";
	"Company Open Stores MTD" = "Hot Topic Opened MTD Input" + "Torrid Opened MTD Input";
	"Company Open Stores YTD" = "Hot Topic Opened YTD Input" + "Torrid Opened YTD Input";
ENDFIX;

/*  Roll-up Depts & Stores at day level  */
FIX("Plan Sales", "Work Hours", "OT Hours", "Training Hours", "Term Hours", "Planned selling hours", "Planned task hours", "Planned minimum hours", "Planned Payroll Hours", 
      "Comp TY", "Comp LY", "Plan Comp TY", "Plan Comp LY", "Comp Status", "New Store Status", 
      "Hot Topic Open Stores Input", "Torrid Open Stores Input", "Hot Topic Opened MTD Input", "Torrid Opened MTD Input", "Hot Topic Opened YTD Input", "Torrid Opened YTD Input",    
      "Company Open Stores", "Company Open Stores MTD", "Company Open Stores YTD", "Week 40 Sunday":"Week 53 Saturday", "FY2006")
	CALC DIM(Dept, Market);
ENDFIX;


/* Roll-up affected time levels for all levels of Dept & Market  */
FIX("Plan Sales", "Work Hours", "OT Hours", "Training Hours", "Term Hours", "Planned selling hours", "Planned task hours", "Planned minimum hours", "Planned Payroll Hours", 
      "Comp TY", "Comp LY", "Plan Comp TY", "Plan Comp LY", "Comp Status", "New Store Status", 
      "Hot Topic Open Stores Input", "Torrid Open Stores Input", "Hot Topic Opened MTD Input", "Torrid Opened MTD Input", "Hot Topic Opened YTD Input", "Torrid Opened YTD Input",    
      "Company Open Stores", "Company Open Stores MTD", "Company Open Stores YTD", @IDESCENDANTS(Dept), @IDESCENDANTS(Market), "FY2006")
	"November Week 1":"January Week 5";
	"January":"January";
	Qtr4;
	"Total Year";
ENDFIX;
