SET UPDATECALC OFF;
SET AGGMISSG OFF;

/*  Run this after the Current Year has been changed to the NEW YEAR  */
FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "Week 50 Sunday":"Week 52 Saturday", "Last Year")
	"Comp LY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp LY" = "Gross Sales"->"2 years ago" + Returns->"2 years ago";
		Else						
			"Comp LY" = 0;					
		ENDIF)					
ENDFIX;


FIX("Last Year", "Comp LY", "Week 50 Sunday":"Week 52 Saturday")
	CALC DIM(Dept, Market);
ENDFIX;

FIX("Last Year", "Comp LY", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"January Week 2":"January Week 4";
	January;
	Qtr4;
	"Total Year";
ENDFIX;