SET UPDATECALC OFF;
SET AGGMISSG ON;

/*  Step 1:  Roll-up Depts & Stores at day level  */
FIX("FY2010", Returns, "Gross Sales", "Units Sold", "Comp TY", "Plan Comp TY", "Comp LY", "Plan Comp LY", Transactions, "Comp Status", "Week 1 Sunday":"Week 10 Saturday")
	CALC DIM(Dept, Market);
ENDFIX;

/*  Step 2:  Calculate Net Sales at day level at all levels of Dept & Market  */
FIX("FY2010", @IDESCENDANTS(Dept), @IDESCENDANTS(Market), "Week 1 Sunday":"Week 10 Saturday")
	"Net Sales";
ENDFIX;

/*  Step 3: Roll-up Market for Dept Trans Count */
FIX("Current Year", "Dept Trans", "Week 1 Sunday":"Week 10 Saturday")
	CALC DIM(Market);
ENDFIX;

/*  Step 4:  Roll-up affected time levels for all levels of Dept & Market  */
FIX("FY2010", "Dept Trans", Returns, "Gross Sales", "Net Sales", "Units Sold", "Comp TY", "Plan Comp TY","Comp LY", "Plan Comp LY", Transactions, "Comp Status", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"February Week 1":"April Week 1";
	"February":"April";
	Qtr1;
	"Total Year";
ENDFIX;
