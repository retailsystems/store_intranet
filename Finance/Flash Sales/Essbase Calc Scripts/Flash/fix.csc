SET UPDATECALC OFF;

FIX("Hot Topic Open Stores Input", "Torrid Open Stores Input", "Hot Topic Opened MTD Input", "Torrid Opened MTD Input", "Hot Topic Opened YTD Input", "Torrid Opened YTD Input",    
      "Company Open Stores", "Company Open Stores MTD", "Company Open Stores YTD", "Week 15 Sunday":"Week 15 Saturday", "Current Year")
	CALC DIM(Dept, Market);
ENDFIX;


/* Roll-up affected time levels for all levels of Dept & Market  */
FIX("Hot Topic Open Stores Input", "Torrid Open Stores Input", "Hot Topic Opened MTD Input", "Torrid Opened MTD Input", "Hot Topic Opened YTD Input", "Torrid Opened YTD Input",    
      "Company Open Stores", "Company Open Stores MTD", "Company Open Stores YTD", @IDESCENDANTS(Dept), @IDESCENDANTS(Market), "Current Year")
	"May Week 2":"May Week 2";
	"May":"May";
	"Qtr2":"Qtr2";
	"Total Year";
ENDFIX;