SET UPDATECALC OFF;
SET AGGMISSG OFF;

/*  Rolls-up only the last processed day &CurrDay */

/*  Calculate Comp TY & COMP LY for all the days in the Current Week - Net Sales has not been calc'd so add the components  */
FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), "Week 1 Sunday":"Week 2 Sunday", "Current Year")
	"Comp TY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Comp TY" = "Gross Sales" + Returns;			
			"Comp LY" = "Gross Sales"->"Last Year" + Returns->"Last Year";
		Else						
			"Comp TY" = 0;					
			"Comp LY" = 0;					
		ENDIF)					
	"Plan Comp TY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp TY" = "Plan Sales";	
			"Plan Comp LY" = "Plan Sales"->"Last Year";		
		Else						
			"Plan Comp TY" = 0;					
			"Plan Comp LY" = 0;		
		ENDIF)			
ENDFIX;

/*  This section was added to MOVE Torrid Internet sales to Store 5990 Torrid Internet  */
FIX("Week 1 Sunday":"Week 2 Sunday", "Current Year", @ICHILDREN("Division 5"))
	"Store 5990"(
	IF ("Store 4490" <> #missing and "Store 4490" <> 0)	
		"Store 5990" = "Store 4490";
	ENDIF);
ENDFIX;
/* Blank the Torrid Internet sales out of the Hot Topic Internet store  */
FIX("Week 1 Sunday":"Week 2 Sunday", "Current Year", @ICHILDREN("Division 5"))
	"Store 4490" = #missing;
ENDFIX;

/*  Roll-up Depts & Stores at day level  */
FIX(Returns, "Gross Sales", Transactions, "Units Sold", "Comp TY", "Plan Comp TY", "Comp LY", "Plan Comp LY", "Comp Status", "Week 1 Sunday":"Week 2 Sunday", "Current Year")
	CALC DIM(Dept, Market);
ENDFIX;

/*  Step 2:  Calculate Net Sales at day level at all levels of Dept & Market  */
FIX(@IDESCENDANTS(Dept), @IDESCENDANTS(Market), "Week 1 Sunday":"Week 2 Sunday", "Current Year")
	"Net Sales";
ENDFIX;

/*  Step 3:  Roll-up affected time levels for all levels of Dept & Market  */
FIX("Current Year", Returns, "Gross Sales", "Net Sales", Transactions, "Units Sold", "Comp TY", "Plan Comp TY", "Comp LY", "Plan Comp LY", "Comp Status", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	"February Week 1":"February Week 2";
	"February";
	"Qtr1":"Qtr1";
	"Total Year";
ENDFIX;

