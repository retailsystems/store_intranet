'****************************************************************************************
' Comp Sales Extract
' creates \\CAFLAPRD01\ess_outbound\CompSales.csv
' Created:  Feb 2005
'****************************************************************************************

option explicit

private const CMP_SLS = "C:\Ess_Outbound\CompSales.csv"

call main

private sub main
  dim shell
  dim fs
  dim folder
  dim fsz


  on error resume next

  set shell = wscript.createobject("wscript.shell")
  set fs = createobject("scripting.filesystemobject")


  call writelog("***** Comp Sales Extract *****")  

  if fs.fileexists(CMP_SLS) then
	    fs.deletefile CMP_SLS
  end if

  shell.run Chr(34) & "C:\Batch\Flash\EssExtracts\OtherExtract.xls" & Chr(34), 1, true
  If Err.Number <> 0 and Err.Number <> 53 and Err.Number <> 70 Then 
    call writelog("Error # " & Err.Number & " running the comp sales extract")
    exit sub
  else
    call writelog("Comp Sales extract ran successfully")
  end if


end sub

private sub writelog(byval message)
  dim filesys, filetxt
  Const ForReading = 1, ForWriting = 2, ForAppending = 8 

  Set filesys = CreateObject("Scripting.FileSystemObject")
  Set filetxt = filesys.OpenTextFile("C:\Batch\Flash\BatchLog\DailyFlash.log", ForAppending, True) 

  filetxt.writeline(date & " " & time & " " & message)

  filetxt.Close 
end sub

