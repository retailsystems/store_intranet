'********************************************************************************
' This script reruns the web pages & publishes them
' through UserRoll.exe
'********************************************************************************

option explicit


private const WORKING_DIR = "\\CAFLAPRD01\batch\Flash\DailyReportRerun_SNS"


call main

private sub main
  dim shell
  dim fs
  dim folder


  on error resume next

  set shell = wscript.createobject("wscript.shell")
  set fs = createobject("scripting.filesystemobject")


  call writelog("***** Web reports clean-up *****")  
  'Clean out all the old web reports

  fs.DeleteFile WORKING_DIR & "\*.htm"

  If fs.FolderExists(WORKING_DIR & "\grouptotals_files\") Then
    set folder = fs.GetFolder(WORKING_DIR & "\grouptotals_files\")
    folder.Delete
  end if
  If fs.FolderExists(WORKING_DIR & "\grouptotalslw_files\") Then
    set folder = fs.GetFolder(WORKING_DIR & "\grouptotalslw_files\")
    folder.Delete
  end if
  If fs.FolderExists(WORKING_DIR & "\htdistrict_files\") Then
    set folder = fs.GetFolder(WORKING_DIR & "\htdistrict_files\")
    folder.Delete
  end if
  If fs.FolderExists(WORKING_DIR & "\htdistrictlw_files\") Then
    set folder = fs.GetFolder(WORKING_DIR & "\htdistrictlw_files\")
    folder.Delete
  end if
  If fs.FolderExists(WORKING_DIR & "\htstorer_files\") Then
    set folder = fs.GetFolder(WORKING_DIR & "\htstorer_files\")
    folder.Delete
  end if
  If fs.FolderExists(WORKING_DIR & "\htstorerlw_files\") Then
    set folder = fs.GetFolder(WORKING_DIR & "\htstorerlw_files\")
    folder.Delete
  end if
  If fs.FolderExists(WORKING_DIR & "\tdistrict_files\") Then
    set folder = fs.GetFolder(WORKING_DIR & "\tdistrict_files\")
    folder.Delete
  end if
  If fs.FolderExists(WORKING_DIR & "\tdistrictlw_files\") Then
    set folder = fs.GetFolder(WORKING_DIR & "\tdistrictlw_files\")
    folder.Delete
  end if
  If fs.FolderExists(WORKING_DIR & "\tstorer_files\") Then
    set folder = fs.GetFolder(WORKING_DIR & "\tstorer_files\")
    folder.Delete
  end if
  If fs.FolderExists(WORKING_DIR & "\tstorerlw_files\") Then
    set folder = fs.GetFolder(WORKING_DIR & "\tstorerlw_files\")
    folder.Delete
  end if


  call writelog("***** Run Web reports *****")  
  shell.run Chr(34) & WORKING_DIR & "\WebRpts.xls" & Chr(34), 1, true
  If Err.Number <> 0 and err.number <> 53 Then 
    call writelog("Error # " & Err.Number & " running the web report for TW")
  else
    call writelog("TW Web reports ran successfully")
  end if


  shell.run WORKING_DIR & "\RerunWeb2.VBS", 1, true
end sub

private sub writelog(byval message)
  dim filesys, filetxt
  Const ForReading = 1, ForWriting = 2, ForAppending = 8 

  Set filesys = CreateObject("Scripting.FileSystemObject")
  Set filetxt = filesys.OpenTextFile(WORKING_DIR & "\RerunWeb.log", ForAppending, True) 

  filetxt.writeline(date & " " & time & " " & message)

  filetxt.Close 
end sub