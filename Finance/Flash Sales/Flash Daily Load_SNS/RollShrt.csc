SET UPDATECALC OFF;
SET AGGMISSG OFF;

/*  Rolls-up only the last processed day &CurrDay */

/*  Calculate Comp TY & COMP LY for all the days in the Current Week - Net Sales has not been calc'd so add the components  */
FIX(@IDESCENDANTS(Dept), @LEVMBRS(Market, 0), @CHILDREN(&CurrWk), "Current Year")
	"Comp TY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"POS Comp TY" = "Gross Sales" + Returns;			
			"POS Comp LY" = "Gross Sales"->"Last Year" + Returns->"Last Year";
			"Kiosk Comp TY" = "Kiosk Digital Gross Sales" + "Kiosk Digital Returns" + "Kiosk Non-Digital Gross Sales" + "Kiosk Non-Digital Returns";			
			"Kiosk Comp LY" = "Kiosk Digital Gross Sales"->"Last Year" + "Kiosk Digital Returns"->"Last Year" + "Kiosk Non-Digital Gross Sales"->"Last Year" + "Kiosk Non-Digital Returns"->"Last Year";
		Else						
			"POS Comp TY" = 0;					
			"POS Comp LY" = 0;
			"Kiosk Comp TY" = 0;					
			"Kiosk Comp LY" = 0;					
		ENDIF)					
	"Plan Comp TY"				
		 (IF("Comp Status"->"No Div" > 0)		
			"Plan Comp TY" = "Plan Sales";	
			"Plan Comp LY" = "Plan Sales"->"Last Year";		
		Else						
			"Plan Comp TY" = 0;					
			"Plan Comp LY" = 0;		
		ENDIF)			
ENDFIX;

/*  Step 1: Roll-up Depts & Stores at day level  */
FIX("Current Year", Returns, "Kiosk Digital Returns", "Kiosk Non-Digital Returns", "Gross Sales", "Kiosk Digital Gross Sales", "Kiosk Non-Digital Gross Sales", "POS Transactions", "Kiosk Transactions", "POS Units Sold", "Kiosk Units Sold", "POS Comp TY", "Kiosk Comp TY", "Plan Comp TY", "POS Comp LY", "Kiosk Comp LY", "Plan Comp LY", "Comp Status", @CHILDREN(&CurrWk))
	CALC DIM(Dept, Market);
ENDFIX;

/*  Step 2:  Calculate Net Sales at day level at all levels of Dept & Market  */
FIX("Current Year", @IDESCENDANTS(Dept), @IDESCENDANTS(Market), &CurrDay)
	"Kiosk Digital Sales";
	"Kiosk Non-Digital Sales";
	"Kiosk Sales";
	"POS Sales";
	"Net Sales";
	"Transactions";
	"Dept Trans";
	"Units Sold";
	"Comp TY";
	"Comp LY";
ENDFIX;

/*  Step 3: Roll-up Market for Dept Trans Count */
FIX("Current Year", "POS Dept Trans",  "Kiosk Dept Trans", @CHILDREN(&CurrWk))
	CALC DIM(Market);
ENDFIX;

/*  Step 4:  Roll-up affected time levels for all levels of Dept & Market */
FIX("Current Year", "POS Dept Trans", "Kiosk Dept Trans", "Dept Trans", Returns, "Gross Sales", "Kiosk Digital Returns", "Kiosk Digital Gross Sales", "Kiosk Digital Sales", "Kiosk Non-Digital Returns", "Kiosk Non-Digital Gross Sales", "Kiosk Non-Digital Sales", "Kiosk Sales", "POS Sales", "Net Sales", "POS Transactions", "Kiosk Transactions", Transactions, "POS Units Sold", "Kiosk Units Sold", "Units Sold", "Comp TY", "POS Comp TY", "Kiosk Comp TY", "Plan Comp TY", "Comp LY", "POS Comp LY", "Kiosk Comp LY", "Plan Comp LY", "Comp Status", @IDESCENDANTS(Dept), @IDESCENDANTS(Market))
	&CurrWk;
	&CurrMth;
	&CurrQtr;
	"Total Year";
ENDFIX;

