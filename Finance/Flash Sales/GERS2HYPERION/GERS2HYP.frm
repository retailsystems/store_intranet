VERSION 5.00
Begin VB.Form GERS2HYP 
   Caption         =   "GERS To Hyperion Data Extract"
   ClientHeight    =   2160
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4140
   LinkTopic       =   "Form1"
   ScaleHeight     =   2160
   ScaleWidth      =   4140
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdGetTrnCnt 
      Caption         =   "Average Trans"
      Height          =   495
      Left            =   720
      TabIndex        =   2
      Top             =   1200
      Width           =   1215
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "Exit"
      Height          =   495
      Left            =   2280
      TabIndex        =   1
      Top             =   1200
      Width           =   1215
   End
   Begin VB.CommandButton cmdRawSls 
      Caption         =   "Raw Sales"
      Height          =   495
      Left            =   720
      TabIndex        =   0
      ToolTipText     =   "Generate"
      Top             =   360
      Width           =   1215
   End
End
Attribute VB_Name = "GERS2HYP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdExit_Click()
End

End Sub

Private Sub cmdGetTrnCnt_Click()
frmAvgTran.Show vbModal

End Sub

Private Sub cmdRawSls_Click()
frmRawSales.Show vbModal

End Sub
