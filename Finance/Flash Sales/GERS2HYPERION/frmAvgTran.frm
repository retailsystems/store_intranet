VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmAvgTran 
   Caption         =   "Average Trans"
   ClientHeight    =   2760
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5385
   LinkTopic       =   "Form1"
   ScaleHeight     =   2760
   ScaleWidth      =   5385
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdAvgTrnRun 
      Caption         =   "&Run"
      Height          =   495
      Left            =   2040
      TabIndex        =   5
      Top             =   1200
      Width           =   1215
   End
   Begin VB.CommandButton cmdAvgTrnExit 
      Caption         =   "E&xit"
      Height          =   495
      Left            =   2040
      TabIndex        =   4
      Top             =   2040
      Width           =   1215
   End
   Begin MSComCtl2.DTPicker DTPickerAvgTrn2 
      Height          =   375
      Left            =   3480
      TabIndex        =   1
      Top             =   480
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      _Version        =   393216
      Format          =   22740993
      CurrentDate     =   37298
   End
   Begin MSComCtl2.DTPicker DTPickerAvgTrn1 
      Height          =   375
      Left            =   960
      TabIndex        =   0
      Top             =   480
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   661
      _Version        =   393216
      Format          =   22740993
      CurrentDate     =   37298
   End
   Begin VB.Label lblAvgTranTo 
      Caption         =   "To:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3000
      TabIndex        =   3
      Top             =   480
      Width           =   615
   End
   Begin VB.Label lblAvgTrnFrom 
      Caption         =   "From:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   480
      Width           =   735
   End
End
Attribute VB_Name = "frmAvgTran"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdAvgTrnExit_Click()
LogTransaction ("Unloading Average Transaction form")
EndLogging

Unload Me

End Sub

Private Sub cmdAvgTrnRun_Click()
'Write Average Trans data into a text file
'Text file is "," delimited
'Data come from POS_TRN_LN

Dim sINIFile As String
Dim strUserID As String
Dim strPasswd As String
Dim strConn As String
Dim strProvider As String
Dim strFname_sls As String
Dim strAmountLimit As String

Dim strSpace As String
Dim strAvgTrnWeek As String
Dim strAvgTrnDept As String
Dim strAvgTrnStore As String
Dim strAvgTrnFY As String
Dim strAvgTrnActual As String
Dim strAvgTrnSalesInp As String

On Error GoTo ErrorLogging

'location of the ini file
sINIFile = App.Path & "\GERS2HYP.ini"

'Read the GERS user login name from ini file
strUserID = sGetINI(sINIFile, "DATABASE SETTING", "Username", "?")
If strUserID = "?" Then
    'No username
    strUserID = InputBox$("Enter GERS user login name, please :")
    writeINI sINIFile, "DATABASE SETTING", "USERNAME", strUserID
End If

'Read the GERS password from ini file
strPasswd = sGetINI(sINIFile, "DATABASE SETTING", "PASSWORD", "?")
If strPasswd = "?" Then
    'No password
    strPasswd = InputBox$("Enter GERS user password, please :")
    writeINI sINIFile, "DATABASE SETTING", "PASSWORD", strPasswd
End If

'Read the GERS database connection from ini file
strConn = sGetINI(sINIFile, "DATABASE SETTING", "CONNECTION", "?")
If strConn = "?" Then
    'No DATABASE
    strConn = InputBox$("Enter GERS database server, please :")
    writeINI sINIFile, "DATABASE SETTING", "CONNECTION", strConn
End If

'Read the GERS database provider from ini file
strProvider = sGetINI(sINIFile, "DATABASE SETTING", "PROVIDER", "?")
If strProvider = "?" Then
    'No PROVIDER
    strProvider = InputBox$("Enter your database provider, please :")
    writeINI sINIFile, "DATABASE SETTING", "PROVIDER", strProvider
End If

'Read the transaction extract location from ini file
strFname_sls = sGetINI(sINIFile, "EXTRACT FILE LOCATION", "TRANS DIR", "?")
If strFname_sls = "?" Then
    'No file location
    strFname_sls = InputBox$("Enter Transaction File Location as C: , please :")
    writeINI sINIFile, "EXTRACT FILE LOCATION", "TRANS DIR", strFname_sls
End If

'Read the amount limit from ini file
strAmountLimit = sGetINI(sINIFile, "DATA", "AMOUNT LIMIT", "?")
If strAmountLimit = "?" Then
    'No amount limit
    strAmountLimit = InputBox$("Enter Amount Limit: , please :")
    writeINI sINIFile, "DATA", "AMOUNT LIMIT", strAmountLimit
End If

LogTransaction ("Running Average Transaction extract")

Dim strAvgTrnDateFrom As String
Dim strAvgTrnDateTo As String

strAvgTrnDateFrom = Format(DTPickerAvgTrn1.Value, "Medium date")
strAvgTrnDateTo = Format(DTPickerAvgTrn2.Value, "Medium date")

LogTransaction ("Changing mouse pointer to 11")
frmAvgTran.MousePointer = 11
LogTransaction ("Connecting to GERS")
Set cn = New ADODB.Connection

strcn1 = "Provider=" & strProvider & ";Password=" & strPasswd
strcn1 = strcn1 & ";User ID=" & strUserID
strcn1 = strcn1 & ";Data Source=" & strConn
strcn1 = strcn1 & ";Persist Security Info=True "

LogTransaction ("Connection string: " & strcn1)

cn.Open strcn1

'Check if Date ranges are used or one day
If strAvgTrnDateFrom = strAvgTrnDateTo Then
'One day of data
'Get Trans count
strSql = " SELECT GR_UTIL.WEEK_NO(trn_dt),   "
strSql = strSql & " GR_UTIL.YEAR_NO(trn_dt), "
strSql = strSql & " TO_CHAR(trn_dt,'Day'),   "
strSql = strSql & "  store_cd, COUNT(*) FROM (  "
strSql = strSql & " SELECT DISTINCT P.trn_dt trn_dt, P.store_cd store_cd, "
strSql = strSql & " P.trn_time trn_time, P.trn_num trn_num, P.term_num term_num  "
strSql = strSql & " FROM pos_trn_ln P, gm_itm I, gm_sku S                        "
strSql = strSql & " WHERE P.ln_tp IN ('SAL','RET') AND P.sku_num = S.sku_num     "
strSql = strSql & " AND S.itm_cd = I.itm_cd             "
strSql = strSql & " AND (I.subclass_cd <> '999999' OR S.sku_num = '999990-000')  "
strSql = strSql & " AND P.void IS NULL AND P.stat_cd = 'V'  "
strSql = strSql & " AND (ABS(P.eff_AMT)/decode(P.qty, 0, 1, P.qty)) < " & strAmountLimit & " "
strSql = strSql & " AND P.trn_dt = '" & strAvgTrnDateFrom & "'  ) "
strSql = strSql & " GROUP BY trn_dt, store_cd        "
'Get Units
strSql1 = " SELECT GR_UTIL.WEEK_NO(trn_dt),    "
strSql1 = strSql1 & " GR_UTIL.YEAR_NO(trn_dt), "
strSql1 = strSql1 & " TO_CHAR(trn_dt,'Day'),   "
strSql1 = strSql1 & " I.dept_cd dept_cd,       "
strSql1 = strSql1 & " P.store_cd store_cd, SUM(P.qty)        "
strSql1 = strSql1 & " FROM pos_trn_ln P, gm_itm I, gm_sku S  "
strSql1 = strSql1 & " WHERE P.sku_num = S.sku_num        "
strSql1 = strSql1 & " AND S.itm_cd = I.itm_cd            "
strSql1 = strSql1 & " AND P.ln_tp IN ('SAL','RET')       "
strSql1 = strSql1 & " AND P.void IS NULL AND P.stat_cd = 'V' "
strSql1 = strSql1 & " AND P.trn_dt = '" & strAvgTrnDateFrom & "'  "
strSql1 = strSql1 & " AND (I.subclass_cd <> '999999' OR S.sku_num = '999990-000') "
strSql1 = strSql1 & " AND (ABS(P.eff_AMT)/decode(P.qty, 0, 1, P.qty)) < " & strAmountLimit & " "
strSql1 = strSql1 & " GROUP BY trn_dt, dept_cd, store_cd  "
'Get stores' and dept trans counts
strSql2 = " SELECT GR_UTIL.WEEK_NO(trn_dt),   "
strSql2 = strSql2 & " GR_UTIL.YEAR_NO(trn_dt), "
strSql2 = strSql2 & " TO_CHAR(trn_dt,'Day'),   "
strSql2 = strSql2 & " dept_cd,                 "
strSql2 = strSql2 & " store_cd, COUNT(*) FROM (  "
strSql2 = strSql2 & " SELECT DISTINCT P.trn_dt trn_dt, P.store_cd store_cd, I.dept_cd dept_cd,"
strSql2 = strSql2 & " P.trn_time trn_time, P.trn_num trn_num, P.term_num term_num "
strSql2 = strSql2 & " FROM pos_trn_ln P, gm_itm I, gm_sku S                       "
strSql2 = strSql2 & " WHERE P.ln_tp IN ('SAL','RET') AND P.sku_num = S.sku_num    "
strSql2 = strSql2 & " AND S.itm_cd = I.itm_cd             "
strSql2 = strSql2 & " AND P.void IS NULL AND P.stat_cd = 'V'  "
strSql2 = strSql2 & " AND (ABS(P.eff_AMT)/decode(P.qty, 0, 1, P.qty)) < " & strAmountLimit & " "
strSql2 = strSql2 & " AND (I.subclass_cd <> '999999' OR S.sku_num = '999990-000') "
strSql2 = strSql2 & " AND P.trn_dt = '" & strAvgTrnDateFrom & "'  ) "
strSql2 = strSql2 & " GROUP BY trn_dt, dept_cd, store_cd"

Else
'Get stores' trans counts
strSql = " SELECT GR_UTIL.WEEK_NO(trn_dt),   "
strSql = strSql & " GR_UTIL.YEAR_NO(trn_dt), "
strSql = strSql & " TO_CHAR(trn_dt,'Day'),   "
strSql = strSql & " store_cd, COUNT(*) FROM (  "
strSql = strSql & " SELECT DISTINCT P.trn_dt trn_dt, P.store_cd store_cd,       "
strSql = strSql & " P.trn_time trn_time, P.trn_num trn_num, P.term_num term_num "
strSql = strSql & " FROM pos_trn_ln P, gm_itm I, gm_sku S                       "
strSql = strSql & " WHERE P.ln_tp IN ('SAL','RET') AND P.sku_num = S.sku_num    "
strSql = strSql & " AND S.itm_cd = I.itm_cd             "
strSql = strSql & " AND P.void IS NULL AND P.stat_cd = 'V'  "
strSql = strSql & " AND (ABS(P.eff_AMT)/decode(P.qty, 0, 1, P.qty)) < " & strAmountLimit & " "
strSql = strSql & " AND (I.subclass_cd <> '999999' OR S.sku_num = '999990-000') "
strSql = strSql & " AND P.trn_dt BETWEEN '" & strAvgTrnDateFrom
strSql = strSql & "' AND '" & strAvgTrnDateTo & "' )  "
strSql = strSql & " GROUP BY trn_dt, store_cd         "
'Get Units
strSql1 = " SELECT GR_UTIL.WEEK_NO(trn_dt),   "
strSql1 = strSql1 & " GR_UTIL.YEAR_NO(trn_dt), "
strSql1 = strSql1 & " TO_CHAR(trn_dt,'Day'),   "
strSql1 = strSql1 & " I.dept_cd dept_cd,  "
strSql1 = strSql1 & " P.store_cd store_cd, SUM(P.qty)    "
strSql1 = strSql1 & " FROM pos_trn_ln P, gm_itm I, gm_sku S  "
strSql1 = strSql1 & " WHERE P.sku_num = S.sku_num        "
strSql1 = strSql1 & " AND S.itm_cd = I.itm_cd            "
strSql1 = strSql1 & " AND P.ln_tp IN ('SAL','RET')       "
strSql1 = strSql1 & " AND P.void IS NULL AND P.stat_cd = 'V' "
strSql1 = strSql1 & " AND trn_dt BETWEEN '" & strAvgTrnDateFrom
strSql1 = strSql1 & "' AND '" & strAvgTrnDateTo & "'  "
strSql1 = strSql1 & " AND (I.subclass_cd <> '999999' OR S.sku_num = '999990-000') "
strSql1 = strSql1 & " AND (ABS(P.eff_AMT)/decode(P.qty, 0, 1, P.qty)) < " & strAmountLimit & " "
strSql1 = strSql1 & " GROUP BY trn_dt, dept_cd, store_cd  "
'Get stores' and dept trans counts
strSql2 = " SELECT GR_UTIL.WEEK_NO(trn_dt),   "
strSql2 = strSql2 & " GR_UTIL.YEAR_NO(trn_dt), "
strSql2 = strSql2 & " TO_CHAR(trn_dt,'Day'),   "
strSql2 = strSql2 & " dept_cd,                 "
strSql2 = strSql2 & " store_cd, COUNT(*) FROM (  "
strSql2 = strSql2 & " SELECT DISTINCT P.trn_dt trn_dt, P.store_cd store_cd, I.dept_cd dept_cd,"
strSql2 = strSql2 & " P.trn_time trn_time, P.trn_num trn_num, P.term_num term_num "
strSql2 = strSql2 & " FROM pos_trn_ln P, gm_itm I, gm_sku S                       "
strSql2 = strSql2 & " WHERE P.ln_tp IN ('SAL','RET') AND P.sku_num = S.sku_num    "
strSql2 = strSql2 & " AND S.itm_cd = I.itm_cd             "
strSql2 = strSql2 & " AND P.void IS NULL AND P.stat_cd = 'V'  "
strSql2 = strSql2 & " AND (ABS(P.eff_AMT)/decode(P.qty, 0, 1, P.qty)) < " & strAmountLimit & " "
strSql2 = strSql2 & " AND (I.subclass_cd <> '999999' OR S.sku_num = '999990-000') "
strSql2 = strSql2 & " AND P.trn_dt BETWEEN '" & strAvgTrnDateFrom
strSql2 = strSql2 & "' AND '" & strAvgTrnDateTo & "' )  "
strSql2 = strSql2 & " GROUP BY trn_dt, dept_cd, store_cd"

End If


sSpc = " " 'a single space
strAvgTrnWeek = "Week "
strAvgTrnDept = "No Dept"
strAvgTrnDept1 = "Dept "
strAvgTrnStore = "Store "
strAvgTrnFY = "FY"
strAvgTrnActual = "Actual"
strAvgTrnMeas1 = "Transactions"
strAvgTrnMeas2 = "Units sold"
strAvgTrnMeas3 = "Dept Trans"

ConvrtFile = FreeFile

LogTransaction ("Opening output file: " & strFname_sls)
Open strFname_sls For Output As #ConvrtFile
LogTransaction ("Executing SQL to extract transaction counts: " & strSql)
Set rs = cn.Execute(strSql)
LogTransaction ("Writing results to file")
While Not rs.EOF
 Print #ConvrtFile, _
   strAvgTrnWeek & rs.Fields(0) & sSpc & Trim(rs.Fields(2)) & "," & strAvgTrnDept; _
   "," & strAvgTrnStore & rs.Fields(3) & "," & strAvgTrnMeas1 & "," & strAvgTrnFY & rs.Fields(1); _
   "," & strAvgTrnActual & "," & rs.Fields(4)
 rs.MoveNext
Wend
LogTransaction ("Executing SQL to extract unit counts: " & strSql1)
Set rs = cn.Execute(strSql1)
LogTransaction ("Writing results to file")
While Not rs.EOF
 Print #ConvrtFile, _
   strAvgTrnWeek & rs.Fields(0) & sSpc & Trim(rs.Fields(2)) & "," & strAvgTrnDept1 & rs.Fields(3); _
   "," & strAvgTrnStore & rs.Fields(4) & "," & strAvgTrnMeas2 & "," & strAvgTrnFY & rs.Fields(1); _
   "," & strAvgTrnActual & "," & rs.Fields(5)
 rs.MoveNext
Wend
LogTransaction ("Executing SQL to extract dept trans counts: " & strSql2)
Set rs = cn.Execute(strSql2)
While Not rs.EOF
 Print #ConvrtFile, _
   strAvgTrnWeek & rs.Fields(0) & sSpc & Trim(rs.Fields(2)) & "," & strAvgTrnDept1 & rs.Fields(3); _
   "," & strAvgTrnStore & rs.Fields(4) & "," & strAvgTrnMeas3 & "," & strAvgTrnFY & rs.Fields(1); _
   "," & strAvgTrnActual & "," & rs.Fields(5)
 rs.MoveNext
Wend

LogTransaction ("Closing output file")
Close #ConvrtFile                'close file
'Set rs = Nothing
LogTransaction ("Closing query result set")
rs.Close
LogTransaction ("Closing GERS connection")
cn.Close
LogTransaction ("Returning mouse cursor to default")
frmAvgTran.MousePointer = Default

Exit Sub

'Error Handling
ErrorLogging:
    LogError (Err.Number & " " & Err.Description)
    LogTransaction (Err.Number & " " & Err.Description)
    EndLogging
    Unload Me
    Err.Raise Err.Number, Err.Source, Err.Description, Err.HelpFile, Err.HelpContext 'create runtime error
End Sub

Private Sub Form_Load()
InitializeLogging (App.Path & "\GERS2HYP.ini") 'read ini and open logs, if necessary
LogTransaction ("Average Transaction Form loaded")

DTPickerAvgTrn1.Value = Now - 1
DTPickerAvgTrn2.Value = Now - 1
End Sub

