VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmRawSales 
   Caption         =   "Raw Sales"
   ClientHeight    =   2850
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5205
   LinkTopic       =   "Form1"
   ScaleHeight     =   2850
   ScaleWidth      =   5205
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton CmdRawSalesExit 
      Caption         =   "E&xit"
      Height          =   495
      Left            =   2040
      TabIndex        =   5
      Top             =   2040
      Width           =   1215
   End
   Begin VB.CommandButton cmdRawSalesRun 
      Caption         =   "&Run"
      Height          =   495
      Left            =   2040
      TabIndex        =   4
      Top             =   1200
      Width           =   1215
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   375
      Left            =   3480
      TabIndex        =   1
      Top             =   480
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   661
      _Version        =   393216
      Format          =   22675457
      CurrentDate     =   37297
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   375
      Left            =   960
      TabIndex        =   0
      Top             =   480
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   661
      _Version        =   393216
      Format          =   22675457
      CurrentDate     =   37297
   End
   Begin VB.Label lblRawSalesTo 
      Caption         =   "To:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   3000
      TabIndex        =   3
      Top             =   480
      Width           =   375
   End
   Begin VB.Label lblRawSalesFrom 
      Caption         =   "From:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   480
      Width           =   615
   End
End
Attribute VB_Name = "frmRawSales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdRawSalesExit_Click()
LogTransaction ("Unloading Raw Sales form")
EndLogging

Unload Me
End Sub

Private Sub cmdRawSalesRun_Click()
'Program writes 2 files, rawsales.txt, exception.txt
'rawsales.txt has normal sales data.
'exception.txt has exception sales data.
'these sku_nums are not in the above 2 reports.
'Data come from PIPE_SA_TRN, GM_ITM, GM_SKU

Dim sINIFile As String
Dim strUserID As String
Dim strPasswd As String
Dim strConn As String
Dim strProvider As String
Dim strsales_sls As String
Dim strsalesexptn_sls As String
Dim strAmountLimit As String

On Error GoTo ErrorLogging

'location of the ini file
sINIFile = App.Path & "\GERS2HYP.ini"

'Read the GERS user login name from ini file
strUserID = sGetINI(sINIFile, "DATABASE SETTING", "Username", "?")
If strUserID = "?" Then
    'No username
    strUserID = InputBox$("Enter GERS user login name, please :")
    writeINI sINIFile, "DATABASE SETTING", "USERNAME", strUserID
End If

'Read the GERS password from ini file
strPasswd = sGetINI(sINIFile, "DATABASE SETTING", "PASSWORD", "?")
If strPasswd = "?" Then
    'No password
    strPasswd = InputBox$("Enter GERS user password, please :")
    writeINI sINIFile, "DATABASE SETTING", "PASSWORD", strPasswd
End If

'Read the GERS database connection from ini file
strConn = sGetINI(sINIFile, "DATABASE SETTING", "CONNECTION", "?")
If strConn = "?" Then
    'No DATABASE
    strConn = InputBox$("Enter GERS database server, please :")
    writeINI sINIFile, "DATABASE SETTING", "CONNECTION", strConn
End If

'Read the GERS database provider from ini file
strProvider = sGetINI(sINIFile, "DATABASE SETTING", "PROVIDER", "?")
If strProvider = "?" Then
    'No PROVIDER
    strProvider = InputBox$("Enter your database provider, please :")
    writeINI sINIFile, "DATABASE SETTING", "PROVIDER", strProvider
End If

'Read the sales extract location from ini file
strsales_sls = sGetINI(sINIFile, "EXTRACT FILE LOCATION", "SALES DIR", "?")
If strsales_sls = "?" Then
    'No file location
    strsales_sls = InputBox$("Enter Sales File Location as C: , please :")
    writeINI sINIFile, "EXTRACT FILE LOCATION", "SALES DIR", strsales_sls
End If

'Read the sales exception extract location from ini file
strsalesexptn_sls = sGetINI(sINIFile, "EXTRACT FILE LOCATION", "SALES EXCEPTION DIR", "?")
If strsalesexptn_sls = "?" Then
    'No file location
    strsalesexptn_sls = InputBox$("Enter Exception File Location as C: , please :")
    writeINI sINIFile, "EXTRACT FILE LOCATION", "SALES EXCEPTION DIR", strsalesexptn_sls
End If

'Read the amount limit from ini file
strAmountLimit = sGetINI(sINIFile, "DATA", "AMOUNT LIMIT", "?")
If strAmountLimit = "?" Then
    'No amount limit
    strAmountLimit = InputBox$("Enter Amount Limit: , please :")
    writeINI sINIFile, "DATA", "AMOUNT LIMIT", strAmountLimit
End If

LogTransaction ("Running Raw Sales extract")

Dim strDateFrom As String
Dim strDateTo As String

strDateFrom = Format(DTPicker1.Value, "Medium date")
strDateTo = Format(DTPicker2.Value, "Medium date")

LogTransaction ("Changing mouse pointer to 11")
frmRawSales.MousePointer = 11
LogTransaction ("Connecting to GERS")
Set cn = New ADODB.Connection

strcn1 = "Provider=" & strProvider & ";Password=" & strPasswd
strcn1 = strcn1 & ";User ID=" & strUserID
strcn1 = strcn1 & ";Data Source=" & strConn
strcn1 = strcn1 & ";Persist Security Info=True "

LogTransaction ("Connection string: " & strcn1)
cn.Open strcn1

'Check if Date ranges are used or one day
If strDateFrom = strDateTo Then
'One day of data
strSql = " SELECT GR_UTIL.WEEK_NO(trn_dt), "
strSql = strSql & " TO_CHAR(trn_dt,'Day'),   "
strSql = strSql & " dept_cd, store_cd, "
strSql = strSql & " decode(trn_tp,'SAL','Gross Sales','RET','Returns'),  "
strSql = strSql & " GR_UTIL.YEAR_NO(trn_dt), "
strSql = strSql & " -1* ext_prc extprc FROM (  "
strSql = strSql & " SELECT P.trn_dt trn_dt, I.dept_cd dept_cd, "
strSql = strSql & " P.store_cd store_cd, P.trn_tp trn_tp, "
strSql = strSql & " SUM(P.ext_prc) ext_prc "
strSql = strSql & " FROM pipe_sa_trn P, gm_itm I, gm_sku S "
strSql = strSql & " WHERE P.sku_num = S.sku_num    "
strSql = strSql & " AND S.itm_cd = I.itm_cd        "
strSql = strSql & " AND P.trn_tp IN ('SAL','RET')  "
strSql = strSql & " AND P.trn_dt = '" & strDateFrom & "'     "
strSql = strSql & " AND (I.subclass_cd <> '999999' OR S.sku_num = '999990-000') "
strSql = strSql & " AND abs((P.ext_prc/decode(P.qty, 0, 1, P.qty))) < " & strAmountLimit & " "
strSql = strSql & " GROUP BY trn_dt, dept_cd, store_cd, trn_tp )"
Else
strSql = " SELECT GR_UTIL.WEEK_NO(trn_dt), "
strSql = strSql & " TO_CHAR(trn_dt,'Day'),   "
strSql = strSql & " dept_cd, store_cd, "
strSql = strSql & " decode(trn_tp,'SAL','Gross Sales','RET','Returns'),  "
strSql = strSql & " GR_UTIL.YEAR_NO(trn_dt), "
strSql = strSql & " -1 * ext_prc ext_prc FROM (  "
strSql = strSql & " SELECT P.trn_dt trn_dt, I.dept_cd dept_cd, "
strSql = strSql & " P.store_cd store_cd, P.trn_tp trn_tp, "
strSql = strSql & " SUM(P.ext_prc) ext_prc "
strSql = strSql & " FROM pipe_sa_trn P, gm_itm I, gm_sku S "
strSql = strSql & " WHERE P.sku_num = S.sku_num    "
strSql = strSql & " AND S.itm_cd = I.itm_cd        "
strSql = strSql & " AND P.trn_tp IN ('SAL','RET')  "
strSql = strSql & " AND P.trn_dt BETWEEN '" & strDateFrom & "' AND '" & strDateTo & "'  "
strSql = strSql & " AND (I.subclass_cd <> '999999' OR S.sku_num = '999990-000') "
strSql = strSql & " AND abs((P.ext_prc/decode(P.qty, 0, 1, P.qty))) < " & strAmountLimit & " "
strSql = strSql & " GROUP BY trn_dt, dept_cd, store_cd, trn_tp )"

End If

Dim strSpace As String
Dim strWeek As String
Dim strDept As String
Dim strStore As String
Dim strFY As String
Dim strActual As String

sSpc = " " 'a single space
strWeek = "Week "
strDept = "Dept "
strStore = "Store "
strFY = "FY"
strActual = "Actual"

strFileName = Mid(Replace(strFileName, "-", "_"), 1, 14)
ConvrtFile = FreeFile
v_filename = Now()
str_fname = Mid(Replace(v_filename, "/", "_"), 1, 14)
str_fname = Mid(Replace(str_fname, " ", "_"), 1, 14)
str_fname = Mid(Replace(str_fname, ":", "_"), 1, 14)

LogTransaction ("Opening output file: " & strsales_sls)
Open strsales_sls For Output As #ConvrtFile
LogTransaction ("Executing SQL to extract raw sales: " & strSql)
Set rs = cn.Execute(strSql)
LogTransaction ("Writing results to file")
While Not rs.EOF
 Print #ConvrtFile, _
   strWeek & rs.Fields(0) & sSpc & Trim(rs.Fields(1)) & "," & strDept & rs.Fields(2); _
   "," & strStore & rs.Fields(3) & "," & rs.Fields(4) & "," & strFY & rs.Fields(5); _
   "," & strActual & "," & rs.Fields(6)
 rs.MoveNext
Wend

LogTransaction ("Closing output file")
Close #ConvrtFile                'close file

'Exception lines
If strDateFrom = strDateTo Then
'One day of data
strSql = " SELECT P.trn_dt trn_dt,  "
strSql = strSql & " P.store_cd store_cd, P.trn_tp trn_tp,  "
strSql = strSql & " P.sku_num, P.qty, P.ext_prc ext_prc    "
strSql = strSql & " FROM pipe_sa_trn P, gm_itm I, gm_sku S "
strSql = strSql & " WHERE P.sku_num = S.sku_num    "
strSql = strSql & " AND S.itm_cd = I.itm_cd        "
strSql = strSql & " AND P.trn_tp IN ('SAL','RET')  "
strSql = strSql & " AND P.trn_dt = '" & strDateFrom & "'     "
strSql = strSql & " AND (I.subclass_cd = '999999' OR S.sku_num <> '999990-000') "
strSql = strSql & " AND abs((P.ext_prc/decode(P.qty, 0, 1, P.qty))) >= " & strAmountLimit & " "

Else

strSql = " SELECT P.trn_dt trn_dt,  "
strSql = strSql & " P.store_cd store_cd, P.trn_tp trn_tp,  "
strSql = strSql & " P.sku_num, P.qty, P.ext_prc ext_prc    "
strSql = strSql & " FROM pipe_sa_trn P, gm_itm I, gm_sku S "
strSql = strSql & " WHERE P.sku_num = S.sku_num    "
strSql = strSql & " AND S.itm_cd = I.itm_cd        "
strSql = strSql & " AND P.trn_tp IN ('SAL','RET')  "
strSql = strSql & " AND P.trn_dt BETWEEN '" & strDateFrom & "' AND '" & strDateTo & "'  "
strSql = strSql & " AND (I.subclass_cd = '999999' OR S.sku_num <> '999990-000') "
strSql = strSql & " AND abs((P.ext_prc/decode(P.qty, 0, 1, P.qty))) >= " & strAmountLimit & " "

End If

'File name should not be overwritten
ConvrtFile = FreeFile

today = Format(Now, "m_d_yyyy h.mm AM/PM")

strsalesexptn_sls = strsalesexptn_sls & "exception" & today & ".txt"

LogTransaction ("Opening exception report file: " & strsalesexptn_sls)
Open strsalesexptn_sls For Output As #ConvrtFile
LogTransaction ("Executing SQL to grab exceptions: " & strSql)
Set rs = cn.Execute(strSql)
LogTransaction ("Writing results to file")
While Not rs.EOF
 Print #ConvrtFile, _
   rs.Fields(0) & "," & sSpc & Trim(rs.Fields(1)) & "," & rs.Fields(2); _
   "," & rs.Fields(3) & "," & rs.Fields(4) & "," & rs.Fields(5)
 rs.MoveNext
Wend

LogTransaction ("Closing exception report file")
Close #ConvrtFile                'close file
'Set rs = Nothing

LogTransaction ("Closing query result set")
rs.Close
LogTransaction ("Closing GERS connection")
cn.Close
LogTransaction ("Returning mouse cursor to default")
frmRawSales.MousePointer = Default

Exit Sub

'Error Handling
ErrorLogging:
    LogError (Err.Number & " " & Err.Description)
    LogTransaction (Err.Number & " " & Err.Description)
    EndLogging
    Unload Me
    Err.Raise Err.Number, Err.Source, Err.Description, Err.HelpFile, Err.HelpContext 'create runtime error
End Sub

Private Sub Form_Load()
InitializeLogging (App.Path & "\GERS2HYP.ini") 'read ini and open logs, if necessary
LogTransaction ("Raw Sales Form loaded")

DTPicker1.Value = Now - 1
DTPicker2.Value = Now - 1

End Sub



