VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmGLNet 
   Caption         =   "GL Net Sales$"
   ClientHeight    =   3225
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7020
   LinkTopic       =   "Form1"
   ScaleHeight     =   3225
   ScaleWidth      =   7020
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdGlNetRun 
      Caption         =   "&Run"
      Height          =   495
      Left            =   2640
      TabIndex        =   5
      Top             =   1680
      Width           =   1215
   End
   Begin VB.CommandButton cmdGlNetExit 
      Caption         =   "E&xit"
      Height          =   495
      Left            =   2640
      TabIndex        =   4
      Top             =   2520
      Width           =   1215
   End
   Begin MSComCtl2.DTPicker DTPickerGlNet2 
      Height          =   375
      Left            =   4080
      TabIndex        =   1
      Top             =   840
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      _Version        =   393216
      Format          =   20250625
      CurrentDate     =   37298
   End
   Begin MSComCtl2.DTPicker DTPickerGlNet1 
      Height          =   375
      Left            =   1080
      TabIndex        =   0
      Top             =   840
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      _Version        =   393216
      Format          =   20250625
      CurrentDate     =   37298
   End
   Begin VB.Label lblglnetTo 
      Caption         =   "To"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3480
      TabIndex        =   3
      Top             =   840
      Width           =   375
   End
   Begin VB.Label lblGlnetFrom 
      Caption         =   "From"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   240
      TabIndex        =   2
      Top             =   840
      Width           =   615
   End
End
Attribute VB_Name = "frmGLNet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdGlNetExit_Click()
LogTransaction ("Unloading GL Net form")
EndLogging

Unload Me
End Sub

Private Sub cmdGlNetRun_Click()
'Write GL Net Sales data into a text file
'
'Data come from JE, JE_LN
On Error GoTo ErrorLogging

LogTransaction ("Running GL Net extract")

Dim strGlNetDateFrom As String
Dim strGlNetDateTo As String

strGlNetDateFrom = Format(DTPickerGlNet1.Value, "Medium date")
strGlNetDateTo = Format(DTPickerGlNet2.Value, "Medium date")

strUserId = "hottopic"
strPasswd = "hottopic"
strConn = "genret"
'strConn = "nurev"
LogTransaction ("Changing mouse pointer to 11")
frmGLNet.MousePointer = 11
LogTransaction ("Connecting to GERS")
Set cn = New ADODB.Connection

strcn1 = "Provider=MSDAORA.1;Password=" & strPasswd & ";User ID=" & strUserId & ";Data Source="
strcn1 = strcn1 & strConn & ";Persist Security Info=True "
LogTransaction ("Connection string: " & strcn1)
 cn.Open strcn1

'Check if Date ranges are used or one day
If strGlNetDateFrom = strGlNetDateTo Then
'One day of data
'strSql = "SELECT GR_UTIL.WEEK_NO(E.post_dt),   "
'strSql = strSql & " TO_CHAR(E.post_dt,'Day'),   "
'strSql = strSql & " L.seg1_acct_cd, GR_UTIL.YEAR_NO(E.post_dt), SUM(amt) amt "
'strSql = strSql & " FROM je E, je_ln L "
'strSql = strSql & " WHERE E.JE_NUM = L.JE_NUM  "
'strSql = strSql & " AND post_dt = '" & strGlNetDateFrom & "'     "
'strSql = strSql & " AND base_acct_cd = '4000'  "
'strSql = strSql & " GROUP BY E.post_dt, L.seg1_acct_cd  "
'Modified 05/01/2002
strSql = "SELECT GR_UTIL.WEEK_NO(A.post_dt),   "
strSql = strSql & " TO_CHAR(A.post_dt,'Day'),  "
strSql = strSql & " A.seg1_acct_cd, GR_UTIL.YEAR_NO(A.post_dt),      "
strSql = strSql & " SUM(nvl(B.amt,0) - nvl(C.amt,0)) amt             "
strSql = strSql & " FROM (SELECT DISTINCT L.seg1_acct_cd, E.post_dt  "
strSql = strSql & " FROM je E, je_ln L        "
strSql = strSql & " WHERE E.JE_NUM = L.JE_NUM "
strSql = strSql & " AND post_dt  = '" & strGlNetDateFrom & "'       "
strSql = strSql & " AND L.base_acct_cd = '4000') A,                 "
strSql = strSql & " (SELECT L.seg1_acct_cd, E.post_dt, SUM(amt) amt "
strSql = strSql & " FROM je E, je_ln L "
strSql = strSql & " WHERE E.JE_NUM = L.JE_NUM "
strSql = strSql & " AND post_dt = '" & strGlNetDateFrom & "'     "
strSql = strSql & " AND L.base_acct_cd = '4000' "
strSql = strSql & " AND L.dr_cr_cd = 'C' "
strSql = strSql & " GROUP BY E.post_dt, L.seg1_acct_cd ) B, "
strSql = strSql & " (SELECT L.seg1_acct_cd, E.post_dt, SUM(amt) amt "
strSql = strSql & " FROM je E, je_ln L "
strSql = strSql & " WHERE E.JE_NUM = L.JE_NUM "
strSql = strSql & " AND post_dt = '" & strGlNetDateFrom & "'     "
strSql = strSql & " AND L.base_acct_cd = '4000' "
strSql = strSql & " AND L.dr_cr_cd = 'D'  "
strSql = strSql & " GROUP BY E.post_dt, L.seg1_acct_cd ) C "
strSql = strSql & " WHERE A.seg1_acct_cd = B.seg1_acct_cd(+) "
strSql = strSql & " AND   A.seg1_acct_cd = C.seg1_acct_cd(+) "
strSql = strSql & " AND   A.post_dt = B.post_dt(+) "
strSql = strSql & " AND   A.post_dt = C.post_dt(+) "
strSql = strSql & " GROUP BY A.post_dt, A.seg1_acct_cd  "


Else
'strSql = "SELECT GR_UTIL.WEEK_NO(E.post_dt),   "
'strSql = strSql & " TO_CHAR(E.post_dt,'Day'),   "
'strSql = strSql & " L.seg1_acct_cd, GR_UTIL.YEAR_NO(E.post_dt), SUM(amt) amt "
'strSql = strSql & " FROM je E, je_ln L "
'strSql = strSql & " WHERE E.JE_NUM = L.JE_NUM  "
'strSql = strSql & " AND post_dt BETWEEN '" & strGlNetDateFrom & "' AND '" & strGlNetDateTo & "'  "
'strSql = strSql & " AND base_acct_cd = '4000'  "
'strSql = strSql & " GROUP BY E.post_dt, L.seg1_acct_cd  "
'Modified 05/01/2002
strSql = "SELECT GR_UTIL.WEEK_NO(A.post_dt),   "
strSql = strSql & " TO_CHAR(A.post_dt,'Day'),  "
strSql = strSql & " A.seg1_acct_cd, GR_UTIL.YEAR_NO(A.post_dt),      "
strSql = strSql & " SUM(nvl(B.amt,0) - nvl(C.amt,0)) amt             "
strSql = strSql & " FROM (SELECT DISTINCT L.seg1_acct_cd, E.post_dt  "
strSql = strSql & " FROM je E, je_ln L        "
strSql = strSql & " WHERE E.JE_NUM = L.JE_NUM "
strSql = strSql & " AND post_dt BETWEEN '" & strGlNetDateFrom & "' AND '" & strGlNetDateTo & "'  "
strSql = strSql & " AND L.base_acct_cd = '4000') A,                 "
strSql = strSql & " (SELECT L.seg1_acct_cd, E.post_dt, SUM(amt) amt "
strSql = strSql & " FROM je E, je_ln L "
strSql = strSql & " WHERE E.JE_NUM = L.JE_NUM "
strSql = strSql & " AND post_dt BETWEEN '" & strGlNetDateFrom & "' AND '" & strGlNetDateTo & "'  "
strSql = strSql & " AND L.base_acct_cd = '4000' "
strSql = strSql & " AND L.dr_cr_cd = 'C' "
strSql = strSql & " GROUP BY E.post_dt, L.seg1_acct_cd ) B, "
strSql = strSql & " (SELECT L.seg1_acct_cd, E.post_dt, SUM(amt) amt "
strSql = strSql & " FROM je E, je_ln L "
strSql = strSql & " WHERE E.JE_NUM = L.JE_NUM "
strSql = strSql & " AND post_dt BETWEEN '" & strGlNetDateFrom & "' AND '" & strGlNetDateTo & "'  "
strSql = strSql & " AND L.base_acct_cd = '4000' "
strSql = strSql & " AND L.dr_cr_cd = 'D'  "
strSql = strSql & " GROUP BY E.post_dt, L.seg1_acct_cd ) C "
strSql = strSql & " WHERE A.seg1_acct_cd = B.seg1_acct_cd(+) "
strSql = strSql & " AND   A.seg1_acct_cd = C.seg1_acct_cd(+) "
strSql = strSql & " AND   A.post_dt = B.post_dt(+) "
strSql = strSql & " AND   A.post_dt = C.post_dt(+) "
strSql = strSql & " GROUP BY A.post_dt, A.seg1_acct_cd  "
strSql = strSql & " ORDER BY A.post_dt, A.seg1_acct_cd  "

End If


Dim strSpace As String
Dim strGlNetWeek As String
Dim strGlNetDept As String
Dim strGlNetStore As String
Dim strGlNetFY As String
Dim strGlNetActual As String
Dim strGlNetSalesInp As String

sSpc = " " 'a single space
strGlNetWeek = "Week "
strGlNetDept = "No Dept"
strGlNetStore = "Store "
strGlNetFY = "FY"
strGlNetActual = "GL"
strGlNetSalesInp = "Net Sales Input"

strFileName = Mid(Replace(strFileName, "-", "_"), 1, 14)
ConvrtFile = FreeFile
v_filename = Now()
str_fname = Mid(Replace(v_filename, "/", "_"), 1, 14)
str_fname = Mid(Replace(str_fname, " ", "_"), 1, 14)
str_fname = Mid(Replace(str_fname, ":", "_"), 1, 14)

'strFname_sls = "C:\aam\GLNet" & strFileName & str_fname & ".txt"
strFname_sls = "\\Hyperion\Essbase\App\Flash\Flash\glsales.txt"

LogTransaction ("Opening output file: " & strFname_sls)
Open strFname_sls For Output As #ConvrtFile
LogTransaction ("Executing SQL to extract GL Net Sales: " & strSql)
Set rs = cn.Execute(strSql)
LogTransaction ("Writing results to file")
While Not rs.EOF
 Print #ConvrtFile, _
   strGlNetWeek & rs.Fields(0) & sSpc & Trim(rs.Fields(1)) & "," & strGlNetDept; _
   "," & strGlNetStore & rs.Fields(2) & "," & strGlNetSalesInp & "," & strGlNetFY & rs.Fields(3); _
   "," & strGlNetActual & "," & rs.Fields(4)
 rs.MoveNext
Wend

LogTransaction ("Closing output file")
Close #ConvrtFile                'close file
'Set rs = Nothing
LogTransaction ("Closing query result set")
rs.Close
LogTransaction ("Closing GERS connection")
cn.Close
LogTransaction ("Returning mouse cursor to default")
frmGLNet.MousePointer = Default

Exit Sub

'Error Handling
ErrorLogging:
    LogError (Err.Number & " " & Err.Description)
    LogTransaction (Err.Number & " " & Err.Description)
    EndLogging
    Unload Me
    Err.Raise Err.Number, Err.Source, Err.Description, Err.HelpFile, Err.HelpContext 'create runtime error
End Sub

Private Sub Form_Load()
InitializeLogging (App.Path & "\GERS2HYP.ini") 'read ini and open logs, if necessary
LogTransaction ("GL Net Form loaded")

DTPickerGlNet1.Value = Now - 1
DTPickerGlNet2.Value = Now - 1
End Sub
