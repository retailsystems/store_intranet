VERSION 5.00
Begin VB.Form frmAutoRwsales 
   Caption         =   "Raw Sales"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
End
Attribute VB_Name = "frmAutoRwsales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'********************************************************************************
' AutoRwSales
' Description:  This is for Autorun (one day of data)
'
'               Required Microsoft ActiveX Data Objects 2.6 library
'
'               Creates Gross Sales and Returns
'               extract file "rwsales.txt"
'               Text file is "," delimited
'               Data come from PIPE_SA_TRN, GM_ITM, GM_SKU
'********************************************************************************

Private Sub Form_Load()
Dim strUserID As String
Dim strPasswd As String
Dim strConn As String
Dim strProvider As String
Dim sINIFile As String
Dim strAmountLimit As String
Dim strFname_sls As String
Dim strFname_exception  As String
Dim strDateFrom As String
'Dim strDateTo As String

Dim strSpace As String
Dim strWeek As String
Dim strDept As String
Dim strStore As String
Dim strFY As String
Dim strActual As String
Dim today As String

'read ini and open logs, if necessary
InitializeLogging (App.Path & "\AutoRwsales.ini")

'location of the ini file
sINIFile = App.Path & "\AutoRwsales.ini"

LogTransaction ("Application Started")

strDateFrom = Format(Now - 1, "Medium date")

'Read the GERS user login name from ini file
strUserID = sGetINI(sINIFile, "DATABASE SETTING", "Username", "?")
If strUserID = "?" Then
    'No username
    strUserID = InputBox$("Enter GERS user login name, please :")
    writeINI sINIFile, "DATABASE SETTING", "USERNAME", strUserID
End If

'Read the GERS password from ini file
strPasswd = sGetINI(sINIFile, "DATABASE SETTING", "PASSWORD", "?")
If strPasswd = "?" Then
    'No password
    strPasswd = InputBox$("Enter GERS user password, please :")
    writeINI sINIFile, "DATABASE SETTING", "PASSWORD", strPasswd
End If

'Read the GERS database connection from ini file
strConn = sGetINI(sINIFile, "DATABASE SETTING", "CONNECTION", "?")
If strConn = "?" Then
    'No DATABASE
    strConn = InputBox$("Enter your database server, please :")
    writeINI sINIFile, "DATABASE SETTING", "CONNECTION", strConn
End If

'Read the GERS database provider from ini file
strProvider = sGetINI(sINIFile, "DATABASE SETTING", "PROVIDER", "?")
If strProvider = "?" Then
    'No PROVIDER
    strProvider = InputBox$("Enter your database provider, please :")
    writeINI sINIFile, "DATABASE SETTING", "PROVIDER", strProvider
End If

'Read the sales directory location from ini file
strFname_sls = sGetINI(sINIFile, "EXTRACT FILE LOCATION", "SALES DIR", "?")
If strFname_sls = "?" Then
    'No password
    strFname_sls = InputBox$("Enter Sales File Location as C: , please :")
    writeINI sINIFile, "EXTRACT FILE LOCATION", "SALES DIR", strFname_sls
End If

'Read the sales exception directory location from ini file
strFname_exception = sGetINI(sINIFile, "EXTRACT FILE LOCATION", "EXCEPTION DIR", "?")
If strFname_exception = "?" Then
    'No password
    strFname_exception = InputBox$("Enter Sales Exception File Location as C: , please :")
    writeINI sINIFile, "EXTRACT FILE LOCATION", "EXCEPTION DIR", strFname_exception
End If

'Read the amount limit from ini file
strAmountLimit = sGetINI(sINIFile, "DATA", "AMOUNT LIMIT", "?")
If strAmountLimit = "?" Then
    'No password
    strAmountLimit = InputBox$("Enter Amount Limit: , please :")
    writeINI sINIFile, "DATA", "AMOUNT LIMIT", strAmountLimit
End If

LogTransaction ("Changing mouse pointer to 11")
frmAutoRwsales.MousePointer = 11
LogTransaction ("Connecting to GERS")
Set cn = New ADODB.Connection

strcn1 = "Provider=" & strProvider & ";Password=" & strPasswd
strcn1 = strcn1 & ";User ID=" & strUserID
strcn1 = strcn1 & ";Data Source=" & strConn
strcn1 = strcn1 & ";Persist Security Info=True "
cn.Open strcn1

LogTransaction ("Connection string: " & strcn1)

'One day of data
strSql = " SELECT GR_UTIL.WEEK_NO(trn_dt), "
strSql = strSql & " TO_CHAR(trn_dt,'Day'),   "
strSql = strSql & " dept_cd, store_cd, "
strSql = strSql & " decode(trn_tp,'SAL','Gross Sales','RET','Returns'),  "
strSql = strSql & " GR_UTIL.YEAR_NO(trn_dt), "
strSql = strSql & " -1* ext_prc extprc FROM (  "
strSql = strSql & " SELECT P.trn_dt trn_dt, I.dept_cd dept_cd, "
strSql = strSql & " P.store_cd store_cd, P.trn_tp trn_tp, "
strSql = strSql & " SUM(P.ext_prc) ext_prc "
strSql = strSql & " FROM pipe_sa_trn P, gm_itm I, gm_sku S "
strSql = strSql & " WHERE P.sku_num = S.sku_num    "
strSql = strSql & " AND S.itm_cd = I.itm_cd        "
strSql = strSql & " AND P.trn_tp IN ('SAL','RET')  "
strSql = strSql & " AND P.trn_dt = '" & strDateFrom & "'     "
strSql = strSql & " AND (I.subclass_cd <> '999999' OR S.sku_num = '999990-000') "
strSql = strSql & " AND abs((P.ext_prc/decode(P.qty, 0, 1, P.qty))) < " & strAmountLimit & " "
strSql = strSql & " GROUP BY trn_dt, dept_cd, store_cd, trn_tp )"

sSpc = " " 'a single space
strWeek = "Week "
strDept = "Dept "
strStore = "Store "
strFY = "FY"
strActual = "Actual"

ConvrtFile = FreeFile

Open strFname_sls For Output As #ConvrtFile
Set rs = cn.Execute(strSql)
While Not rs.EOF
 Print #ConvrtFile, _
   strWeek & rs.Fields(0) & sSpc & Trim(rs.Fields(1)) & "," & strDept & rs.Fields(2); _
   "," & strStore & rs.Fields(3) & "," & rs.Fields(4) & "," & strFY & rs.Fields(5); _
   "," & strActual & "," & rs.Fields(6)
 rs.MoveNext
Wend

Close #ConvrtFile                'close file

'Exception report
strSql = " SELECT P.trn_dt trn_dt,  "
strSql = strSql & " P.store_cd store_cd, P.trn_tp trn_tp,  "
strSql = strSql & " P.sku_num, P.qty, P.ext_prc ext_prc    "
strSql = strSql & " FROM pipe_sa_trn P, gm_itm I, gm_sku S "
strSql = strSql & " WHERE P.sku_num = S.sku_num    "
strSql = strSql & " AND S.itm_cd = I.itm_cd        "
strSql = strSql & " AND P.trn_tp IN ('SAL','RET')  "
strSql = strSql & " AND P.trn_dt = '" & strDateFrom & "'     "
strSql = strSql & " AND (I.subclass_cd = '999999' OR S.sku_num <> '999990-000') "
strSql = strSql & " AND abs((P.ext_prc/decode(P.qty, 0, 1, P.qty))) >= " & strAmountLimit & " "

'File name should not be overwritten
ConvrtFile = FreeFile

today = Format(Now, "m_d_yyyy h.mm AM/PM")

strFname_exception = strFname_exception & "exception" & today & ".txt"

Open strFname_exception For Output As #ConvrtFile
Set rs = cn.Execute(strSql)
While Not rs.EOF
 Print #ConvrtFile, _
   rs.Fields(0) & "," & sSpc & Trim(rs.Fields(1)) & "," & rs.Fields(2); _
   "," & rs.Fields(3) & "," & rs.Fields(4) & "," & rs.Fields(5)
 rs.MoveNext
Wend

Close #ConvrtFile                'close file
'Set rs = Nothing
rs.Close
cn.Close
frmAutoRwsales.MousePointer = Default

'close form
Unload frmAutoRwsales

End Sub
