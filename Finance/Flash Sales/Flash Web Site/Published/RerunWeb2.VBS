'********************************************************************************
' This script reruns the LW web pages & publishes them
' through UserRoll.exe
'********************************************************************************

option explicit


private const SALES_FILE = "\\bowie\Batch\Flash\Extracts\rwsales.txt"
private const TRANS_FILE = "\\bowie\Batch\Flash\Extracts\avgtrn.txt"
private const ESSBASE_ERR_FILE = "\\bowie\Batch\Flash\EssError.err"
private const WEB_TW = "\\bowie\Batch\Flash\DailyReportRerun\TW.ini"
private const WEB_LW = "\\bowie\Batch\Flash\DailyReportRerun\LW.ini"
private const WEB_RPT = "\\bowie\Batch\Flash\DailyReportRerun\WebRpts.ini"


call main

private sub main
  dim shell
  dim fs
  dim folder
  dim fsz


  on error resume next

  set shell = wscript.createobject("wscript.shell")
  set fs = createobject("scripting.filesystemobject")




   shell.run Chr(34) & "\\bowie\Batch\Flash\DailyReportRerun\lwWebRpts.xls" & Chr(34), 1, true
   If Err.Number <> 0 and err.number <> 53 Then 
     call writelog("Error # " & Err.Number & " running the web report for LW")
   else
     call writelog("LW Web reports ran successfully")
   end if

   call writelog("***** Run Polling.xls *****")  
   shell.run Chr(34) & "\\bowie\batch\flash\DailyReportRerun\polling.xls" & Chr(34), 1, true


  call writelog("***** Publish Web reports *****")  
'  Publish all the Web Reports
  fs.CopyFile "\\bowie\batch\flash\DailyReportRerun\*.*", "\\intranet\flashsalesnew\published\", True
  If Err.Number <> 0 Then 
    call writelog("Error # " & Err.Number & " publishing the summary files.")
    exit sub
  End If 
  fs.CopyFolder "\\bowie\batch\flash\DailyReportRerun\*.*", "\\intranet\flashsalesnew\published\"
  If Err.Number <> 0 Then 
    call writelog("Error # " & Err.Number & " publishing the FOLDERS.")
    exit sub
  End If 

  call writelog("Flash Web Pages have been published")

end sub

private sub writelog(byval message)
  dim filesys, filetxt
  Const ForReading = 1, ForWriting = 2, ForAppending = 8 

  Set filesys = CreateObject("Scripting.FileSystemObject")
  Set filetxt = filesys.OpenTextFile("\\bowie\batch\flash\DailyReportRerun\RerunWeb.log", ForAppending, True) 

  filetxt.writeline(date & " " & time & " " & message)

  filetxt.Close 
end sub

