<%@ Page Language="vb" AutoEventWireup="false" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
	<head>
		<title>Error</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio.NET 7.0">
		<meta name="CODE_LANGUAGE" content="Visual Basic 7.0">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
	</head>
	<body MS_POSITIONING="GridLayout">
		<form id="Form1" method="post" runat="server">
			<table align="center" width="500" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="2"><br>
						<br>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="errStyle">
						<P>Unexpected error occured while processing your request,&nbsp;you 
							may&nbsp;contact your system administrator for further assistance.
						</P>
						<P>
							Click <a href="index.aspx">here</a> to go back.
						</P>
					</td>
				</tr>
				
			</table>
		
		</form>
	</body>
</html>
