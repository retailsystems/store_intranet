'********************************************************************************
' Balancing
' Created:  May 2004
' Updated: 6/16/05 by Cris Tran - removed cebelling and wtrinidad emails
' Updated: 02/22/2006 by Jenny Castaneda - Changed the subject format to 9 
'          characters to fit the screen of the BB
'********************************************************************************

option explicit
	private const WorkingDir = "C:\batch\Flash\FlashEmail\"

	private const HTESS_FILE = "C:\batch\Flash\FlashEmail\Company.txt"
	private const HTESS_FILE1 = "C:\batch\Flash\FlashEmail\Region 01.txt"
	private const HTESS_FILE2 = "C:\batch\Flash\FlashEmail\Region 02.txt"
	private const HTESS_FILE3 = "C:\batch\Flash\FlashEmail\Region 03.txt"
	private const HTESS_FILE4 = "C:\batch\Flash\FlashEmail\Region 04.txt"
	private const HTESS_FILE5 = "C:\batch\Flash\FlashEmail\Region 05.txt"
	private const HTESS_FILE6 = "C:\batch\Flash\FlashEmail\Region 06.txt"
	private const HTESS_FILE7 = "C:\batch\Flash\FlashEmail\Region 07.txt"
	private const HTESS_FILE8 = "C:\batch\Flash\FlashEmail\Region 08.txt"
	private const HTESS_FILE20 = "C:\batch\Flash\FlashEmail\Region 20.txt"
	private const HTESS_FILE21 = "C:\batch\Flash\FlashEmail\Region 21.txt"
	private const HTESS_FILE22 = "C:\batch\Flash\FlashEmail\Region 22.txt"
	private const HTESS_FILEhtr = "C:\batch\Flash\FlashEmail\HotTopic Regions.txt"

	Call clean

private sub clean

	dim fso
	dim shell
	dim folder
	dim txt
	dim pos

	set shell = wscript.createobject("wscript.shell")
	Set fso = CreateObject("Scripting.FileSystemObject")

	'Set folder variable to the working directory
	set folder = fso.getFolder(WorkingDir)
	
	on error resume next

	writelog("Delete blackberry text files.")
	Wscript.echo date & " " & time & " " & "Delete blackberry text files."
	
	'Delete every district txt file
	for each txt in folder.files 
		pos = InStr(LCase(txt.Name),"district")		
		if pos = 1 And fso.FileExists(WorkingDir & txt.Name) then
			fso.DeleteFile WorkingDir & txt.Name	
		end if
	next
	
	writelog("Start creating blackberry text files.")
	Wscript.echo date & " " & time & " " & "Start creating blackberry text files."
	
	shell.run chr(34) & WorkingDir & "DistFlashEmail.xls" & chr(34), 1, true
	
	if (Err.Number <> 0 and Err.Number <> 53) then
		Wscript.echo date & " " & time & " " & "FlashDistrictBBFailure"
		call writelog("Error # " & Err.Number & " failed creating District text files")
		call notify("Error # " & Err.Number & " failed creating District text files", "FlashDistrictBBFailure", "ProdControlNotify@hottopic.com")
		exit sub
	else
		writelog("FlashDistrictBBSuccess")
		Wscript.echo date & " " & time & " " & "FlashDistrictBBSuccess"
	    call notify("District FlashEmail txt has been created", "FlashDistrictBBSuccess", "ProdControlNotify@hottopic.com")
	end if

end sub

private sub notify(byval message, byval subject, byval recipient)
  dim objMessage
  dim dt

  dt = date()-1

  Set objMessage = CreateObject("CDO.Message")
  
  'SMTP server configuration (needed for Windows Server 2003)
  objMessage.Configuration.Fields.Item _ 
  ("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2 
  objMessage.Configuration.Fields.Item _ 
  ("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "filter"
  objMessage.Configuration.Fields.Item _ 
  ("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25 
  objMessage.Configuration.Fields.Update 

  objMessage.Subject = subject & " " & Month(dt)&"/"& Day(dt) & " - CAFLAPRD01"
  objmessage.From = "FlashSupport" 
  objMessage.Sender = "FlashSupport"
  objMessage.To = recipient
  objMessage.TextBody = message
  'objMessage.To = "cdao@hottopic.com;kgatlin@hottopic.com;THeiner@hottopic.com;jyan@hottopic.com;TBeheshti@hottopic.com"
  'objMessage.To = "cdao@hottopic.com"
  'objMessage.TextBody = recipient & message
  'objMessage.TextBody = message
  objMessage.Send
  if err.number <> 0 then
    msgbox err.number
  end if

end sub

private sub writelog(byval message)
  dim filesys, filetxt
  Const ForReading = 1, ForWriting = 2, ForAppending = 8 

  Set filesys = CreateObject("Scripting.FileSystemObject")
  Set filetxt = filesys.OpenTextFile(WorkingDir & "DistFlashEmail.log", ForAppending, True) 

  filetxt.writeline(date & " " & time & " " & message)

  filetxt.Close 
end sub