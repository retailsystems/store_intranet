Attribute VB_Name = "Module2"
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' DECLARATIONS SECTION

Private Const SYNCHRONIZE As Long = &H100000
Private Const INFINITE = &HFFFF 'Wait forever
 
Private Declare Function WaitForSingleObject Lib "KERNEL32" (ByVal hHandle As Long, ByVal dwMilliseconds As Long) As Long
Private Declare Function OpenProcess Lib "KERNEL32" (ByVal dwAccess As Long, ByVal fInherit As Integer, ByVal hObject As Long) As Long
Private Declare Function CloseHandle Lib "KERNEL32" (hObject As Long) As Long


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' CODE SECTION

' Start the indicated program and wait for it
' to finish, hiding while we wait.

Public Sub ShellAndWait(ByVal program_name As String, _
    ByVal window_style As VbAppWinStyle)

    Dim process_id As Long
    Dim process_handle As Long

    ' Start the program.
    On Error GoTo ShellError
    process_id = Shell(program_name, window_style)
    On Error GoTo 0

    DoEvents

    ' Wait for the program to finish.
    ' Get the process handle.
    process_handle = OpenProcess(SYNCHRONIZE, 0, process_id)
    If process_handle <> 0 Then
        WaitForSingleObject process_handle, INFINITE
        CloseHandle process_handle
    End If

    Exit Sub

ShellError:
    Call ShellAndWaitErrors("Error starting task " & program_name & ". " & Err.Description)
    Call Log("Error starting task " & program_name & ". " & Err.Description)
End Sub

Public Sub ShellAndWaitErrors(Message As String)
    Open App.Path & "\" & "MaintErr.Err" For Append As #7
    Write #7, Date & " " & Time & " " & Message
    Close #7
End Sub

Public Sub Log(Message As String)
    Open App.Path & "\" & "DBmaint.log" For Append As #8
    Write #8, Date & " " & Time & " " & Message
    Close #8
End Sub
