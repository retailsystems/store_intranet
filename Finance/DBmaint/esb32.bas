Attribute VB_Name = "Module1"
' Copyright 1994-1999 Hyperion Solutions Corporation. All Rights Reserved.

' RESTRICTED RIGHTS LEGEND:

' Use, duplication, or disclosure by the Government is subject to
' restrictions as set forth in subparagraph (c)(1)(ii) of the Rights
' in Technical Data and Computer Software clause at DFARS 252.227-7013,
' or in the Commercial Computer Software Restricted Rights clause at
' FAR 52.227-19, as applicable.

' Hyperion Solutions Corporation
' 1344 Crossman Avenue, Sunnyvale, CA  94089  USA

'**************************************************************************
'  General Global Constants
'**************************************************************************

Global Const ESB_API_VERSION = &H60000                 ' VB API Version
Global Const ESB_NULL = 0                               ' Null value
'**************************************************************************
' Booleans to be used within user defined types.  Make sure to assign value
' in the "form" part of the main program
' Example:
'          ESB_TRUE = 1
'          ESB_FALSE = 0
'**************************************************************************
Global ESB_TRUE As Integer                           ' Boolean TRUE
Global ESB_FALSE As Integer                          ' Boolean FALSE

'**************************************************************************
' Yes/No flag constants to be used as VB API function parameters
'**************************************************************************
Global Const ESB_YES = 1                                ' Yes
Global Const ESB_NO = 0                                 ' No

'**************************************************************************
' Function return status constant
'**************************************************************************
Global Const ESB_STS_NOERR = 0         ' VB API function normal return value
Global Const ESB_STS_CANCEL = 1        ' Operation cancelled return value

'**************************************************************************
' Constants for Error Level - used by Level argument in eSBErrorMessage
'**************************************************************************
Global Const ESB_LEVEL_INFO = 2        ' Message is for information only
Global Const ESB_LEVEL_WARNING = 3     ' Warning message
Global Const ESB_LEVEL_ERROR = 4       ' Error message
Global Const ESB_LEVEL_SERIOUS = 7     ' Serious error message
Global Const ESB_LEVEL_FATAL = 8       ' Fatal error message

'**************************************************************************
'  Auto Login OPTION constants
'**************************************************************************
Global Const ESB_AUTO_DEFAULT = 0
Global Const ESB_AUTO_NODIALOG = 1
Global Const ESB_AUTO_NOSELECT = 2

'**************************************************************************
'  Dimension Type constants
'**************************************************************************
'Global Const ESB_DIMTYPE_DENSE = 0
'Global Const ESB_DIMTYPE_SPARSE = 1

'**************************************************************************
'  Data Level constants
'**************************************************************************
Global Const ESB_DATA_ALL = 1
Global Const ESB_DATA_LEVEL0 = 2
Global Const ESB_DATA_INPUT = 3

'**************************************************************************
' Options for Local/Remote/SQL file loading (used by BuildDimension)
'**************************************************************************
Global Const ESB_FILE_CLIENT = 0      ' Data file at the client
Global Const ESB_FILE_SERVER = 1      ' Data file at the server
Global Const ESB_FILE_SQL = 2         ' Data file at the SQL server

'**************************************************************************
'  Maximum String Lengths (Including Terminating Null)
'**************************************************************************

Global Const ESB_NAMELEN = 30               ' Max length of a general name
Global Const ESB_USERNAMELEN = 30           ' Max length of a user/group name
Global Const ESB_PASSWORDLEN = 100          ' Max length of a password string
Global Const ESB_SVRNAMELEN = 30            ' Max length of a server name
Global Const ESB_APPNAMELEN = 8             ' Max length of an app name
Global Const ESB_ALIASNAMELEN = 30          ' Max length of an alias tbl name
Global Const ESB_DBNAMELEN = 8              ' Max length of a database name
Global Const ESB_OBJNAMELEN = 8             ' Max length of a file object name
Global Const ESB_MBRNAMELEN = 80            ' Max length of a member name
Global Const ESB_FTRNAMELEN = 30            ' Max length of a filter name
Global Const ESB_PATHLEN = 120              ' Max length of a path name
Global Const ESB_LINELEN = 256              ' Max length of a report line
Global Const ESB_DESCLEN = 80               ' Max length of an application or
                                            ' database description
Global Const ESB_CRDB_MAXDIMNUM = 4         ' Maximum Dimension Number for
                                            ' a Currency DB
Global Const ESB_VARVALUELEN = 256          ' Maximum value of substitution var

Global Const ESB_MBRCOMMENTLEN = 256        ' Maximum value of member comment

'**************************************************************************
'  LRO Global Constants
'**************************************************************************
Global Const ESB_ONAMELEN_API = 512          ' Maximum value of object name
Global Const ESB_LRODESCLEN_API = 80         ' Maximum value of LRO object description
Global Const ESB_LRONOTELEN_API = 600        ' Maximum value of LRO annotation
Global Const ESB_NOSTORE_OBJECT_API = &H1    ' Object not stored to server
Global Const ESB_STORE_OBJECT_API = &H10     ' Object stored to server
Global Const ESB_LRO_OBJ_API = 1             ' Update object only
Global Const ESB_LRO_CATALOG_API = 2         ' Update catalog only
Global Const ESB_LRO_BOTH_API = 3            ' Update object and catalog
Global Const ESB_LROTYPE_CELLNOTE_API = 0    ' Cell note
Global Const ESB_LROTYPE_WINAPP_API = 1      ' Windows app
Global Const ESB_LROTYPE_URL_API = 2         ' URL

'**************************************************************************
'  Partition Constants
'**************************************************************************
Global Const ESB_PARTITION_OP_REPLICATED = &H1   'Replicated region types
Global Const ESB_PARTITION_OP_LINKED = &H2       'Linked region types
Global Const ESB_PARTITION_OP_TRANSPARENT = &H4       'Transparent region types
Global Const ESB_PARTITION_OP_ALL = _
   ESB_PARTITION_OP_REPLICATED + ESB_PARTITION_OP_LINKED + ESB_PARTITION_OP_TRANSPARENT
 
Global Const ESB_PARTITION_DATA_SOURCE = &H1     'Source regions
Global Const ESB_PARTITION_DATA_TARGET = &H2     'Target regions
Global Const ESB_PARTITION_DATA_BOTH = _
   ESB_PARTITION_DATA_SOURCE + ESB_PARTITION_DATA_TARGET
 
Global Const ESB_PARTITION_META_SOURCE = &H1     'Source metadata regions
Global Const ESB_PARTITION_META_TARGET = &H2     'Target metadata regions
Global Const ESB_PARTITION_META_BOTH = _
   ESB_PARTITION_META_SOURCE + ESB_PARTITION_META_TARGET

'**************************************************************************
'  Outline Synchronization Constants
'**************************************************************************

Global Const ESB_PARTITION_OTLDIM_ADD = &H1      'Add dimension
Global Const ESB_PARTITION_OTLDIM_DELETE = &H2   'Delete dimension
Global Const ESB_PARTITION_OTLDIM_UPDATE = &H4   'Dimension dense/sparse change
Global Const ESB_PARTITION_OTLDIM_MOVE = &H8     'Move dimension
Global Const ESB_PARTITION_OTLDIM_RENAME = &H10  'Rename dimension
Global Const ESB_PARTITION_OTLDIM_MBRCHG = &H20  'Alter members in dimension
Global Const ESB_PARTITION_OTLDIM_ALL = _
   ESB_PARTITION_OTLDIM_ADD + ESB_PARTITION_OTLDIM_DELETE + ESB_PARTITION_OTLDIM_UPDATE + _
   ESB_PARTITION_OTLDIM_MOVE + ESB_PARTITION_OTLDIM_RENAME + ESB_PARTITION_OTLDIM_MBRCHG

Global Const ESB_PARTITION_OTLMBR_ADD = &H1      'Add members
Global Const ESB_PARTITION_OTLMBR_DELETE = &H2   'Delete members
Global Const ESB_PARTITION_OTLMBR_RENAME = &H4   'Rename members
Global Const ESB_PARTITION_OTLMBR_MOVE = &H8     'Move members
Global Const ESB_PARTITION_OTLMBR_UPDATE = &H10  'Update existing member
Global Const ESB_PARTITION_OTLMBR_ALL = _
   ESB_PARTITION_OTLMBR_ADD + ESB_PARTITION_OTLMBR_DELETE + ESB_PARTITION_OTLMBR_RENAME + _
   ESB_PARTITION_OTLMBR_MOVE + ESB_PARTITION_OTLMBR_UPDATE

Global Const ESB_PARTITION_OTLMBRATTR_STATUS = &H1  'Status changes
Global Const ESB_PARTITION_OTLMBRATTR_ALIAS = &H2   'Alias changes
Global Const ESB_PARTITION_OTLMBRATTR_UCALC = &H4   'Unary calc symbol changes
Global Const ESB_PARTITION_OTLMBRATTR_ATYPE = &H8   'Account type changes
Global Const ESB_PARTITION_OTLMBRATTR_CCONVERT = &H10  'Currency conversion flag
Global Const ESB_PARTITION_OTLMBRATTR_CRMBRNAME = &H20 'Tagged currency db mbr
Global Const ESB_PARTITION_OTLMBRATTR_UDA = &H40       'User defined attrib changes
Global Const ESB_PARTITION_OTLMBRATTR_CALC = &H80      'Calc formula changes
Global Const ESB_PARTITION_OTLMBRATTR_LEVEL = &H100    'Level number changes
Global Const ESB_PARTITION_OTLMBRATTR_GENERATION = &H200  'Gen number changes
Global Const ESB_PARTITION_OTLMBRATTR_ALL = _
   ESB_PARTITION_OTLMBRATTR_STATUS + ESB_PARTITION_OTLMBRATTR_ALIAS + ESB_PARTITION_OTLMBRATTR_UCALC + _
   ESB_PARTITION_OTLMBRATTR_ATYPE + ESB_PARTITION_OTLMBRATTR_CCONVERT + _
   ESB_PARTITION_OTLMBRATTR_CRMBRNAME + ESB_PARTITION_OTLMBRATTR_UDA + ESB_PARTITION_OTLMBRATTR_CALC + _
   ESB_PARTITION_OTLMBRATTR_LEVEL + ESB_PARTITION_OTLMBRATTR_GENERATION

Global Const ESB_ALLCHG = ESB_PARTITION_OTLMBR_ALL + ESB_PARTITION_OTLDIM_ALL

'**************************************************************************
'   Minimum and maximum page sizes
'

Global Const ESB_INDEXPAGEMIN_SIZE = 1024   ' Minimal index page size
Global Const ESB_INDEXPAGEMAX_SIZE = 8192   ' Maximum index page size

'**************************************************************************
'   Minimum index cache size
'
Global Const ESB_INDEXCACHEMIN_SIZE = 1048576 ' Minimal index cache size

Global Const ESB_RATEINFOLEN = ESB_MBRNAMELEN * ESB_CRDB_MAXDIMNUM

'**************************************************************************
'  GetNextItem Data Type Id
'**************************************************************************

Global Const ESB_USERINFO_TYPE = 1     ' ESB_USERINFO_T (eSBListUsers)
Global Const ESB_GROUPINFO_TYPE = 2    ' ESB_USERINFO_T (eSBListGroups)
Global Const ESB_USERAPP_TYPE = 3      ' ESB_USERAPP_T  (eSBGetApplicationAccess)
Global Const ESB_USERDB_TYPE = 4       ' ESB_USERDB_T   (eSBGetDatabaseAccess)
Global Const ESB_LOCKINFO_TYPE = 5     ' ESB_LOCKINFO_T (eSBListLocks)
Global Const ESB_OBJINFO_TYPE = 6      ' ESB_OBJINFO_T  (eSBListObjects)
Global Const ESB_APPDB_TYPE = 7        ' ESB_APPDB_T    (eSBListDatabases)
Global Const ESB_CAPPDB_TYPE = 8       ' ESB_APPDB_T    (eSBListCurrencyDatabase)
Global Const ESB_APPNAME_TYPE = 9      ' ByVal <var> As String * ESB_APPNAMELEN
                                       ' (eSBListApplications)
Global Const ESB_DBNAME_TYPE = 10      ' ByVal <var> As String * ESB_DBNAMELEN
                                       ' (eSBGetApplicationInfo)
Global Const ESB_GROUPNAME_TYPE = 11   ' ByVal <var> As String * ESB_USERNAMELEN
                                       ' (eSBGetGroupList)
Global Const ESB_FTRNAME_TYPE = 12     ' ByVal <var> As String * ESB_FTRNAMELEN
                                       ' (eSBListFilters)
Global Const ESB_FUSERNAME_TYPE = 13   ' ByVal <var> As String * ESB_USERNAMELEN
                                       ' (eSBGetFilterList)
Global Const ESB_OBJNAME_TYPE = 14     ' ByVal <var> As String * ESB_OBJNAMELEN
                                       ' (eSBGetCalcList)
Global Const ESB_DIMSTATS_TYPE = 15    ' ESB_DIMSTATS_T (eSBGetDatabaseStats)
Global Const ESB_CUSERINFO_TYPE = 16   ' ESB_USERINFO_T (eSBListConnections)
Global Const ESB_LAPPDB_TYPE = 17      ' ESB_APPDB_T (eSBLogin)
Global Const ESB_ALIASNAME_TYPE = 18   ' ESB_ALIASNAME_T (eSBListAliases)
Global Const ESB_MBRALT_TYPE = 19      ' ESB_MBRALT_T    (eSBDisplayAlias)
Global Const ESB_RATEINFO_TYPE = 20    ' ESB_RATEINFO_T  (eSBGetCurrencyRateInfo)
Global Const ESB_OUTLINEINFO_TYPE = 21 ' ByVal <var> As String * ESB_ALIASNAMELEN
                                       ' (EsbOtlGetOutlineInfo)
Global Const ESB_OUTERROR_TYPE = 22    ' ESB_OUTERROR_T  (EsbOtlVerifyOutline)
Global Const ESB_OTLUSERATTR_TYPE = 23 ' ByVal <var> As String * ESB_MBRNAMELEN
                                       ' (EsbOtlGetUserAttributes)
Global Const ESB_APPINFOEX_TYPE = 24   ' ESB_APPINFOEX_T (EsbGetApplicationInfoEx)
Global Const ESB_DBREQINFO_TYPE = 25   ' ESB_DBREQINFO_T (eSBGetDatabaseInfo)
Global Const ESB_DIMINFO_TYPE = 26     ' ESB_DIMENSIONINFO_T (EsbGetDimensionInfo)
Global Const ESB_HMEMBER_TYPE = 27     ' ESB_HMEMBER_T (EsbOtlQueryMembers)
Global Const ESB_GENLEVELNAME_TYPE = 28 ' ESB_GENLEVELNAME_T (EsbOtlGetGenNames)
Global Const ESB_VARIABLE_TYPE = 29    ' ESB_VARIABLE_T (EsbListVariables)
Global Const ESB_LRO_TYPE = 30         ' ESB_LRODESC_T
Global Const ESB_PART_INFO_TYPE = 31   ' ESB_PART_INFO_T
Global Const ESB_DTS_TYPE = 32         ' ESB_DTSMBRINFO_T (EsbGetEnabledDTSMembers)
Global Const ESB_MBRNAME_TYPE = 33     ' ESB_MBRNAME_T (EsbOtlGetDimensionUserAttributes)
Global Const ESB_ATTRIBUTEINFO_TYPE = 34

'**************************************************************************
'  API GLOBAL CONSTANT DEFINITIONS
'**************************************************************************

'**************************************************************************
'  Global Constants for User/Group Type Flag -
'  Used for Type field in ESB_USERINFO_T and ESB_GROUPINFO_T structures.


Global Const ESB_TYPE_NONE = 0               ' Not a valid user/group
Global Const ESB_TYPE_USER = 1               ' User
Global Const ESB_TYPE_GROUP = 2              ' Group
Global Const ESB_TYPE_SYSUSER = 3            ' System user (non-editable)
Global Const ESB_TYPE_SYSGROUP = 4           ' System group (non-editable)

'**************************************************************************
'  Global Constants for Database Type -
'  Used by eSBCreateDatabase function.


Global Const ESB_DBTYPE_NORMAL = 0           ' normal database
Global Const ESB_DBTYPE_CURRENCY = 1         ' currency database


'**************************************************************************
'  Global Constants for Object Type -
'  Used by Object Functions.

Global Const ESB_OBJTYPE_NONE = &H0
Global Const ESB_OBJTYPE_OUTLINE = &H1       '1    Outline File Obj
Global Const ESB_OBJTYPE_CALCSCRIPT = &H2    '2    Calc Script File Obj
Global Const ESB_OBJTYPE_REPORT = &H4        '4    Report Specificatn File Obj
Global Const ESB_OBJTYPE_RULES = &H8         '8    Rules File Obj
Global Const ESB_OBJTYPE_ALIAS = &H10        '16   Alias File Obj
Global Const ESB_OBJTYPE_STRUCTURE = &H20    '32   Structure File Obj
Global Const ESB_OBJTYPE_ASCBACKUP = &H40    '64   ASCII Backup File Data Obj
Global Const ESB_OBJTYPE_BINBACKUP = &H80    '128  Binary Backup File Data Obj
Global Const ESB_OBJTYPE_EXCEL = &H100       '256  Excel Worksht File Data Obj
Global Const ESB_OBJTYPE_LOTUS2 = &H200      '512  1-2-3 2.x Worksheet File Obj
Global Const ESB_OBJTYPE_LOTUS3 = &H400      '1024 1-2-3 3.x Worksheet File Obj
Global Const ESB_OBJTYPE_TEXT = &H800        '2048 Text File Data Obj
Global Const ESB_OBJTYPE_LOTUS4 = &H8000     '32768 1-2-3 4.x Worksheet File Obj
Global Const ESB_OBJTYPE_WIZARD = &H10000    '1-2-3 4.x Worksheet File Obj
Global Const ESB_OBJTYPE_MAX = &H10000       'max single obj type value

'**************************************************************************
' combined object types

Global Const ESB_OBJTYPE_BACKUP = ESB_OBJTYPE_ASCBACKUP + ESB_OBJTYPE_BINBACKUP

Global Const ESB_OBJTYPE_WORKSHEET = ESB_OBJTYPE_EXCEL + ESB_OBJTYPE_LOTUS2 + ESB_OBJTYPE_LOTUS3 + ESB_OBJTYPE_LOTUS4

Global Const ESB_OBJTYPE_DATA = ESB_OBJTYPE_BACKUP + ESB_OBJTYPE_WORKSHEET + ESB_OBJTYPE_TEXT

Global Const ESB_OBJTYPE_ALL = &HFFFF        ' all objects

'**************************************************************************
'  Security Access Levels & Privileges (for variables of type ESB_ACCESS_T).

Global Const ESB_PRIV_NONE = &H0             '0    no privilege
Global Const ESB_PRIV_READ = &H1             '1    read data
Global Const ESB_PRIV_WRITE = &H2            '2    write data
Global Const ESB_PRIV_CALC = &H4             '4    calculate data
Global Const ESB_PRIV_DBLOAD = &H10          '16   load/unload databases
Global Const ESB_PRIV_DBDESIGN = &H20        '32   design databases
Global Const ESB_PRIV_DBCREATE = &H40        '64   create/delete/edit databases
Global Const ESB_PRIV_APPLOAD = &H100        '256  load/unload applications
Global Const ESB_PRIV_APPDESIGN = &H200      '512  design applications
Global Const ESB_PRIV_APPCREATE = &H400      '1024 create/delete/edit apps
Global Const ESB_PRIV_USERCREATE = &H1000    '4096 create/delete/edit users

'**************************************************************************
'  Predefines for Data Storage types

Global Const ESB_DEFAULT_DATA_STORAGE = &H0                      '0
Global Const ESB_MULTIDIM_DATA_STORAGE = &H1                     '1
Global Const ESB_DB2RELATIONAL_DATA_STORAGE = &H2                '2
Global Const ESB_ORACLERELATIONAL_DATA_STORAGE = &H3             '3
Global Const ESB_UNDEFINED_DATA_STORAGE = &H2710                 '10000


'**************************************************************************
'  Predefined Security Access Levels

Global Const ESB_ACCESS_NONE = ESB_PRIV_NONE
Global Const ESB_ACCESS_READ = ESB_PRIV_READ + ESB_PRIV_APPLOAD + ESB_PRIV_DBLOAD
Global Const ESB_ACCESS_WRITE = ESB_PRIV_WRITE + ESB_ACCESS_READ
Global Const ESB_ACCESS_CALC = ESB_PRIV_CALC + ESB_ACCESS_WRITE
Global Const ESB_ACCESS_DBDESIGN = ESB_PRIV_DBDESIGN + ESB_ACCESS_CALC
Global Const ESB_ACCESS_DBCREATE = ESB_PRIV_DBCREATE + ESB_ACCESS_DBDESIGN
Global Const ESB_ACCESS_APPDESIGN = ESB_PRIV_APPDESIGN + ESB_ACCESS_DBCREATE
Global Const ESB_ACCESS_APPCREATE = ESB_PRIV_APPCREATE + ESB_ACCESS_APPDESIGN
Global Const ESB_ACCESS_FILTER = ESB_PRIV_APPLOAD + ESB_PRIV_DBLOAD

Global Const ESB_ACCESS_DBALL = &HFF         '255  full database access
Global Const ESB_ACCESS_APPALL = &HFFF       '4095 full app/database access
Global Const ESB_ACCESS_SUPER = &HFFFF       '-1   supervisor (unrestr. access)

'**************************************************************************
'  Global Const for State Field of Process State Structure (ESB_PROCSTATE_T).

Global Const ESB_STATE_DONE = 0               ' No process, or process complete
Global Const ESB_STATE_INPROGRESS = 1         ' Async process is in progress

'**************************************************************************
'   Constants for DbReqType field of Database Request Info Structure
'   (ESB_DBREQINFO_T).

Global Const ESB_DBREQTYPE_DATLOAD = 0      ' Data load
Global Const ESB_DBREQTYPE_CALC = 1         ' Calculation
Global Const ESB_DBREQTYPE_OTLUPD = 2       ' Outline update

'**************************************************************************
'   Constants for DbReqFlags bit field of Database Request Info Structure
'   (ESS_DBREQINFO_T).

Global Const ESB_DBREQFLAG_CALCDEF = 1      ' Used default calc script
Global Const ESB_DBREQFLAG_CALCSCR = 2      ' Used custom calc script

'**************************************************************************
'  Global Constants for Application/Database Info Structures
'  (ESB_APPINFO_T & ESB_DBINFO_T).

Global Const ESB_STATUS_NOTLOADED = 0         ' Application not loaded
Global Const ESB_STATUS_LOADING = 1           ' Application is loading
Global Const ESB_STATUS_LOADED = 2            ' Application loaded
Global Const ESB_STATUS_UNLOADING = 3         ' Application is unloading

'**************************************************************************
'  Global Constants for Data field of Database Info Structure
'  (ESB_DBINFO_T).

Global Const ESB_DBDATA_NONE = 0              ' database has no data
Global Const ESB_DBDATA_LOADNOCALC = 1        ' data loaded but not calced
Global Const ESB_DBDATA_CLEAN = 2             ' data is calculated

'**************************************************************************
'  Global Constants for Member Structure (ESB_MBR_T).

'  Global Constants for the UCalc field

Global Const ESB_UCALC_ADD = 0
Global Const ESB_UCALC_SUB = 1
Global Const ESB_UCALC_MULT = 2
Global Const ESB_UCALC_DIV = 3
Global Const ESB_UCALC_PERCENT = 4
Global Const ESB_UCALC_NOOP = 5


'  TagType - No tag value

Global Const ESB_TTYPE_NONE = 0             ' Dimension not tagged
Global Const ESB_TTYPE_CCATEGORY = 1        ' Accounts -- currency category tag
Global Const ESB_TTYPE_CNAME = 2            ' Country  -- currency name tag
Global Const ESB_TTYPE_CTIME = 3            ' Time - currency TIME tag
Global Const ESB_TTYPE_CTYPE = 4            ' Type - currency TYPE tag
Global Const ESB_TTYPE_CPARTITION = 5       ' Currency Partition tag
Global Const ESB_TTYPE_ATTRIBUTE = 6        ' Attribute tag
Global Const ESB_TTYPE_ATTRCALC = 7        ' Aggregate tag




'  Notes:
'       The high 2 bits are used for masking data value for operations:
'       BALFIRST, BALLAST, PERCENT, AVERAGE (these operations are mutually
'       exclusive)

Global Const ESB_ATYPE_MASKNON = &H0         ' do not mask on zero and missing
                                             ' value
Global Const ESB_ATYPE_MASKMISSG = &H4000    ' mask on missing value
Global Const ESB_ATYPE_MASKZERO = &H8000     ' mask on zero value
Global Const ESB_ATYPE_MASKBOTH = &HC000     ' mask on both zero and missing
                                             ' value
Global Const ESB_ATYPE_BALFIRST = &H1        ' Account type - Balance FIRST
Global Const ESB_ATYPE_BALLAST = &H2         ' Balance LAST
Global Const ESB_ATYPE_PERCENT = &H4         ' Percent
Global Const ESB_ATYPE_AVERAGE = &H8         ' Average
Global Const ESB_ATYPE_UNITS = &H10          ' Unit
Global Const ESB_ATYPE_DETAILONLY = &H20     ' DetailsOnly
Global Const ESB_ATYPE_EXPENSE = &H40        ' EXPENSE

'************************************************************
' Member Status Types


Global Const ESB_MBRSTS_NOTSET = &H0        '   Normal case
Global Const ESB_MBRSTS_NEVER = &H1         '   Never set SHARE
Global Const ESB_MBRSTS_LABEL = &H2         '   Never carries any data
Global Const ESB_MBRSTS_REFER = &H4         '   Member refers to the prototype
                                            '    member with the same name
Global Const ESB_MBRSTS_REFNME = &H8        '   Member refers to the prototype
                                            '    member with a different name
Global Const ESB_MBRSTS_SHARE = &H10        '   Single parent-child case,
                                            '    This is an implicit share
Global Const ESB_MBRSTS_VIRTSTORE = &H20    '    Virtual Mbr. (Stored)
Global Const ESB_MBRSTS_VIRTNOSTORE = &H40  '    Virtual Mbr. (Not stored)
Global Const ESB_MBRSTS_ATTRIBUTE = &H800   '   Attribute Mbr.
Global Const ESB_MBRSTS_REFERRED = &H8000   '   Shared member.  More than
                                            '   one member with the same name


'**************************************************************************
'  Global Constants for Database State Structure (ESB_DBSTATE_T).

Global Const ESB_DIMTYPE_DENSE = 0           ' Dense dimension type
Global Const ESB_DIMTYPE_SPARSE = 1          ' Sparse dimension type

Global Const ESB_STRUCT_ARRAY = 0            ' Array structure
Global Const ESB_STRUCT_TREE = 1             ' Tree structure

Global Const ESB_CRCTYPE_DIV = 0             ' cval=(val*ForeignRate)/LocalRate
Global Const ESB_CRCTYPE_MULT = 1            ' cval=(val*LocalRate)/ForeignRate

'**************************************************************************
'  Global Constants for Database State Structure (ESB_DBSTATS_T).

Global Const ESB_INDEXTYPE_ARRAY = 0
Global Const ESB_INDEXTYPE_AVL = 1

'**************************************************************************
'  Global Constants for Outline Restructuring

Global Const ESB_DOR_ALLDATA = 1
Global Const ESB_DOR_NODATA = 2
Global Const ESB_DOR_LOWDATA = 3
Global Const ESB_DOR_INDATA = 4

'**************************************************************************
'  Global Constants for Currency conversion types

Global Const ESB_CONV_NONE = 0
Global Const ESB_CONV_CATEGORY = 1
Global Const ESB_CONV_NOCONV = 2

'**************************************************************************
'  Global Constants for time balance

Global Const ESB_TIMEBAL_NONE = 0
Global Const ESB_TIMEBAL_FIRST = 1
Global Const ESB_TIMEBAL_LAST = 2
Global Const ESB_TIMEBAL_AVG = 3

'**************************************************************************
'  Global Constants for time balance skip options

Global Const ESB_SKIP_NONE = 0
Global Const ESB_SKIP_MISSING = 1
Global Const ESB_SKIP_ZEROS = 2
Global Const ESB_SKIP_BOTH = 3

'**************************************************************************
'  Global Constants for share options

Global Const ESB_SHARE_DATA = 0
Global Const ESB_SHARE_NEVER = 1
Global Const ESB_SHARE_LABEL = 2
Global Const ESB_SHARE_SHARE = 3
Global Const ESB_SHARE_DYNCALCSTORE = 4
Global Const ESB_SHARE_DYNCALCNOSTORE = 5

'**************************************************************************
'  Global Constants for dimension categories

Global Const ESB_CAT_NONE = 0
Global Const ESB_CAT_ACCOUNTS = 1
Global Const ESB_CAT_TIME = 2
Global Const ESB_CAT_COUNTRY = 3
Global Const ESB_CAT_TYPE = 4
Global Const ESB_CAT_CURPARTITION = 5
Global Const ESB_CAT_ATTRIBUTE = 6

'**************************************************************************
'  Global Constants for dimension storage categories

Global Const ESB_STORECAT_OTHER = 0
Global Const ESB_STORECAT_TIME = 1
Global Const ESB_STORECAT_UNITS = 2
Global Const ESB_STORECAT_SCENARIO = 3
Global Const ESB_STORECAT_ACCOUNTS = 4
Global Const ESB_STORECAT_PRODUCT = 5
Global Const ESB_STORECAT_ORGAN = 6
Global Const ESB_STORECAT_MARKET = 7
Global Const ESB_STORECAT_CUSTOMER = 8
Global Const ESB_STORECAT_DIST = 9
Global Const ESB_STORECAT_BUSUNIT = 10
Global Const ESB_STORECAT_GEOG = 11
Global Const ESB_STORECAT_ATTRIBUTE = 12



'**************************************************************************
'  Global Constants for outline sorting options

Global Const ESB_SORT_ASCENDING = 0
Global Const ESB_SORT_DESCENDING = 1
Global Const ESB_SORT_USERDEFINED = 2

'**************************************************************************
'  Global Constants for outline query types

Global Const ESB_CHILDREN = 1
Global Const ESB_DESCENDANTS = 2
Global Const ESB_BOTTOMLEVEL = 3
Global Const ESB_SIBLINGS = 4
Global Const ESB_SAMELEVEL = 5
Global Const ESB_SAMEGENERATION = 6
Global Const ESB_PARENT = 7
Global Const ESB_DIMENSION = 8
Global Const ESB_NAMEDGENERATION = 9
Global Const ESB_NAMEDLEVEL = 10
Global Const ESB_SEARCH = 11
Global Const ESB_WILDSEARCH = 12
Global Const ESB_USERATTRIBUTE = 13
Global Const ESB_ANCESTORS = 14
Global Const ESB_DTSMEMBER = 15
Global Const ESB_DIMUSERATTRIBUTES = 16

'**************************************************************************
'  Global Constants for outline query options

Global Const ESB_MEMBERSONLY = &H1
Global Const ESB_ALIASESONLY = &H2
Global Const ESB_MEMBERSANDALIASES = &H4
Global Const ESB_COUNTONLY = &H8

'**************************************************************************
'  Global Constants for generation and levels

Global Const ESB_GENLEV_ALL = 0
Global Const ESB_GENLEV_ACTUAL = 1
Global Const ESB_GENLEV_DEFAULT = 2
Global Const ESB_GENLEV_NOACTUAL = 3

'**************************************************************************
'  Global Constants for attribute Query

Global Const ESB_INVALID_MEMBER = 0
Global Const ESB_STANDARD_MEMBER = 1
Global Const ESB_STANDARD_DIMENSION = 3
Global Const ESB_BASE_MEMBER = 5
Global Const ESB_BASE_DIMENSION = 7
Global Const ESB_ATTRIBUTE_MEMBER = &H9
Global Const ESB_ATTRIBUTE_DIMENSION = &HB
Global Const ESB_ATTRIBUTED_MEMBER = &H15

'***********************************************************************
'  Attribute Query Related operators

Global Const ESB_EQ = 0
Global Const ESB_NEQ = 1
Global Const ESB_GT = 2
Global Const ESB_LT = 3
Global Const ESB_GTE = 4
Global Const ESB_LTE = 5
Global Const ESB_TYPEOF = 6
Global Const ESB_ALL = 7

'***********************************************************************
'  Attribute Specifications related constants

'***** Number of children in Attribute calculations and boolean dimensions ****
Global Const ESB_ATTRCALC_CHILDCOUNT = 5
Global Const ESB_BOOLEAN_CHILDCOUNT = 2

'*****  Numeric Attribute Generate long name by ***********
Global Const ESB_GENNAMEBY_PREFIX = 0   'default
Global Const ESB_GENNAMEBY_SUFFIX = 1

'****** Numeric Attribute Use Name of *********************
Global Const ESB_USENAMEOF_NONE = 0 'default
Global Const ESB_USENAMEOF_PARENT = 1
Global Const ESB_USENAMEOF_GRANDPARENTANDPARENT = 2
Global Const ESB_USENAMEOF_ALLANCESTORS = 3
Global Const ESB_USENAMEOF_DIMENSION = 4

'************ Delimiter *****************
Global Const ESB_DELIMITER_UNDERSCORE = 0  ' default
Global Const ESB_DELIMITER_PIPE = 1
Global Const ESB_DELIMITER_CARET = 2

'************* Date Format ****************
Global Const ESB_DATEFORMAT_MMDDYYYY = 0 'default
Global Const ESB_DATEFORMAT_DDMMYYYY = 1

'************* Bucketing Type Constants *********
Global Const ESB_UPPERBOUNDINCLUSIVE = 0 'default
Global Const ESB_LOWERBOUNDINCLUSIVE = 1

'************** Defaults for the Attribute Calculations (aggregate) dimension ****
Global Const ESB_DEFAULT_TRUESTRING = "True"
Global Const ESB_DEFAULT_FALSESTRING = "False"
Global Const ESB_DEFAULT_ATTRIBUTECALULATIONS = "Attribute Calculations"
Global Const ESB_DEFAULT_SUM = "Sum"
Global Const ESB_DEFAULT_COUNT = "Count"
Global Const ESB_DEFAULT_AVERAGE = "Average"
Global Const ESB_DEFAULT_MIN = "Min"
Global Const ESB_DEFAULT_MAX = "Max"

'**************************************************************************
'  Global Constants for member error bitmask

Global Const ESB_OUTERROR_ILLEGALNAME As Long = &H1
Global Const ESB_OUTERROR_DUPLICATENAME As Long = &H2
Global Const ESB_OUTERROR_ILLEGALCURRENCY As Long = &H4
Global Const ESB_OUTERROR_ILLEGALDEFALIAS As Long = &H8
Global Const ESB_OUTERROR_ILLEGALCOMBOALIAS As Long = &H10
Global Const ESB_OUTERROR_ILLEGALALIASSTRING As Long = &H20
Global Const ESB_OUTERROR_ILLEGALTAG As Long = &H40
Global Const ESB_OUTERROR_NOTIMEDIM As Long = &H80
Global Const ESB_OUTERROR_DUPLICATEALIAS As Long = &H100
Global Const ESB_OUTERROR_MEMBERCALC As Long = &H200
Global Const ESB_OUTERROR_SHARENOTLEVEL0 As Long = &H400
Global Const ESB_OUTERROR_NOSHAREPROTO As Long = &H800
Global Const ESB_OUTERROR_TIMESPARSE As Long = &H1000
Global Const ESB_OUTERROR_LEAFLABEL As Long = &H2000
Global Const ESB_OUTERROR_ALIASSHARED As Long = &H4000
Global Const ESB_OUTERROR_BADTIMEBAL As Long = &H8000
Global Const ESB_OUTERROR_BADSKIP As Long = &H10000
Global Const ESB_OUTERROR_BADSHARE As Long = &H20000
Global Const ESB_OUTERROR_BADSTORAGE As Long = &H40000
Global Const ESB_OUTERROR_BADCATEGORY As Long = &H80000
Global Const ESB_OUTERROR_BADSTORAGECATEGORY As Long = &H100000
Global Const ESB_OUTERROR_CURTOOMANYDIMS As Long = &H200000
Global Const ESB_OUTERROR_SHAREDMEMBERFORMULA As Long = &H400000
Global Const ESB_OUTERROR_SHAREUDA As Long = &H800000
Global Const ESB_OUTERROR_DUPGENLEVNAME As Long = &H1000000

Global Const ESB_OUTERROR_ERRORMASK As Long = &H80FFFFFF
Global Const ESB_OUTERROR_SHAREPRECEEDSPROTO As Long = &H1000000
Global Const ESB_OUTERROR_VIRTTOOMANYCHILDREN As Long = &H2000000

' This is added for backward compatability. The users with old version
' of client will not see ulErrors2. Hence this value will indicate that
' there is an error, but you need to refer to ulErrors to get it.

Global Const ESB_OUTERROR_REFERERROR2 As Long = &H80000000
 
'Member Error2 Bitmask Constants (ESB_OUTERROR_T.ulErrors2)
Global Const ESB_OUTERROR2_NOTLEVEL0 As Long = &H1
Global Const ESB_OUTERROR2_LEVELMISMATCH As Long = &H2
Global Const ESB_OUTERROR2_ILLEGALORDER As Long = &H4
Global Const ESB_OUTERROR2_ILLEGALDATATYPE As Long = &H8
Global Const ESB_OUTERROR2_DATATYPEMISMATCH As Long = &H10
Global Const ESB_OUTERROR2_ILLEGALATTRIBUTEPARENT As Long = &H20
Global Const ESB_OUTERROR2_ATTRDIMNOTASSOCIATED As Long = &H40
Global Const ESB_OUTERROR2_ILLEGALUDA As Long = &H80
Global Const ESB_OUTERROR2_CHILDCOUNT As Long = &H100
Global Const ESB_OUTERROR2_ILLEGALATTRCALC As Long = &H200
Global Const ESB_OUTERROR2_DUPLICATEATTRCALC As Long = &H400
Global Const ESB_OUTERROR2_ILLEGALATTRSET As Long = &H800
Global Const ESB_OUTERROR2_ILLEGALATTRCALCSET As Long = &H1000
Global Const ESB_OUTERROR2_NOTATTRIBUTE As Long = &H2000
Global Const ESB_OUTERROR2_ATTRCALCABSENT As Long = &H4000
Global Const ESB_OUTERROR2_ILLEGALATTRVALUE As Long = &H8000
        
Global Const ESB_OUTERROR_BADATTRIBUTECODE As Long = &H200000

'*****************************************************************
'  Constants for the Message function arguments

Global Const ESB_DT_CHAR = 1
Global Const ESB_DT_UCHAR = 2
Global Const ESB_DT_SHORT = 3
Global Const ESB_DT_USHORT = 4
Global Const ESB_DT_LONG = 5
Global Const ESB_DT_ULONG = 6
Global Const ESB_DT_FLOAT = 7
Global Const ESB_DT_DOUBLE = 8
Global Const ESB_DT_BOOL = 9
Global Const ESB_DT_STRING = 10
Global Const ESB_DT_DATETIME = 11


'***********************************************************************
' Attribute Member or Dimension  Data Type

Global Const ESB_ATTRMBRDT_DOUBLE = ESB_DT_DOUBLE
Global Const ESB_ATTRMBRDT_BOOL = ESB_DT_BOOL
Global Const ESB_ATTRMBRDT_DATETIME = ESB_DT_DATETIME
Global Const ESB_ATTRMBRDT_STRING = ESB_DT_STRING
Global Const ESB_ATTRMBRDT_NONE = 0


'**************************************************************************
'  ESB STRUCTURE DEFINITIONS
'**************************************************************************

'**************************************************************************
'  Essbase ESB Initialization Structure

Type ESB_INIT_T

   Version     As Long                  ' Version of API
   MaxHandles  As Integer               ' max number of context handles req
   LocalPath   As String * ESB_PATHLEN  ' local path to use for file oper
   MessageFile As String * ESB_PATHLEN  ' full path name of message db file
   HelpFile    As String * ESB_PATHLEN  ' full path name of help file
   ClientError As Integer            ' allow use of a pseudo client error
                                        ' handler
   ErrorStack  As Integer               ' size of the error message stack
 
End Type

'**************************************************************************
'  Application Database List Structure

Type ESB_APPDB_T

   AppName      As String * ESB_APPNAMELEN ' application name
   DbName       As String * ESB_DBNAMELEN  ' database name

End Type

'**************************************************************************
'  Application State Structure

Type ESB_APPSTATE_T

   Description  As String * ESB_DESCLEN ' application description field
   Loadable     As Integer              ' allow application to be loaded
   Autoload     As Integer              ' automatically load on startup
   Access       As Integer              ' default access level
   Connects     As Integer              ' allow connects flag
   Commands     As Integer              ' allow commands flag
   Updates      As Integer              ' allow updates flag
   LockTimeout  As Long                 ' lock timeout period
   lroSizeLimit As Long                 ' LRO file size limit
   Security     As Integer              ' enable application security

End Type

'**************************************************************************
'  Application Info Structure (GET only)

Type ESB_APPINFO_T

   Name          As String * ESB_APPNAMELEN     ' application name
   Server        As String * ESB_SVRNAMELEN     ' network server name
   storageType   As Integer                     ' data storage type
   status        As Integer                     ' application load status
   nConnects     As Integer                     ' number of users connected
   nDbs          As Integer                     ' number of databases in this app
   ElapsedAppTime As Long                       ' elapsed application time

End Type

'**************************************************************************
'  Application InfoEx Structure (GET only)
' Moved ElapsedAppTime to the end of the structure for the bug 23911
Type ESB_APPINFOEX_T

   Name          As String * ESB_APPNAMELEN     ' application name
   Server        As String * ESB_SVRNAMELEN     ' network server name
   storageType   As Integer                     ' data storage type
   status        As Integer                     ' application load status
   nConnects     As Integer                     ' number of users connected
   ElapsedAppTime As Long                       ' elapsed application time

End Type

'**************************************************************************
'  Allowed Storage Types Structure

Type ESB_ALLOWEDSTORAGETYPES_T

 defaultType      As Integer
 multidimstorage  As Integer
 db2storage       As Integer
 oraclestorage    As Integer

End Type


'**************************************************************************
'  Database State Structure

Type ESB_DBSTATE_T

   Description  As String * ESB_DESCLEN    ' db description field
   Loadable         As Integer          ' allow database to be loaded
   Autoload         As Integer          ' automatically load on startup
   Access           As Integer             ' default access level
   IndexType        As Integer             ' db index type - array or tree
   MaxMem           As Long                ' max memory for non compressed
                                           ' blocks
   MaxCompMem       As Long                ' max memory for compressed blocks
   MaxMemIndex      As Long                ' maximum index memory
   IndexPageSize    As Long                ' index page size

   CalcNoAggMissing As Integer          ' Don't aggregate if all children
                                           ' are missing
   CalcNoAvgMissing As Integer          ' Don't average on missing value
   CalcTwoPass      As Integer          ' Perform two pass calc when
                                           ' running "calc all;"
   CalcCreateBlock  As Integer          ' Create a sparse data block on
                                           ' Global Constant assignment calc
                                           ' equation
   CrDbName         As String * ESB_DBNAMELEN  'Currency database name
   CrTypeMember     As String * ESB_MBRNAMELEN 'currency conversion type member

   CrConvType       As Integer             ' currency conversion type
   DataCompress     As Integer             ' Optional compression flag
   RetrievalBuffer  As Long                ' Retrieval buffer size allocated
                                           ' per retrieval requests
                                           ' (2048 bytes default)
   RetrievalSortBuffer As Long             ' Retrieval sort buffer size
                                           ' allocated per retrieval requests
                                           ' (10240 bytes default)
   TimeOut          As Long                ' Time out set in seconds for
                                           ' COMMITTED access only
                                           ' IMMEDIATE default)
   CommitBlocks     As Long                ' The number of data blocks updated
                                           ' before the explicit commit is
                                           ' performed (used during calculation
                                           ' and spredsheet updates)
   CommitRows       As Long                ' The number of rows of the input
                                           ' file processed before the explicit
                                           ' commit is performed during the
                                           ' dataload
   nVolumes         As Long                ' number of disk volume setting for
                                           ' this database

   DataCompressType As Integer             ' Data Compression Type
   IsolationLevel   As Integer             ' Isolation level
                                           ' UNCOMMITTED default
   PreImage         As Integer             ' Flag to read previously commited
                                           ' data during the read-only requests
                                           ' Can only be set for COMMITTED
                                           ' access (YES default)
End Type

'**************************************************************************
'  Database Info Structure (GET only)

Type ESB_DBINFO_T

   ElapsedDbTime        As Long                    ' Elapsed database time
   DataFileCacheSetting As Long                    ' Data File Cache Size database
   DataFileCacheSize    As Long
   DataCacheSetting     As Long
   DataCacheSize        As Long                    ' run-time size of the Data cache
   IndexCacheSetting    As Long
   IndexCacheSize       As Long                   ' run-time size of the Index cache
   IndexPageSetting     As Long
   IndexPageSize        As Long                    ' run-time size of an Index Page
   nDims                As Long                    ' number of dimensions
   DbType               As Integer                 ' Database Type
   status               As Integer                 ' database load status
   nConnects            As Integer                 ' number of users connected
   nLocks               As Integer                 ' number of blocks locked
   Data                 As Integer                 ' data loaded flag
   AppName              As String * ESB_APPNAMELEN ' application name
   Name                 As String * ESB_DBNAMELEN  ' database name
   Country              As String * ESB_MBRNAMELEN ' curr country dimensionmember
   Time                 As String * ESB_MBRNAMELEN ' curr time dimension member
   Category             As String * ESB_MBRNAMELEN ' curr category dimension member
   Type                 As String * ESB_MBRNAMELEN ' curr type dimension member
   CrPartition          As String * ESB_MBRNAMELEN ' curr partition member

End Type

'**************************************************************************
' Dimension information structure
Type ESB_DIMENSIONINFO_T
   DimName       As String * ESB_MBRNAMELEN ' The dimension name
   DimNumber     As Long                    ' Dimension number of the member
   DimType       As Integer                 ' dimension type
                                            ' either ESB_DIMTYPE_DENSE or
                                            ' ESB_DIMTYPE_SPARSE
   DimTag        As Integer                 ' dimension tag type
                                            ' one of ESS_TTYPE_xxx
   DeclaredDimSize As Long                  ' declared dimension size
   ActualDimSize As Long                    ' actual dimension size

   Description   As String * ESB_DESCLEN    ' Reserved -- not supported now
   DimDataType   As Integer                 ' Dimension(attribute) Data Type

End Type

'**************************************************************************
' Member information structure

Type ESB_MEMBERINFO_T
   CrMbrName     As String * ESB_MBRNAMELEN ' The name of the tagged currency
   MbrName       As String * ESB_MBRNAMELEN ' The member name
   DimName       As String * ESB_MBRNAMELEN ' The member's dimension name
   ParentMbrName As String * ESB_MBRNAMELEN ' Parent member name
   ChildMbrName  As String * ESB_MBRNAMELEN ' First child member name
   PrevMbrName   As String * ESB_MBRNAMELEN ' Previous sibling member name
   NextMbrName   As String * ESB_MBRNAMELEN ' Next sibling member name
   Description   As String * ESB_DESCLEN    ' Member description
   MbrNumber     As Long                    ' Member number, starting from bottom left
   DimNumber     As Long                    ' Dimension number of the member
   status        As Integer                 ' Member status: defined in
                                            ' ESB_MBRSTS_xxx
   Level         As Integer                 ' Member level number
   Generation    As Integer                 ' Member generation number
   UnaryCalc     As Integer                 ' Default rollup for this member
                                            ' Defined in eSS_UCAL_xxx
   MbrTagType    As Integer                 ' A 16 bit mask for mbr's tagged
                                            ' types masks are defined in
                                            ' ESB_ATYPE_xxx
   CurrConvert   As Integer                 ' Currency conversion
                                            ' (ESB_TRUE/ESB_FALSE)
   Attribute     As Variant                   ' Attribute Value
   IsAttributed  As Integer                 ' Indicates whether attribute associated
End Type

'**************************************************************************
' Attribute information structure

Type ESB_ATTRIBUTEINFO_T

   MbrName       As String * ESB_MBRNAMELEN ' The member name
   DimName       As String * ESB_MBRNAMELEN ' The member's dimension name
   Attribute     As Variant                 ' Attribute Value

End Type

'**************************************************************************
'  Currency Partition Rate Info Structure

Type ESB_RATEINFO_T

   MbrName       As String * ESB_MBRNAMELEN  ' Currency partition member name
   RateMbr       As String * ESB_RATEINFOLEN ' Array of rate member names

End Type

'**************************************************************************
'  Alias List Structure

Type ESB_MBRALT_T

   MbrName           As String * ESB_MBRNAMELEN   ' member name
   AltName           As String * ESB_MBRNAMELEN   ' alias name
  
End Type

'**************************************************************************
'  Dimension statistics Info Structure

Type ESB_DIMSTATS_T

   DeclaredDimSize   As Long                      ' delclared dimension size
   ActualDimSize     As Long                      ' actual dimension size
   DimType           As Integer                   ' dimension type
   DimName           As String * ESB_MBRNAMELEN   ' dimension name

End Type

'**************************************************************************
'  Db statistics Info Structure

Type ESB_DBSTATS_T

   nDims                   As Long                ' number of dimensions
   DeclaredBlockSize       As Long                ' declared block size
   ActualBlockSize         As Long                ' actual block size
   DeclaredMaxBlocks       As Double              ' declared max # of blocks
   ActualMaxBlocks         As Double              ' actual max # of blocks
   NonMissingLeafBlocks    As Double              ' # of non-missing leaf blks
   NonMissingNonLeafBlocks As Double              ' # of non-missing non-leaf
                                                  ' blocks
   NonMissingBlocks        As Double              ' # of non-missing blocks
   PagedOutBlocks          As Double              ' # of paged out blocks
   PagedInBlocks           As Double              ' # of paged in blocks
   InMemCompBlocks         As Double              ' # of in memory compressed
                                                  ' blocks, obsolete, zero'd
   TotalBlocks             As Double              ' # of blocks
   NonExclusiveLockCount   As Double              ' # of non exclusive locks
   ExclusiveLockCount      As Double              ' # of exclusive locks
   TotMemPagedInBlocks     As Double              ' total memory used for
                                                  ' paged in blocks
   TotMemBlocks            As Double              ' total memory for all
                                                  ' blocks
   TotMemIndex             As Double              ' total memory used for
                                                  ' indexing
   TotMemInMemCompBlocks   As Double              ' total memory used for in
                                                  ' memory compressing
                                                  ' obsolete, zero'd
   BlockDensity            As Double              ' average block density
   SparseDensity           As Double              ' average sparse density
   CompressionRatio        As Double              ' block compression ratio
   IndexType               As Integer             ' Current Indexing Type

End Type

'**************************************************************************
'  Server Process State Structure

Type ESB_PROCSTATE_T

   Action       As Integer        ' current process action
   State        As Integer        ' current process state
   Reserved1    As Integer        ' reserved for future use
   Reserved2    As Long           ' reserved for future use
   Reserved3    As Long           ' reserved for future use

End Type

'**************************************************************************
'  Structure for User/Group Information

Type ESB_USERINFO_T

   LastLogin     As Long            ' date and time of last successful login
   DbConnectTime As Long            ' date and time of db connection
   LoginId       As Long            ' user identification tag Id Type
   Login         As Integer         ' login flag (user only)
   Type          As Integer         ' user/group flag
   Access        As Integer         ' user default access level
   MaxAccess     As Integer         ' user default max access level
   Expiration    As Integer         ' password expiration date (user only)
   FailCount     As Integer         ' failed login attempts since last login
   Name          As String * ESB_USERNAMELEN  ' user/group name
   AppName       As String * ESB_APPNAMELEN   ' connected application
   DbName        As String * ESB_DBNAMELEN    ' connected database
   Description   As String * ESB_DESCLEN      ' user/group description
   EMailID       As String * ESB_DESCLEN      ' user/group email address
   LockedOut     As Integer         ' User Locked Out Flag
   PwdChgNow     As Integer         ' Force User to change password while login

End Type

'**************************************************************************
'  Structure for User/Group Application Information

Type ESB_USERAPP_T

   Access       As Integer         ' user's own application access level
   MaxAccess    As Integer         ' user's highest application access level
   userName     As String * ESB_USERNAMELEN  ' user/group name
   AppName      As String * ESB_APPNAMELEN   ' connected application

End Type

'**************************************************************************
'  Structure for User/Group Database Information

Type ESB_USERDB_T

   Access       As Integer                   ' user's own db access level
   MaxAccess    As Integer                   'user's highest database access level
   AppName      As String * ESB_APPNAMELEN   ' application name
   DbName       As String * ESB_DBNAMELEN    ' database name
   userName     As String * ESB_USERNAMELEN  ' user/group name
   FilterName   As String * ESB_FTRNAMELEN   ' database filter set (if any)

End Type

'**************************************************************************
'  Structure for User Lock Information

Type ESB_LOCKINFO_T

   LoginId      As Long                      ' user identification tag
   Time         As Long                      ' maximum time held (in seconds)
   nLocks       As Integer                   ' number of block locks held
   userName     As String * ESB_USERNAMELEN  ' user/group name

End Type

'**************************************************************************
'  Structure for Global Security Parameters.

Type ESB_GLOBAL_T

   Security     As Integer     ' global security status (enabled/disabled)
   Logins       As Integer     ' global login status (enabled/disabled)
   Access       As Integer        ' global default access level
   Validity     As Integer        ' global password validity (in days)
   Currency     As Integer     ' global currency enabled flag
   PwMin        As Integer        ' global minimum password length
   InactivityTime As Long         ' global auto-logout time (in seconds)
   InactivityCheck As Long        ' global auto-logout checking interval (sec)

End Type

'**************************************************************************
'  Structure for Time Record

Type ESB_TIMERECORD_T

   TimeValue   As Long           ' Time value in seconds after 1/1/70
   Seconds     As Integer        ' Seconds after the minute [0-59]
   Minutes     As Integer        ' Minutes after the hour [0-59]
   Hours       As Integer        ' Hours since midnight [0-23]
   Day         As Integer        ' Date of the month [1-31]
   Month       As Integer        ' Months since January [0-11], January = 0
   Year        As Integer        ' Years since 1900
   Weekday     As Integer        ' Days since Sunday [0-6], Sunday = 0
   Reserved    As Integer        ' (reserved field -- always 0)

End Type

'**************************************************************************
'  Structure for Database Request Information

Type ESB_DBREQINFO_T

   DbReqType   As Long                          ' Type of database request
   DbReqFlags  As Long                          ' Bit map of informational flags
   StartTimeRec As ESB_TIMERECORD_T             ' Request start time record
   EndTimeRec  As ESB_TIMERECORD_T              ' Request end time record
   User        As String * ESB_USERNAMELEN      ' User name

End Type


'**************************************************************************
'  Structure for File Object Information.

Type ESB_OBJINFO_T

   AppName        As String * ESB_APPNAMELEN  ' Application name
   DbName         As String * ESB_DBNAMELEN   ' Database name
   Name           As String * ESB_OBJNAMELEN  ' Object name
   Type           As Long                     ' Object type
   FileSize       As Long                     ' Allocated file size (in bytes)
   TimeStamp      As Long                     ' Date & time of last checkout
   TimeModified   As ESB_TIMERECORD_T         ' Date & time of last modification
   User           As String * ESB_USERNAMELEN ' Name of last user to check out
   Locked         As Integer                  ' Flag to indicate locked out

End Type

'**************************************************************************
'  Structure for Object Definition.

Type ESB_OBJDEF_T

  hCtx          As Long                       ' Context handle
  Type          As Long                       ' Object type
  AppName       As String * ESB_APPNAMELEN    ' Application name
  DbName        As String * ESB_DBNAMELEN     ' Database name
  FileName      As String * ESB_PATHLEN       ' Object file name

End Type

'**************************************************************************
'  Structure for Mbr User.

Type ESB_MBRUSER_T

  User          As String * ESB_USERNAMELEN
  Password      As String * ESB_PASSWORDLEN

End Type


'**************************************************************************
'  Structure for Outline Mbr Info

Type ESB_MBRINFO_T

   szMember       As String * ESB_MBRNAMELEN ' member name array
   usLevel        As Integer                 ' level number of the member
   usGen          As Integer                 ' generation number of the member
   usConsolidation As Integer                ' unary consolidation type
   fTwoPass       As Integer                 ' two pass member flag
   fExpense       As Integer                 ' expense member flag
   usConversion   As Integer                 ' currency conversion type
   szCurMember    As String * ESB_MBRNAMELEN ' currency conversion member
   usTimeBalance  As Integer                 ' time balance option
   usSkip         As Integer                 ' skip option (for time balance)
   usShare        As Integer                 ' share option
   usStorage      As Integer                 ' storage type (dimension only)
   usCategory     As Integer                 ' dimension category
   usStorageCategory As Integer              ' dimension storage category
   ulChildCount   As Long                    ' number of children
   szComment As String * ESB_MBRCOMMENTLEN   ' member comment array
   szDimName As String * ESB_MBRNAMELEN       ' Dimension Name
   Attribute As Variant                      ' Attribute value
   IsAttributed As Integer
End Type

'**************************************************************************
'  Structure for Outline Info

Type ESB_OUTLINEINFO_T

   fCaseSensitive As Integer              ' case sensitive mbr names flag
   usOutlineType  As Integer                 ' outline type
   fAutoConfigure As Integer              ' auto config dense/sparse flag
         
End Type

'**************************************************************************
'  Structure for Outline errors

Type ESB_OUTERROR_T

   hMember        As Long                    ' member handle
   ulErrors       As Long                    ' bitmask of member errors
   ulErrors2      As Long                    ' bitmask of additional errors
         
End Type

'**************************************************************************
'  Structure for Outline generation and level names

Type ESB_GENLEVELNAME_T

   usNumber       As Integer                 ' gen or level number
   szName         As String * ESB_MBRNAMELEN ' gen or level name
         
End Type

'**************************************************************************
'  Structure for Outline query predicate

Type ESB_PREDICATE_T

   ulQuery        As Long                    ' query type
   ulOptions      As Long                    ' query options
   pszDimension   As String * ESB_MBRNAMELEN ' dimension name
   pszString1     As String * 256            ' string 1
   pszString2     As String * 256            ' string 2
         
End Type

'**************************************************************************
'  Structure for Outline query member counts

Type ESB_MBRCOUNTS_T

   ulStart        As Long                    ' starting member
   ulMaxCount     As Long                    ' maximum members to retrieve
   ulTotalCount   As Long                    ' return total count of mbrs in query
   ulReturnCount  As Long                    ' return total count of mbrs returned
         
End Type

'**************************************************************************
'  Structure for Substitution Variables

Type ESB_VARIABLE_T

    Server      As String * ESB_SVRNAMELEN
    AppName     As String * ESB_APPNAMELEN
    DbName      As String * ESB_DBNAMELEN
    VarName     As String * ESB_MBRNAMELEN
    VarValue    As String * ESB_VARVALUELEN
        
End Type

'**************************************************************************
'  Structure for Linked Reporting Objects (LRO)

Type ESB_CELLADDR_API_T                        ' defn of cell address

   cellOffset     As Long                      ' cell offset within datablock
   blkOffset      As Double                    ' block offset
   segment        As Double                    ' segment number

End Type

Type ESB_LROHANDLE_API_T

   cellKey        As ESB_CELLADDR_API_T         ' cell address
   hObject        As Long                       ' object or note handle

End Type

Type ESB_LROINFO_API_T

   ObjName        As String * ESB_ONAMELEN_API  ' source file name
   objDesc        As String * ESB_LRODESCLEN_API ' object description
   
End Type

Type ESB_LRODESC_API_T

   ObjType        As Integer                    ' object type
   status         As Integer                    ' catalog entry status
   memCount       As Long                       ' member count
   linkId         As ESB_LROHANDLE_API_T        ' Link Id of LRO
   updateDate     As Long                       ' timestamp of last update
   accessLevel    As Integer                    ' access level of mbr combo
   userName       As String * ESB_USERNAMELEN   ' user name
   memComb        As String                     ' member combination
   note           As String * ESB_LRONOTELEN_API ' cell annotation
   lroInfo        As ESB_LROINFO_API_T          ' object description
   
End Type

'**************************************************************************
'  Structures for Partitions
'**************************************************************************
Type ESB_PART_INFO_T
   
   ' type of region (e.g. replication data source)
   OperationType      As Integer    'op type supported by this region
   DataDirection      As Integer    'source/target for data
   MetaDirection      As Integer    'source/target for metadata
   usReserved         As Integer    'reserved field -- always 0

   ' metadata change information
   LastMetaUpdateTime As Long       'last time metadata updated

   ' the following fields only apply to replication data targets
   LastRefreshTime    As Long       'last time data target refreshed
   AreaUpdatable    As Integer    'are chgs allowed to replicated data?

   ' the following fields only apply to replication data sources
   IncrRefreshAllowed As Integer    'can only changed data be refreshed?
   LastUpdateTime     As Long       'time of last change to data in region

   ' remote connection information
   SvrName       As String * ESB_SVRNAMELEN 'host for other side of region
   AppName       As String * ESB_APPNAMELEN 'app for other side of region
   DbName        As String * ESB_DBNAMELEN  'db for other side of region

End Type

Type ESB_PART_CONNECT_INFO_T
   
   HostName       As String * ESB_SVRNAMELEN    ' host (server) name
   AppName        As String * ESB_APPNAMELEN    ' application name
   DbName         As String * ESB_DBNAMELEN     ' database name

End Type

Type ESB_PART_REPL_T

   AreaCount    As Long        ' number of regions to refresh (-1 = ALL)
   UpdatedOnly    As Integer     ' refresh only cells modified at source
                                 ' since last update?
End Type

Type ESB_PARTSLCT_T

   OperationTypes      As Integer  ' type of region returned (any
                                   ' combination of ESB_PARTITION_OP_REPLICATED,
                                   ' ESB_PARTITION_OP_LINKED, and ..._TRANSPARENT)
   DirectionTypes      As Integer  ' type of region returned (any
                                   ' combination of ESB_PARTITION_DATA_SOURCE
                                   ' and ESB_PARTITION_DATA_TARGET)
   MetaDirectionTypes  As Integer  ' type of region returned (any
                                   ' combination of ESB_PARTITION_META_SOURCE
                                   ' and ESB_PARTITION_META_TARGET)
End Type

Type ESB_PARTOTL_QRY_FILTER_T    ' define meta data retrieval criteria

   TimeStamp           As Long  ' query meta change after this time
   DimFilter           As Long  ' bitfield to select dimension changes
   MbrFilter           As Long  ' bitfield to select member changes
   MbrAttrFilter       As Long  ' bitfield to select mbr attribute chg

End Type

Type ESB_PARTOTL_QUERY_T            ' query metadata changes

   OperationType     As Integer  ' ESB_PARTITION_OP_REPLICATED, _LINKED, _TRANSPARENT
   HostDatabase      As ESB_PART_CONNECT_INFO_T
   MetaFilter        As ESB_PARTOTL_QRY_FILTER_T

End Type

Type ESB_PART_DEFINED_T

   usType        As Integer  ' ESB_PARTITION_OP_REPLICATED, _LINKED, or _TRANSPARENT
   Direction     As Integer  ' ESB_PARTITION_DATA_SOURCE or _TARGET
   HostDatabase  As ESB_PART_CONNECT_INFO_T

End Type

'**************************************************************************
'  Structure for Dynamic Time Series Members
'**************************************************************************
Type ESB_DTSMBRINFO_T

   szDTSMember As String * ESB_MBRNAMELEN ' DTS Member name array
   usGen As Integer                       ' Generation at which DTS member
                                          ' is enabled
End Type

'**************************************************************************
'  Structure for Query Attribute
'**************************************************************************

Type ESB_ATTRIBUTEQUERY_T
   ' Member Type
   '      ESB_BASE_DIMENSION
   '      ESB_BASE_MEMBER
   '      ESB_ATTRIBUTE_DIMENSION
   '      ESB_ATTRIBUTE_MEMBER

   '   Operations
   '      ESB_EQ, ESB_NEQ, ESB_GT, ESB_LT,
   '      ESB_GTE, ESB_LTE, ESB_TYPEOF, ESB_ALL, etc.

   InputMember As Variant
   Attribute As Variant
   InputMemberType As Integer
   OutputMemberType As Integer
   Operation  As Integer
   

End Type

'**************************************************************************
'  Structure for Attribute Specification
'**************************************************************************

Type ESB_ATTRSPECS_T
    DefaultTrueString          As String * ESB_MBRNAMELEN
    DefaultFalseString         As String * ESB_MBRNAMELEN
    DefaultAttrCalcDimName     As String * ESB_MBRNAMELEN
    DefaultSumMbrName          As String * ESB_MBRNAMELEN
    DefaultCountMbrName        As String * ESB_MBRNAMELEN
    DefaultAverageMbrName      As String * ESB_MBRNAMELEN
    DefaultMinMbrName          As String * ESB_MBRNAMELEN
    DefaultMaxMbrName          As String * ESB_MBRNAMELEN
    GenNameBy                  As Integer
    UseNameOf                  As Integer
    Delimiter                  As Integer
    DateFormat                 As Integer
    BucketingType              As Integer
End Type


'*****************************************************************************
'  Function Declarations
'*****************************************************************************
Declare Function EsbOtlOpenOutline Lib "esbotln.DLL" (ByVal hCtx As Long, pObject As ESB_OBJDEF_T, ByVal fLock As Integer, ByVal fKeepTrans As Integer, phOutline As Long) As Long

Declare Function EsbInit Lib "esbapin.DLL" (pInit As ESB_INIT_T, phInst As Long) As Long

Declare Function EsbTerm Lib "esbapin" (ByVal hInst As Long) As Long

Declare Function EsbGetAPIVersion Lib "esbapin.DLL" (lVersion As Long) As Long

Declare Function EsbLogin Lib "esbapin" (ByVal hInst As Long, ByVal Server As String, ByVal User As String, ByVal Password As String, pItems As Integer, hCtx As Long) As Long

Declare Function EsbLoginSetPassword Lib "esbapin" (ByVal hInst As Long, ByVal Server As String, ByVal User As String, ByVal Password As String, ByVal NewPassword As String, pItems As Integer, hCtx As Long) As Long
 
Declare Function EsbLogout Lib "esbapin" (ByVal hCtx As Long) As Long

Declare Function EsbLogoutUser Lib "esbapin" (ByVal hCtx As Long, ByVal LoginId As Long) As Long

Declare Function EsbAutoLogin Lib "esbapin" (ByVal hInst As Long, ByVal Server As String, ByVal User As String, ByVal Password As String, ByVal AppName As String, ByVal DbName As String, ByVal opt As Integer, pAccess As Integer, phCtx As Long) As Long

Declare Function EsbCreateLocalContext Lib "esbapin" (ByVal hInst As Long, ByVal User As String, ByVal Password As String, phCtx As Long) As Long

Declare Function EsbDeleteLocalContext Lib "esbapin" (ByVal hCtx As Long) As Long
       
Declare Function EsbGetVersion Lib "esbapin" (ByVal hCtx As Long, Release As Integer, Version As Integer, Revision As Integer) As Long

Declare Function EsbShutdownServer Lib "esbapin" (ByVal hInst As Long, ByVal Server As String, ByVal User As String, ByVal Password As String) As Long

Declare Function EsbSetPath Lib "esbapin" (ByVal Path As String) As Long

'*****************************************************************************
'  Application Functions
'*****************************************************************************

Declare Function EsbGetActive Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal szApp As Integer, ByVal DbName As String, ByVal szDb As Integer, pAccess As Integer) As Long

Declare Function EsbSetActive Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, pAccess As Integer) As Long

Declare Function EsbClearActive Lib "esbapin" (ByVal hCtx As Long) As Long

Declare Function EsbListApplications Lib "esbapin" (ByVal hCtx As Long, pItems As Integer) As Long
 
Declare Function EsbCreateApplication Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String) As Long

Declare Function EsbCreateStorageTypedApplication Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal storageType As Integer) As Long

Declare Function EsbGetAllowedStorageTypes Lib "esbapin" (ByVal hCtx As Long, pAllowedTypes As ESB_ALLOWEDSTORAGETYPES_T) As Long

Declare Function EsbDeleteApplication Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String) As Long

Declare Function EsbCopyApplication Lib "esbapin" (ByVal hCtx As Long, ByVal hSrcCtx As Long, ByVal AppName As String, ByVal nAppName As String) As Long

Declare Function EsbRenameApplication Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal nAppName As String) As Long

Declare Function EsbGetApplicationState Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, pAppState As ESB_APPSTATE_T) As Long

Declare Function EsbSetApplicationState Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, pAppState As ESB_APPSTATE_T) As Long

Declare Function EsbGetApplicationInfo Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, pAppInfo As ESB_APPINFO_T, pItems As Integer) As Long
 
Declare Function EsbGetApplicationInfoEx Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, pItems As Integer) As Long

Declare Function EsbLoadApplication Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String) As Long

Declare Function EsbUnloadApplication Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String) As Long

'*****************************************************************************
'   Database Functions
'*****************************************************************************

Declare Function EsbListDatabases Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, pItems As Integer) As Long

Declare Function EsbListCurrencyDatabases Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, pItems As Integer) As Long
   
Declare Function EsbCreateDatabase Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal DbType As Integer) As Long

Declare Function EsbDeleteDatabase Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String) As Long

Declare Function EsbCopyDatabase Lib "esbapin" (ByVal hCtx As Long, ByVal hSrcCtx As Long, ByVal AppName As String, ByVal nAppName As String, ByVal DbName As String, ByVal nDbName As String) As Long

Declare Function EsbRenameDatabase Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal nDbName As String) As Long
 
Declare Function EsbGetDatabaseState Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, pDbState As ESB_DBSTATE_T) As Long

Declare Function EsbSetDatabaseState Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, pDbState As ESB_DBSTATE_T) As Long

Declare Function EsbGetDatabaseInfo Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, pDbInfo As ESB_DBINFO_T, pItems As Integer) As Long
                       
Declare Function EsbGetDatabaseNote Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal DbNote As String, ByVal szDbNote As Integer) As Long

Declare Function EsbSetDatabaseNote Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal DbNote As String) As Long

Declare Function EsbGetDatabaseStats Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, pDbStats As ESB_DBSTATS_T, pItems As Integer) As Long

Declare Function EsbLoadDatabase Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String) As Long

Declare Function EsbUnloadDatabase Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String) As Long

Declare Function EsbClearDatabase Lib "esbapin" (ByVal hCtx As Long) As Long

Declare Function EsbCommitDatabase Lib "esbapin" (ByVal hCtx As Long) As Long

Declare Function EsbQueryDatabaseMembers Lib "esbapin" (ByVal hCtx As Long, ByVal mbrQuery As String) As Long

Declare Function EsbBuildDimension Lib "esbapin" (ByVal hCtx As Long, pRules As ESB_OBJDEF_T, pData As ESB_OBJDEF_T, pUser As ESB_MBRUSER_T, ByVal ErrName As String) As Long

Declare Function EsbBuildDimStart Lib "esbapin" (ByVal hCtx As Long) As Long

Declare Function EsbBuildDimFile Lib "esbapin" (ByVal hCtx As Long, pRules As ESB_OBJDEF_T, pData As ESB_OBJDEF_T, pUser As ESB_MBRUSER_T, ByVal ErrName As String, ByVal ErrFileOverwrite As Integer) As Long

Declare Function EsbGetMemberInfo Lib "esbapin" (ByVal hCtx As Long, ByVal MbrName As String, MbrInfo As ESB_MEMBERINFO_T) As Long

Declare Function EsbGetMemberCalc Lib "esbapin" (ByVal hCtx As Long, ByVal MbrName As String, ByVal MbrCalc As String, ByVal szMbrCalc As Integer, ByVal MbrLastCalc As String, ByVal szMbrLastCalc As Integer) As Long

Declare Function EsbGetCurrencyRateInfo Lib "esbapin" (ByVal hCtx As Long, pItems As Integer) As Long

Declare Function EsbGetDimensionInfo Lib "esbapin" (ByVal hCtx As Long, ByVal Dimension As String, pItems As Integer) As Long

Declare Function EsbValidateDB Lib "esbapin" (ByVal hCtx As Long, ByVal DbName As String, ByVal FileName As String) As Long

'*****************************************************************************
' Alias tables related Esbs
'*****************************************************************************

Declare Function EsbLoadAlias Lib "esbapin" (ByVal hCtx As Long, ByVal AltName As String, ByVal FileName As String) As Long
   
Declare Function EsbRemoveAlias Lib "esbapin" (ByVal hCtx As Long, ByVal AltName As String) As Long
      
Declare Function EsbListAliases Lib "esbapin" (ByVal hCtx As Long, pItems As Integer) As Long

Declare Function EsbSetAlias Lib "esbapin" (ByVal hCtx As Long, ByVal AltName As String) As Long

Declare Function EsbGetAlias Lib "esbapin" (ByVal hCtx As Long, ByVal AltName As String, ByVal szName As Integer) As Long

Declare Function EsbDisplayAlias Lib "esbapin" (ByVal hCtx As Long, ByVal AltName As String, pItems As Integer) As Long

Declare Function EsbClearAliases Lib "esbapin" (ByVal hCtx As Long) As Long

'*****************************************************************************
'  Reporting/Update/Calculation Functions
'*****************************************************************************

Declare Function EsbReport Lib "esbapin" (ByVal hCtx As Long, ByVal isOutput As Integer, ByVal isLock As Integer, ByVal rptQuery As String) As Long

Declare Function EsbBeginReport Lib "esbapin" (ByVal hCtx As Long, ByVal isOutput As Integer, ByVal isLock As Integer) As Long
 
Declare Function EsbEndReport Lib "esbapin" (ByVal hCtx As Long) As Long

Declare Function EsbUpdate Lib "esbapin" (ByVal hCtx As Long, ByVal isStore As Integer, ByVal isUnlock As Integer, ByVal updQuery As String) As Long
 
Declare Function EsbBeginUpdate Lib "esbapin" (ByVal hCtx As Long, ByVal isStore As Integer, ByVal isUpdate As Integer) As Long

Declare Function EsbEndUpdate Lib "esbapin" (ByVal hCtx As Long) As Long

Declare Function EsbSendString Lib "esbapin" (ByVal hCtx As Long, ByVal sndString As String) As Long

Declare Function EsbGetString Lib "esbapin" (ByVal hCtx As Long, ByVal getString As String, ByVal szString As Integer) As Long

Declare Function EsbGetStringBuf Lib "esbapin" (ByVal hCtx As Long, ByVal getString As String, ByVal szString As Integer) As Long

Declare Function EsbCalc Lib "esbapin" (ByVal hCtx As Long, ByVal isCalculate As Integer, ByVal cscQuery As String) As Long

Declare Function EsbBeginCalc Lib "esbapin" (ByVal hCtx As Long, ByVal isCalculate As Integer) As Long

Declare Function EsbEndCalc Lib "esbapin" (ByVal hCtx As Long) As Long

Declare Function EsbDefaultCalc Lib "esbapin" (ByVal hCtx As Long) As Long

Declare Function EsbGetDefaultCalc Lib "esbapin" (ByVal hCtx As Long, ByVal cscString As String, ByVal szString As Integer) As Long

Declare Function EsbSetDefaultCalc Lib "esbapin" (ByVal hCtx As Long, ByVal cscString As String) As Long

'*****************************************************************************
'  File Functions
'*****************************************************************************

Declare Function EsbReportFile Lib "esbapin" (ByVal hDestCtx As Long, ByVal hSrcCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal FileName As String, ByVal isOutput As Integer, ByVal isLock As Integer) As Long

Declare Function EsbUpdateFile Lib "esbapin" (ByVal hDestCtx As Long, ByVal hSrcCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal FileName As String, ByVal isStore As Integer, ByVal isUnlock As Integer) As Long
     
Declare Function EsbCalcFile Lib "esbapin" (ByVal hDestCtx As Long, ByVal hSrcCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal FileName As String, ByVal isCalculate As Integer) As Long

Declare Function EsbSetDefaultCalcFile Lib "esbapin" (ByVal hDestCtx As Long, ByVal hSrcCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal FileName As String) As Long

Declare Function EsbArchive Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal FilePath As String, ByVal isData As Integer) As Long

Declare Function EsbArchiveBegin Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal FileName As String) As Long

Declare Function EsbArchiveEnd Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String) As Long

Declare Function EsbRestore Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal FilePath As String, ByVal isData As Integer) As Long

Declare Function EsbExport Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal FilePath As String, ByVal Level As Integer, ByVal isColumns As Integer) As Long
                                                                                                                                                                                          
Declare Function EsbImport Lib "esbapin" (ByVal hCtx As Long, pRules As ESB_OBJDEF_T, pData As ESB_OBJDEF_T, User As ESB_MBRUSER_T, ByVal ErrName As String, ByVal isAbortOnError As Integer) As Long

'*****************************************************************************
'  Object Functions
'*****************************************************************************

Declare Function EsbGetLocalPath Lib "esbapin" (ByVal hCtx As Long, ByVal ObjType As Long, ByVal AppName As String, ByVal DbName As String, ByVal ObjName As String, ByVal isCreate As Integer, ByVal Path As String, ByVal szPath As Integer) As Long

Declare Function EsbGetObjectInfo Lib "esbapin" (ByVal hCtx As Long, ByVal ObjType As Long, ByVal AppName As String, ByVal DbName As String, ByVal ObjName As String, ObjInfo As ESB_OBJINFO_T) As Long

Declare Function EsbListObjects Lib "esbapin" (ByVal hCtx As Long, ByVal ObjType As Long, ByVal AppName As String, ByVal DbName As String, pItems As Integer) As Long
 
Declare Function EsbGetObject Lib "esbapin" (ByVal hCtx As Long, ByVal ObjType As Long, ByVal AppName As String, ByVal DbName As String, ByVal ObjName As String, ByVal LocalName As String, ByVal isLock As Integer) As Long

Declare Function EsbPutObject Lib "esbapin" (ByVal hCtx As Long, ByVal ObjType As Long, ByVal AppName As String, ByVal DbName As String, ByVal ObjName As String, ByVal LocalName As String, ByVal isUnlock As Integer) As Long

Declare Function EsbLockObject Lib "esbapin" (ByVal hCtx As Long, ByVal ObjType As Long, ByVal AppName As String, ByVal DbName As String, ByVal ObjName As String) As Long

Declare Function EsbUnlockObject Lib "esbapin" (ByVal hCtx As Long, ByVal ObjType As Long, ByVal AppName As String, ByVal DbName As String, ByVal ObjName As String) As Long

Declare Function EsbCreateObject Lib "esbapin" (ByVal hCtx As Long, ByVal ObjType As Long, ByVal AppName As String, ByVal DbName As String, ByVal ObjName As String) As Long

Declare Function EsbDeleteObject Lib "esbapin" (ByVal hCtx As Long, ByVal ObjType As Long, ByVal AppName As String, ByVal DbName As String, ByVal ObjName As String) As Long

Declare Function EsbRenameObject Lib "esbapin" (ByVal hCtx As Long, ByVal ObjType As Long, ByVal AppName As String, ByVal DbName As String, ByVal ObjName As String, ByVal nObjName As String) As Long

Declare Function EsbCopyObject Lib "esbapin" (ByVal hCtx As Long, ByVal hDestCtx As Long, ByVal ObjType As Long, ByVal AppName As String, ByVal nAppName As String, ByVal DbName As String, ByVal nDbName As String, ByVal ObjName As String, ByVal nObjName As String) As Long

Declare Function EsbWriteToLogFile Lib "esbapin" (ByVal hCtx As Long, ByVal AgentLog As Boolean, ByVal Message As String) As Long


'*****************************************************************************
'  User/Group Functions
'*****************************************************************************

Declare Function EsbListUsers Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, pItems As Integer) As Long

Declare Function EsbCreateUser Lib "esbapin" (ByVal hCtx As Long, ByVal userName As String, ByVal Password As String) As Long

Declare Function EsbDeleteUser Lib "esbapin" (ByVal hCtx As Long, ByVal userName As String) As Long

Declare Function EsbDeleteGroup Lib "esbapin" (ByVal hCtx As Long, ByVal GrpName As String) As Long

Declare Function EsbRenameUser Lib "esbapin" (ByVal hCtx As Long, ByVal userName As String, ByVal nUserName As String) As Long

Declare Function EsbRenameGroup Lib "esbapin" (ByVal hCtx As Long, ByVal GrpName As String, ByVal nGrpName As String) As Long

Declare Function EsbGetUser Lib "esbapin" (ByVal hCtx As Long, ByVal userName As String, pUserInfo As ESB_USERINFO_T) As Long

Declare Function EsbGetGroup Lib "esbapin" (ByVal hCtx As Long, ByVal GrpName As String, pUserInfo As ESB_USERINFO_T) As Long

Declare Function EsbSetUser Lib "esbapin" (ByVal hCtx As Long, pUserInfo As ESB_USERINFO_T) As Long

Declare Function EsbSetGroup Lib "esbapin" (ByVal hCtx As Long, pUserInfo As ESB_USERINFO_T) As Long

Declare Function EsbSetPassword Lib "esbapin" (ByVal hCtx As Long, ByVal userName As String, ByVal Password As String) As Long

Declare Function EsbListConnections Lib "esbapin" (ByVal hCtx As Long, pItems As Integer) As Long
   
Declare Function EsbListLocks Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, pItems As Integer) As Long

Declare Function EsbRemoveLocks Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal LoginId As Long) As Long

Declare Function EsbGetApplicationAccess Lib "esbapin" (ByVal hCtx As Long, ByVal User As String, ByVal AppName As String, pItems As Integer) As Long

Declare Function EsbSetApplicationAccess Lib "esbapin" (ByVal hCtx As Long, ByVal Items As Integer, pUserApp As ESB_USERAPP_T) As Long

Declare Function EsbGetDatabaseAccess Lib "esbapin" (ByVal hCtx As Long, ByVal User As String, ByVal AppName As String, ByVal DbName As String, pItems As Integer) As Long

Declare Function EsbSetDatabaseAccess Lib "esbapin" (ByVal hCtx As Long, ByVal Items As Integer, pUserDb As ESB_USERDB_T) As Long

Declare Function EsbListGroups Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, pItems As Integer) As Long

Declare Function EsbCreateGroup Lib "esbapin" (ByVal hCtx As Long, ByVal GrpName As String) As Long

Declare Function EsbGetGroupList Lib "esbapin" (ByVal hCtx As Long, ByVal GrpName As String, pItems As Integer) As Long

Declare Function EsbSetGroupList Lib "esbapin" (ByVal hCtx As Long, ByVal GrpName As String, ByVal GrpList As String, ByVal Items As Integer) As Long

Declare Function EsbAddToGroup Lib "esbapin" (ByVal hCtx As Long, ByVal GrpName As String, ByVal User As String) As Long
           
Declare Function EsbDeleteFromGroup Lib "esbapin" (ByVal hCtx As Long, ByVal GrpName As String, ByVal User As String) As Long

Declare Function EsbGetCalcList Lib "esbapin" (ByVal hCtx As Long, ByVal User As String, ByVal AppName As String, ByVal DbName As String, isAllCalcs As Integer, pItems As Integer) As Long

Declare Function EsbSetCalcList Lib "esbapin" (ByVal hCtx As Long, ByVal User As String, ByVal AppName As String, ByVal DbName As String, ByVal isAllCalcs As Integer, ByVal CalcList As String, ByVal Items As Integer) As Long

'*****************************************************************************
'  Security Filter Functions
'*****************************************************************************

Declare Function EsbListFilters Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, pItems As Integer) As Long
Declare Function EsbGetFilter Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal FltName As String, isActive As Integer, pAccess As Integer) As Long
Declare Function EsbGetFilterRow Lib "esbapin" (ByVal hCtx As Long, ByVal FltRow As String, ByVal szRow As Integer, pAccess As Integer) As Long
Declare Function EsbSetFilter Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal FltName As String, ByVal isActive As Integer, ByVal pAccess As Integer) As Long
Declare Function EsbVerifyFilter Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String) As Long
Declare Function EsbVerifyFilterRow Lib "esbapin" (ByVal hCtx As Long, ByVal FltRow As Any) As Long
Declare Function EsbSetFilterRow Lib "esbapin" (ByVal hCtx As Long, ByVal FltRow As Any, ByVal pAccess As Integer) As Long
Declare Function EsbGetFilterList Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal FltName As String, pItems As Integer) As Long
Declare Function EsbSetFilterList Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal FltName As String, ByVal UserList As String, ByVal Items As Integer) As Long
Declare Function EsbDeleteFilter Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal FltName As String) As Long
Declare Function EsbRenameFilter Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal DbName As String, ByVal FltName As String, ByVal nFltName As String) As Long
Declare Function EsbCopyFilter Lib "esbapin" (ByVal hCtx As Long, ByVal hSrcCtx As Long, ByVal AppName As String, ByVal nAppName As String, ByVal DbName As String, ByVal nDbName As String, ByVal FltName As String, ByVal nFltName As String) As Long

'*****************************************************************************
'  Miscellaneous Functions
'*****************************************************************************

Declare Function EsbCheckMemberName Lib "esbapin" (ByVal hCtx As Long, ByVal MemName As String, isOk As Integer) As Long
Declare Function EsbGetProcessState Lib "esbapin" (ByVal hCtx As Long, ProcState As ESB_PROCSTATE_T) As Long
Declare Function EsbCancelProcess Lib "esbapin" (ByVal hCtx As Long) As Long
Declare Function EsbGetLogFile Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String, ByVal TimeStamp As Long, ByVal LocalName As String) As Long
Declare Function EsbDeleteLogFile Lib "esbapin" (ByVal hCtx As Long, ByVal AppName As String) As Long
Declare Function EsbGetGlobalState Lib "esbapin" (ByVal hCtx As Long, pGlobal As ESB_GLOBAL_T) As Long
Declare Function EsbSetGlobalState Lib "esbapin" (ByVal hCtx As Long, pGlobal As ESB_GLOBAL_T) As Long
Declare Function EsbGetNextItem Lib "esbapin" (ByVal hCtx As Long, ByVal dType As Integer, pItem As Any) As Long
Declare Function EsbGetMessage Lib "esbapin" (ByVal hInst As Long, ErrLevel As Integer, ErrNum As Long, ByVal ErrMessage As String, ByVal szErrMessage As Integer) As Long
Declare Function EsbValidateHCtx Lib "esbapin" (ByVal hCtx As Long) As Long

'*****************************************************************************
'  Substitution Variable Functions
'*****************************************************************************

Declare Function EsbCreateVariable Lib "esbapin" (ByVal hCtx As Long, pVariable As ESB_VARIABLE_T) As Long
Declare Function EsbDeleteVariable Lib "esbapin" (ByVal hCtx As Long, pVariable As ESB_VARIABLE_T) As Long
Declare Function EsbGetVariable Lib "esbapin" (ByVal hCtx As Long, pVariable As ESB_VARIABLE_T) As Long
Declare Function EsbListVariables Lib "esbapin" (ByVal hCtx As Long, pVariable As ESB_VARIABLE_T, pItems As Integer) As Long

'*****************************************************************************
'  Linked Reporting Object (LRO) Functions
'*****************************************************************************

Declare Function EsbLROAddObject Lib "esbapin" (ByVal hCtx As Long, ByVal memCount As Long, ByVal memComb As String, ByVal usOption As Integer, pLRODesc As ESB_LRODESC_API_T) As Long
Declare Function EsbLRODeleteObject Lib "esbapin" (ByVal hCtx As Long, pLinkID As ESB_LROHANDLE_API_T) As Long
Declare Function EsbLROUpdateObject Lib "esbapin" (ByVal hCtx As Long, pLinkID As ESB_LROHANDLE_API_T, ByVal usOption As Integer, pLRODesc As ESB_LRODESC_API_T) As Long
Declare Function EsbLROGetObject Lib "esbapin" (ByVal hCtx As Long, pLinkID As ESB_LROHANDLE_API_T, ByVal targetFile As String, ByVal usOption As Integer, pLRODesc As ESB_LRODESC_API_T) As Long
Declare Function EsbLROGetCatalog Lib "esbapin" (ByVal hCtx As Long, ByVal memCount As Long, ByVal memComb As String, pulCount As Integer) As Long
Declare Function EsbLRODeleteCellObjects Lib "esbapin" (ByVal hCtx As Long, ByVal memCount As Long, ByVal memComb As String, pulCount As Integer) As Long
Declare Function EsbLROListObjects Lib "esbapin" (ByVal hCtx As Long, ByVal userName As String, ByVal listDate As Long, pulCount As Integer) As Long
Declare Function EsbLROPurgeObjects Lib "esbapin" (ByVal hCtx As Long, ByVal userName As String, ByVal purgeDate As Long, pulCount As Integer) As Long
Declare Function EsbLROGetMemberCombo Lib "esbapin" (ByVal hCtx As Long, ByVal memberIndex As Long, ByVal MemberName As String) As Long

'*****************************************************************************
'  Partition Functions
'*****************************************************************************

Declare Function EsbPartitionGetReplCells Lib "esbapin" (ByVal hCtx As Long, ReplicatedRegion As ESB_PART_REPL_T, ByVal HostAppDbList As String) As Long
Declare Function EsbPartitionPutReplCells Lib "esbapin" (ByVal hCtx As Long, ReplicatedRegion As ESB_PART_REPL_T, ByVal HostAppDbList As String) As Long
Declare Function EsbPartitionGetList Lib "esbapin" (ByVal hCtx As Long, SelectRegion As ESB_PARTSLCT_T, pusCount As Integer) As Long
Declare Function EsbPartitionGetAreaCellCount Lib "esbapin" (ByVal hCtx As Long, ByVal pszSlice As String, pdCount As Double) As Long
Declare Function EsbPartitionApplyOtlChangeFile Lib "esbapin" (ByVal hCtx As Long, ByVal usfilenum As Integer, ByVal filelist As String) As Long
Declare Function EsbPartitionGetOtlChanges Lib "esbapin" (ByVal hCtx As Long, MetaQuery As ESB_PARTOTL_QUERY_T, ByVal ChangeFile As String, ByVal szChangeFile As Integer) As Long
Declare Function EsbPartitionPurgeOtlChangeFile Lib "esbapin" (ByVal hCtx As Long, pRegion As ESB_PART_DEFINED_T, ByVal TimeStamp As Long) As Long
Declare Function EsbPartitionResetOtlChangeTime Lib "esbapin" (ByVal hCtx As Long, pSourceRegion As ESB_PART_DEFINED_T, pDestRegion As ESB_PART_DEFINED_T) As Long
'*****************************************************************************
'  Outline Functions
'*****************************************************************************

Declare Function EsbOtlNewOutline Lib "esbotln.DLL" (ByVal hCtx As Long, pNewInfo As ESB_OUTLINEINFO_T, phOutline As Long) As Long

Declare Function EsbOtlOpenOutlineQuery Lib "esbotln" (ByVal hCtx As Long, pObject As ESB_OBJDEF_T, phOutline As Long) As Long
Declare Function EsbOtlWriteOutline Lib "esbotln" (ByVal hOutline As Long, pObject As ESB_OBJDEF_T) As Long
Declare Function EsbOtlRestructure Lib "esbotln" (ByVal hCtx As Long, ByVal usRestructType As Integer) As Long
Declare Function EsbOtlCloseOutline Lib "esbotln" (ByVal hOutline As Long) As Long
Declare Function EsbOtlGetOutlineInfo Lib "esbotln" (ByVal hOutline As Long, pInfo As ESB_OUTLINEINFO_T, pusCount As Integer) As Long
Declare Function EsbOtlSetOutlineInfo Lib "esbotln" (ByVal hOutline As Long, pInfo As ESB_OUTLINEINFO_T) As Long
Declare Function EsbOtlVerifyOutline Lib "esbotln" (ByVal hOutline As Long, pulErrors As Long, pulCount As Long) As Long
Declare Function EsbOtlSortChildren Lib "esbotln" (ByVal hOutline As Long, ByVal hParent As Long, ByVal usType As Integer) As Long
Declare Function EsbOtlGenerateCurrencyOutline Lib "esbotln" (ByVal hOutline As Long, phCurOutline As Long) As Long
Declare Function EsbOtlAddMember Lib "esbotln" (ByVal hOutline As Long, pMemberInfo As ESB_MBRINFO_T, ByVal hParent As Long, ByVal hPrevSibling As Long, phMember As Long) As Long
Declare Function EsbOtlAddDimension Lib "esbotln" (ByVal hOutline As Long, pMemberInfo As ESB_MBRINFO_T, ByVal hPrevSibling As Long, ByVal pszDataMbr As String, phMember As Long) As Long
Declare Function EsbOtlDeleteMember Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long) As Long
Declare Function EsbOtlDeleteDimension Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, ByVal pszDataMbr As String) As Long
Declare Function EsbOtlRenameMember Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, ByVal pszNewMember As String) As Long
Declare Function EsbOtlMoveMember Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, ByVal hNewParent As Long, ByVal hNewPrevSibling As Long) As Long
Declare Function EsbOtlFindMember Lib "esbotln" (ByVal hOutline As Long, ByVal pszMember As String, phMember As Long) As Long
Declare Function EsbOtlFindAlias Lib "esbotln" (ByVal hOutline As Long, ByVal pszAlias As String, ByVal pszAliasTable As String, phMember As Long) As Long
Declare Function EsbOtlGetMemberInfo Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, pInfo As ESB_MBRINFO_T) As Long
Declare Function EsbOtlSetMemberInfo Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, pInfo As ESB_MBRINFO_T) As Long
Declare Function EsbOtlGetFirstMember Lib "esbotln" (ByVal hOutline As Long, phMember As Long) As Long
Declare Function EsbOtlGetChild Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, phMember As Long) As Long
Declare Function EsbOtlGetParent Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, phMember As Long) As Long
Declare Function EsbOtlGetNextSibling Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, phMember As Long) As Long
Declare Function EsbOtlGetPrevSibling Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, phMember As Long) As Long
Declare Function EsbOtlGetNextSharedMember Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, phMember As Long) As Long
Declare Function EsbOtlGetMemberFormula Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, ByVal pszFormula As String, ByVal usBufSize As Integer) As Long
Declare Function EsbOtlGetMemberLastFormula Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, ByVal pszFormula As String, ByVal usBufSize As Integer) As Long
Declare Function EsbOtlSetMemberFormula Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, ByVal pszFormula As String) As Long
Declare Function EsbOtlDeleteMemberFormula Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long) As Long
Declare Function EsbOtlGetMemberAlias Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, ByVal pszAliasTable As String, ByVal pszAlias As String) As Long
Declare Function EsbOtlSetMemberAlias Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, ByVal pszAliasTable As String, ByVal pszAlias As String) As Long
Declare Function EsbOtlDeleteMemberAlias Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, ByVal pszAliasTable As String) As Long
Declare Function EsbOtlGetUserAttributes Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, pusCount As Integer) As Long
Declare Function EsbOtlSetUserAttribute Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, ByVal pszString As String) As Long
Declare Function EsbOtlDeleteUserAttribute Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, ByVal pszString As String) As Long
Declare Function EsbOtlAddAliasCombination Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, ByVal pszAliasTable As String, ByVal pszAlias As String, ByVal pszCombination As String) As Long
Declare Function EsbOtlDeleteAliasCombination Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, ByVal pszAliasTable As String, ByVal pszAlias As String) As Long
Declare Function EsbOtlGetNextAliasCombination Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, ByVal pszAliasTable As String, ByVal pszAlias As String, ByVal pszCombination As String, ByVal usBufSize As Integer) As Long
Declare Function EsbOtlCreateAliasTable Lib "esbotln" (ByVal hOutline As Long, ByVal pszAliasTable As String) As Long
Declare Function EsbOtlCopyAliasTable Lib "esbotln" (ByVal hOutline As Long, ByVal pszSourceAliasTable As String, ByVal pszDestAliasTable As String, ByVal fMerge As Integer) As Long
Declare Function EsbOtlRenameAliasTable Lib "esbotln" (ByVal hOutline As Long, ByVal pszAliasTable As String, ByVal pszNewAliasTable As String) As Long
Declare Function EsbOtlClearAliasTable Lib "esbotln" (ByVal hOutline As Long, ByVal pszAliasTable As String) As Long
Declare Function EsbOtlDeleteAliasTable Lib "esbotln" (ByVal hOutline As Long, ByVal pszAliasTable As String) As Long
Declare Function EsbOtlSetGenName Lib "esbotln" (ByVal hOutline As Long, ByVal pszDimension As String, ByVal usGen As Integer, ByVal pszName As String) As Long
Declare Function EsbOtlGetGenName Lib "esbotln" (ByVal hOutline As Long, ByVal pszDimension As String, ByVal usGen As Integer, ByVal pszName As String) As Long
Declare Function EsbOtlGetGenNames Lib "esbotln" (ByVal hOutline As Long, ByVal pszDimension As String, ByVal ulOptions As Long, pulCount As Long) As Long
Declare Function EsbOtlDeleteGenName Lib "esbotln" (ByVal hOutline As Long, ByVal pszDimension As String, ByVal usGen As Integer) As Long
Declare Function EsbOtlSetLevelName Lib "esbotln" (ByVal hOutline As Long, ByVal pszDimension As String, ByVal usLevel As Integer, ByVal pszName As String) As Long
Declare Function EsbOtlGetLevelName Lib "esbotln" (ByVal hOutline As Long, ByVal pszDimension As String, ByVal usLevel As Integer, ByVal pszName As String) As Long
Declare Function EsbOtlGetLevelNames Lib "esbotln" (ByVal hOutline As Long, ByVal pszDimension As String, ByVal ulOptions As Long, pulCount As Long) As Long
Declare Function EsbOtlDeleteLevelName Lib "esbotln" (ByVal hOutline As Long, ByVal pszDimension As String, ByVal usLevel As Integer) As Long
Declare Function EsbOtlQueryMembers Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long, pPredicate As ESB_PREDICATE_T, pCounts As ESB_MBRCOUNTS_T) As Long
Declare Function EsbOtlQueryMembersByName Lib "esbotln" (ByVal hOutline As Long, ByVal pszMember As String, pPredicate As ESB_PREDICATE_T, pCounts As ESB_MBRCOUNTS_T) As Long
Declare Function EsbOtlFreeMember Lib "esbotln" (ByVal hOutline As Long, ByVal hMember As Long) As Long
Declare Function EsbOtlGetDimensionUserAttributes Lib "esbotln" (ByVal hOutline As Long, pPredicate As ESB_PREDICATE_T, pCounts As ESB_MBRCOUNTS_T) As Long
Declare Function EsbOtlGetUpdateTime Lib "esbotln" (ByVal hOutline As Long, ByRef TimeStamp As Long) As Long

'*****************************************************************************
'  Outline Functions for Dynamic Time Series
'*****************************************************************************
Declare Function EsbOtlGetDTSMemberAlias Lib "esbotln" (ByVal hOutline As Long, ByVal pszDTSMember As String, ByVal pszAliasTable As String, ByVal pszAlias As String) As Long
Declare Function EsbOtlSetDTSMemberAlias Lib "esbotln" (ByVal hOutline As Long, ByVal pszDTSMember As String, ByVal pszAlias As String, ByVal pszAliasTable As String) As Long
Declare Function EsbOtlDeleteDTSMemberAlias Lib "esbotln" (ByVal hOutline As Long, ByVal pszDTSMember As String, ByVal pszAliasTable As String) As Long
Declare Function EsbOtlEnableDTSMember Lib "esbotln" (ByVal hOutline As Long, ByVal pszDTSMember As String, ByVal usGen As Integer, ByVal bEnable As Integer) As Long
Declare Function EsbOtlGetEnabledDTSMembers Lib "esbotln" (ByVal hOutline As Long, pusCount As Integer) As Long

'*****************************************************************************
'  Attribute functions
'*****************************************************************************

Declare Function EsbGetAttributeInfo Lib "esbapin" (ByVal hCtx As Long, ByVal AttrName As String, AttrInfo As ESB_ATTRIBUTEINFO_T) As Long
Declare Function EsbCheckAttributes Lib "esbapin" (ByVal hCtx As Long, ByVal Count As Integer, ByRef AttrNameArray() As String, ByRef AttrTypeArray As Variant) As Long
Declare Function EsbGetAssociatedAttributesInfo Lib "esbapin" (ByVal hCtx As Long, ByVal MbrName As String, ByVal AttrDimName As String, ByRef Count As Long) As Long
Declare Function EsbGetAttributeSpecifications Lib "esbapin" (ByVal hCtx As Long, ByRef AttrSpecs As ESB_ATTRSPECS_T) As Long

Declare Function EsbOtlQueryAttributes Lib "esbotln" (ByVal hOutline As Long, ByRef AttrQuery As ESB_ATTRIBUTEQUERY_T, ByRef Count As Long, ByRef MemberArray As Variant) As Long
Declare Function EsbOtlGetAttributeSpecifications Lib "esbotln" (ByVal hOutline As Long, ByRef AttrSpecs As ESB_ATTRSPECS_T) As Long
Declare Function EsbOtlSetAttributeSpecifications Lib "esbotln" (ByVal hOutline As Long, ByRef AttrSpecs As ESB_ATTRSPECS_T) As Long
Declare Function EsbOtlAssociateAttributeDimension Lib "esbotln" (ByVal hOutline As Long, ByVal BaseDimension As Long, ByVal AttributeDimension As Long) As Long
Declare Function EsbOtlDisassociateAttributeDimension Lib "esbotln" (ByVal hOutline As Long, ByVal BaseDimension As Long, ByVal AttributeDimension As Long) As Long
Declare Function EsbOtlAssociateAttributeMember Lib "esbotln" (ByVal hOutline As Long, ByVal BaseMember As Long, ByVal AttributeMember As Long) As Long
Declare Function EsbOtlDisassociateAttributeMember Lib "esbotln" (ByVal hOutline As Long, ByVal BaseMember As Long, ByVal AttributeMember As Long) As Long
Declare Function EsbOtlFindAttributeMembers Lib "esbotln" (ByVal hOutline As Long, ByVal MemberName As String, ByVal DimensionName As String, ByRef Count As Integer, ByRef MemberArray As Variant) As Long
Declare Function EsbOtlGetAssociatedAttributes Lib "esbotln" (ByVal hOutline As Long, ByVal Member As Long, ByRef Count As Integer, ByRef MemberArray As Variant) As Long
Declare Function EsbOtlGetAttributeInfo Lib "esbotln" (ByVal hOutline As Long, ByVal Member As Long, ByRef AttrInfo As ESB_ATTRIBUTEINFO_T) As Long


'*****************************************************************************
'  Location Alias functions
'*****************************************************************************

Declare Function EsbCreateLocationAlias Lib "esbapin" (ByVal hCtx As Long, ByVal AliasName As String, ByVal HostName As String, ByVal AppName As String, ByVal DbName As String, ByVal Login As String, ByVal Password As String) As Long
Declare Function EsbDeleteLocationAlias Lib "esbapin" (ByVal hCtx As Long, ByVal AliasName As String) As Long
Declare Function EsbGetLocationAliasList Lib "esbapin" (ByVal hCtx As Long, ByRef ListCount As Integer, ByRef Aliases As Variant, _
            ByRef Hosts As Variant, ByRef AppNames As Variant, ByRef DbNames As Variant, ByRef UserNames As Variant) As Long
