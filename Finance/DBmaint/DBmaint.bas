Attribute VB_Name = "EssMaint"
Public Server As String
Public Application As String
Public DB As String
Public User As String
Public Psswrd As String

Public hInst As Long
Public hCtx As Long

Public CurrQtr As String
Public CurrMth As String
Public CurrDay As String
Public CurrWk As String
Public NextWk As String
Public LastWk As String
Public WhileAgoMth As String
Public LYNumOfWeeks As Integer

Public dAlign As String
Public dRestruct As String
Public dSubVar As String
Public dWebIni As String
Public dReRunWebIni As String
Public dAlignPath As String

Public twkformcy As String
Public twkformly As String
Public fwkformcy As String
Public fwkformly As String

Public Sub Main()
Dim SubVar As String
Dim VarForm As String
Dim GroupMaint As String
Dim ItemAge As String
Dim Restruct As String

Dim words() As String

    EssFile = App.Path & "\" & "DBmaint.ini"
    Open EssFile For Input As #2
    Line Input #2, Server
    Line Input #2, Application
    Line Input #2, DB
    Line Input #2, User
    Line Input #2, Psswrd
    Line Input #2, temp
    Line Input #2, dAlign
    Line Input #2, dRestruct
    Line Input #2, dSubVar
    Line Input #2, sundayAlign
    Line Input #2, dDisconnect
    Line Input #2, LYNumOfWks
    Line Input #2, temp
    Line Input #2, dWebIni
    Line Input #2, dReRunWebIni
    Line Input #2, dAllAlignPath
    Line Input #2, dAlignPath
    Close #2
    
    words() = Split(LYNumOfWks)
    
    LYNumOfWeeks = words(0)
    
    If Left(LCase(dDisconnect), 1) = "y" Then DisconnectAllUsers

    If Left(LCase(dAlign), 1) = "y" Then MarketMaint
    
    If Left(LCase(dSubVar), 1) = "y" Then
        SubVarMaint
        OTLMaint
    End If
    
    If Left(LCase(sundayAlign), 1) = "y" Then
    
        'If it is the FIRST week of the month (and only do this on Sunday):
        '   Change the Web pages lwWebRpts.ini to be Prior Alignment
        '   Perform MarketMaint  (deletes Recent Alignments)
        '   Run AllAlign.cmd - which recreates all the alignments just deleted
        'If it is the SECOND week of the month (and only do this on Sunday):
        '   Change the Web pages lwWebRpts.ini back to Current Alignment
        If LCase(Right(CurrDay, 6)) = "sunday" And Right(NextWk, 1) = "2" Then
            Call ChangeLW("Prior Alignment", dWebIni)
            Call ChangeLW("Prior Alignment", dReRunWebIni)
            MarketMaint
        
            'Make a copy of the prior files as backup
            FileCopy dAlignPath & "Prior.txt", dAlignPath & "Prior " & CurrMth & ".txt"
            FileCopy dAlignPath & "pdmgrs.xls", dAlignPath & "pdmgrs " & CurrMth & ".xls"
            FileCopy dAlignPath & "prmgrs.xls", dAlignPath & "prmgrs " & CurrMth & ".xls"
            'This file is not in production yet!!!
            'FileCopy dAlignPath & "pmmgrs.xls", dAlignPath & "pmmgrs " & CurrMth & ".xls"
            
            Call ShellAndWait(dAllAlignPath, vbNormalFocus)
            
        ElseIf LCase(Right(CurrDay, 6)) = "sunday" And Right(NextWk, 1) = "3" Then
            Call ChangeLW("Current Alignment", dWebIni)
            Call ChangeLW("Current Alignment", dReRunWebIni)
        End If
    End If
    
    If Left(LCase(dRestruct), 1) = "y" Then DoRestructure
End Sub

Public Sub ChangeLW(NewAlign As String, iniPath As String)
Dim tLine As String

    Open dWebIni For Input As #4
    Open App.Path & "\temp.ini" For Output As #5
    For x = 1 To 19
        Line Input #4, tLine
        
        If x = 15 Then
            pos = InStrRev(iniPath, "\")
            tLine = Left(iniPath, pos - 1)
        End If
        
        Print #5, tLine
    Next x
    
    Line Input #4, tLine
    If Right(tLine, 9) = "Alignment" Then tLine = NewAlign
    Print #5, tLine
    Close #4
    Close #5
    
    'Copy File to Report Location
    FileCopy App.Path & "\temp.ini", iniPath

End Sub

Public Sub MarketMaint()
'This procedure will delete the "Recent Alignments" alternate hierarchy in the Market dimension
Dim Object As ESB_OBJDEF_T
Dim hOutline As Long
Dim ProcState As ESB_PROCSTATE_T

    Open App.Path & "\" & "DBmaint.log" For Append As #5
    
    Call Login
    
    Object.hCtx = hCtx
    Object.Type = ESB_OBJTYPE_OUTLINE
    Object.AppName = Application
    Object.DbName = DB
    Object.FileName = DB
    
    sts = EsbOtlOpenOutline(hCtx, Object, ESB_YES, ESB_YES, hOutline)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "Open Outline failed. Code = " & sts
        Call MaintErrors("Open Outline failed. Code = " & sts)
        GoTo TheEnd
    End If

    'Delete the old groups from the Essbase outline
    sts = EsbOtlFindMember(hOutline, "Recent Alignments", hMemberParent)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "Could not find the member Recent Alignments. Code = " & sts
        Call MaintErrors("Could not find the member Recent Alignments. Code = " & sts)
        GoTo TheEnd
    End If
    sts = EsbOtlDeleteMember(hOutline, hMemberParent)
    If sts = 0 Then
        Write #5, Date & " " & Time & " " & "Successfully deleted Recent Alignments"
    Else
        Write #5, Date & " " & Time & " " & "Delete member error. Code = " & sts
        Call MaintErrors("Delete member error. Code = " & sts)
        GoTo TheEnd
    End If
                    
    sts = EsbOtlWriteOutline(hOutline, Object)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "WriteOutline Error. Code = " & sts
        Call MaintErrors("WriteOutline Error. Code = " & sts)
        GoTo TheEnd
    End If
    
    sts = EsbOtlRestructure(hCtx, ESB_DOR_ALLDATA)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "Restructure Error. Code = " & sts
        Call MaintErrors("Restructure Error. Code = " & sts)
        GoTo TheEnd
    End If
    sts = EsbGetProcessState(hCtx, ProcState)
    Do While ProcState.State <> ESB_STATE_DONE
      sts = EsbGetProcessState(hCtx, ProcState)
    Loop

TheEnd:
    sts = EsbUnlockObject(hCtx, Object.Type, Application, DB, DB)
    sts = EsbOtlCloseOutline(hOutline)

    Call Logout
        
    Write #5, Date & " " & Time & " " & "***** Market Dimension maintenance complete *****"
    Close #5

End Sub

Public Sub SubVarMaint()
'This procedure will update applicable substitution variables

    Dim sysdate As Date
    Dim LastSun As Date
    
    Dim SubVar As ESB_VARIABLE_T
    Dim sts As Long
    Dim dbl_quote As String
    
    Dim hWk As Long
    Dim hMth As Long
    Dim hQtr As Long
    
    Dim Object As ESB_OBJDEF_T
    Dim hOutline As Long
    Dim newfd(500) As String
    Dim MbrInfo As ESB_MBRINFO_T
    
    Dim CurrWkSun As String
    
    Dim ShortAgoWk As String
    Dim ShortAgoMth As String
    Dim ShortAgoQtr As String
    Dim ShortAgoSun As String
    Dim WhileAgoWk As String
    Dim WhileAgoQtr As String
    Dim WhileAgoSun As String
    Dim LongAgoWk As String
    Dim LongAgoMth As String
    Dim LongAgoQtr As String
    Dim LongAgoSun As String

    Dim DOW(7) As String
    Dim dowINT As Integer
    Dim xx As Integer
    
    DOW(1) = "Sunday"
    DOW(2) = "Monday"
    DOW(3) = "Tuesday"
    DOW(4) = "Wednesday"
    DOW(5) = "Thursday"
    DOW(6) = "Friday"
    DOW(7) = "Saturday"
    
    On Error GoTo proc_exit
    dbl_quote = """"
    
    Open App.Path & "\" & "DBmaint.log" For Append As #5
    Write #5, Date & " " & Time & " " & "***** Substitution Variable Maintenance Start *****"
    
    'Step 1:  Based on the system clock get the Last Processed Week
    
    sysdate = Format(Now - 7, "MM/DD/YYYY")
    dowINT = Weekday(Format(Now - 1, "MM/DD/YYYY"))
    
    LastSun = Now - Weekday(Format(Now, "MM/DD/YYYY")) + 1
    
    temp = tempDate(Format(Now, "MM/DD/YYYY"))
    CurrDay = "Week " & Mid(temp, 2, Len(temp) - 6) & " " & DOW(dowINT)
    
    CurrWkSun = "Week " & Mid(temp, 2, Len(temp) - 6) & " " & "Sunday"
    
    'Step 2:  Update the Substitution Variable LastWk with the value of CurrDay
    
    Call Login
    
    Object.hCtx = hCtx
    Object.Type = ESB_OBJTYPE_OUTLINE
    Object.AppName = Application
    Object.DbName = DB
    Object.FileName = DB
        
    sts = EsbOtlOpenOutline(hCtx, Object, ESB_NO, ESB_NO, hOutline)
    If sts = 0 Then
        
        'Put all the weeks in the DB into 'newfd' array
        cnt = 0
        sts = EsbOtlFindMember(hOutline, "Total Year", hMemberParent)
        q = EsbOtlGetChild(hOutline, hMemberParent, hqtrs)
        Do While hqtrs <> 0
            m = EsbOtlGetChild(hOutline, hqtrs, hmths)
            Do While hmths <> 0
                w = EsbOtlGetChild(hOutline, hmths, hwks)
                Do While hwks <> 0
                    sts = EsbOtlGetMemberInfo(hOutline, hwks, MbrInfo)
                    newfd(cnt) = Mid(MbrInfo.szMember, 1, InStr(1, MbrInfo.szMember, Chr$(0)) - 1)
                    cnt = cnt + 1
                    hLastWk = hwks
                    w = EsbOtlGetNextSibling(hOutline, hLastWk, hwks)
                Loop
                hlastmth = hmths
                m = EsbOtlGetNextSibling(hOutline, hlastmth, hmths)
            Loop
            hlastqtr = hqtrs
            q = EsbOtlGetNextSibling(hOutline, hlastqtr, hqtrs)
            
            'There 2 alternate hierarchies "Current Week" & "Contribution" that we need to ignore
            sts = EsbOtlGetMemberInfo(hOutline, hqtrs, MbrInfo)
            temp = Mid(MbrInfo.szMember, 1, InStr(1, MbrInfo.szMember, Chr$(0)) - 1)
            If temp = "Current Week" Or temp = "Contribution" Then Exit Do
        Loop
    End If
    
    'Get the parent of CurrDay
    sts = EsbOtlFindMember(hOutline, CurrDay, hMbr)
    sts = EsbOtlGetParent(hOutline, hMbr, hWk)
    sts = EsbOtlGetMemberInfo(hOutline, hWk, MbrInfo)
    CurrWk = Mid(MbrInfo.szMember, 1, InStr(1, MbrInfo.szMember, Chr$(0)) - 1)
    
    'Get the parent of CurrWk
    sts = EsbOtlGetParent(hOutline, hWk, hMth)
    sts = EsbOtlGetMemberInfo(hOutline, hMth, MbrInfo)
    CurrMth = Mid(MbrInfo.szMember, 1, InStr(1, MbrInfo.szMember, Chr$(0)) - 1)
    
    'Get the parent of CurrMth
    sts = EsbOtlGetParent(hOutline, hMth, hQtr)
    sts = EsbOtlGetMemberInfo(hOutline, hQtr, MbrInfo)
    CurrQtr = Mid(MbrInfo.szMember, 1, InStr(1, MbrInfo.szMember, Chr$(0)) - 1)
    
    xx = 0
    Do While CurrWk <> newfd(xx)
        xx = xx + 1
        If xx > cnt Then Write #5, Date & " " & Time & " " & "Substitution Variable update failed.  Could not find CurrWk in the outline."
    Loop
    If xx > 0 Then
        LastWk = newfd(xx - 1)
    ElseIf LYNumOfWeeks = 52 Then
        LastWk = newfd(cnt - 2)
    Else
        '53 Weeks
        LastWk = newfd(cnt - 1)
    End If
    
    If xx > 3 Then
        ShortAgoWk = newfd(xx - 4)
        '****** Build formula for 4-Weeks start *****
        fwkformcy = ""
        fwkformly = ""
        For iss = 1 To 4
            fwkformcy = fwkformcy & dbl_quote & newfd(xx - iss) & dbl_quote & "->" & dbl_quote & "Current Year" & dbl_quote & "+" & vbCrLf
            fwkformly = fwkformly & dbl_quote & newfd(xx - iss) & dbl_quote & "->" & dbl_quote & "Last Year" & dbl_quote & "+" & vbCrLf
        Next iss
        fwkformcy = Mid(fwkformcy, 1, Len(fwkformcy) - 3) & ";" & vbCrLf
        fwkformly = Mid(fwkformly, 1, Len(fwkformly) - 3) & ";" & vbCrLf
        ''****** Build formula for 4-Weeks end *****
    Else
        
        If LYNumOfWeeks = 52 Then
            '52 Weeks
            ShortAgoWk = newfd(xx - 5 + cnt)
        Else
            '53 Weeks
            ShortAgoWk = newfd(xx - 4 + cnt)
        End If
        
        '****** Build formula for 4-Weeks start *****
        '2 years ago
        fwkformcy = ""
        fwkformly = ""
        For iss = 1 To xx
            fwkformcy = fwkformcy & dbl_quote & newfd(xx - iss) & dbl_quote & "->" & dbl_quote & "Current Year" & dbl_quote & "+" & vbCrLf
            fwkformly = fwkformly & dbl_quote & newfd(xx - iss) & dbl_quote & "->" & dbl_quote & "Last Year" & dbl_quote & "+" & vbCrLf
        Next iss
        For iss = 1 To Abs(xx - 4)
            If LYNumOfWeeks = 52 Then
                '52 weeks
                fwkformcy = fwkformcy & dbl_quote & newfd(52 - iss) & dbl_quote & "->" & dbl_quote & "Last Year" & dbl_quote & "+" & vbCrLf
                fwkformly = fwkformly & dbl_quote & newfd(52 - iss) & dbl_quote & "->" & dbl_quote & "2 years ago" & dbl_quote & "+" & vbCrLf
            Else
                '53 weeks
                fwkformcy = fwkformcy & dbl_quote & newfd(53 - iss) & dbl_quote & "->" & dbl_quote & "Last Year" & dbl_quote & "+" & vbCrLf
                fwkformly = fwkformly & dbl_quote & newfd(53 - iss) & dbl_quote & "->" & dbl_quote & "2 years ago" & dbl_quote & "+" & vbCrLf
            End If
        Next iss
        fwkformcy = Mid(fwkformcy, 1, Len(fwkformcy) - 3) & ";" & vbCrLf
        fwkformly = Mid(fwkformly, 1, Len(fwkformly) - 3) & ";" & vbCrLf
        '****** Build formula for 4-Weeks end *****
    End If
    
    'If 53 or 52 weeks next week should start over again. Subtract one because zero based
    If LYNumOfWeeks - 1 = xx And xx < cnt Then
        NextWk = newfd(0)   'New Fiscal Year, Need to start over
    ElseIf xx < cnt Then
        NextWk = newfd(xx + 1)
    End If
    'If xx < cnt Then NextWk = newfd(xx + 1)
    
    'Get the parent of ShortAgoWk
    sts = EsbOtlFindMember(hOutline, ShortAgoWk, hWk)
    sts = EsbOtlGetParent(hOutline, hWk, hMth)
    sts = EsbOtlGetMemberInfo(hOutline, hMth, MbrInfo)
    ShortAgoMth = Mid(MbrInfo.szMember, 1, InStr(1, MbrInfo.szMember, Chr$(0)) - 1)
    'Get the child of ShortAgoWk
    sts = EsbOtlGetChild(hOutline, hWk, hMbr)
    sts = EsbOtlGetMemberInfo(hOutline, hMbr, MbrInfo)
    ShortAgoSun = Mid(MbrInfo.szMember, 1, InStr(1, MbrInfo.szMember, Chr$(0)) - 1)
    'Get the parent of WhileAgoMth
    sts = EsbOtlGetParent(hOutline, hMth, hQtr)
    sts = EsbOtlGetMemberInfo(hOutline, hQtr, MbrInfo)
    ShortAgoQtr = Mid(MbrInfo.szMember, 1, InStr(1, MbrInfo.szMember, Chr$(0)) - 1)
    
    If xx > 7 Then
        WhileAgoWk = newfd(xx - 8)
    Else
        If LYNumOfWeeks = 52 Then
            WhileAgoWk = newfd(xx - 9 + cnt)
        Else
            WhileAgoWk = newfd(xx - 8 + cnt)
        End If
    End If
    'Get the parent of WhileAgoWk
    sts = EsbOtlFindMember(hOutline, WhileAgoWk, hWk)
    sts = EsbOtlGetParent(hOutline, hWk, hMth)
    sts = EsbOtlGetMemberInfo(hOutline, hMth, MbrInfo)
    WhileAgoMth = Mid(MbrInfo.szMember, 1, InStr(1, MbrInfo.szMember, Chr$(0)) - 1)
    'Get the child of WhileAgoWk
    sts = EsbOtlGetChild(hOutline, hWk, hMbr)
    sts = EsbOtlGetMemberInfo(hOutline, hMbr, MbrInfo)
    WhileAgoSun = Mid(MbrInfo.szMember, 1, InStr(1, MbrInfo.szMember, Chr$(0)) - 1)
    'Get the parent of WhileAgoMth
    sts = EsbOtlGetParent(hOutline, hMth, hQtr)
    sts = EsbOtlGetMemberInfo(hOutline, hQtr, MbrInfo)
    WhileAgoQtr = Mid(MbrInfo.szMember, 1, InStr(1, MbrInfo.szMember, Chr$(0)) - 1)
    
    If xx > 12 Then
        LongAgoWk = newfd(xx - 12)
        '****** Build formula for 13-Weeks start *****
        twkformcy = ""
        twkformly = ""
        For iss = 1 To 13
            twkformcy = twkformcy & dbl_quote & newfd(xx - iss) & dbl_quote & "->" & dbl_quote & "Current Year" & dbl_quote & "+" & vbCrLf
            twkformly = twkformly & dbl_quote & newfd(xx - iss) & dbl_quote & "->" & dbl_quote & "Last Year" & dbl_quote & "+" & vbCrLf
        Next iss
        twkformcy = Mid(twkformcy, 1, Len(twkformcy) - 3) & ";" & vbCrLf
        twkformly = Mid(twkformly, 1, Len(twkformly) - 3) & ";" & vbCrLf
        '****** Build formula for 13-Weeks end *****
    Else
        If xx = 12 Then
            LongAgoWk = newfd(0)
        Else
            If LYNumOfWeeks = 52 Then
                LongAgoWk = newfd(xx - 13 + cnt)
            Else
                LongAgoWk = newfd(xx - 12 + cnt)
            End If
        End If
        '****** Build formula for 13-Weeks start *****
        '2 years ago
        twkformcy = ""
        twkformly = ""
        For iss = 1 To xx
            twkformcy = twkformcy & dbl_quote & newfd(xx - iss) & dbl_quote & "->" & dbl_quote & "Current Year" & dbl_quote & "+" & vbCrLf
            twkformly = twkformly & dbl_quote & newfd(xx - iss) & dbl_quote & "->" & dbl_quote & "Last Year" & dbl_quote & "+" & vbCrLf
        Next iss
        For iss = 1 To Abs(xx - 13)
            If LYNumOfWeeks = 52 Then
                twkformcy = twkformcy & dbl_quote & newfd(52 - iss) & dbl_quote & "->" & dbl_quote & "Last Year" & dbl_quote & "+" & vbCrLf
                twkformly = twkformly & dbl_quote & newfd(52 - iss) & dbl_quote & "->" & dbl_quote & "2 years ago" & dbl_quote & "+" & vbCrLf
            Else
                twkformcy = twkformcy & dbl_quote & newfd(53 - iss) & dbl_quote & "->" & dbl_quote & "Last Year" & dbl_quote & "+" & vbCrLf
                twkformly = twkformly & dbl_quote & newfd(53 - iss) & dbl_quote & "->" & dbl_quote & "2 years ago" & dbl_quote & "+" & vbCrLf
            End If
        Next iss
        twkformcy = Mid(twkformcy, 1, Len(twkformcy) - 3) & ";" & vbCrLf
        twkformly = Mid(twkformly, 1, Len(twkformly) - 3) & ";" & vbCrLf
        '****** Build formula for 13-Weeks end *****
    End If
    'Get the parent of WhileAgoWk
    sts = EsbOtlFindMember(hOutline, LongAgoWk, hWk)
    sts = EsbOtlGetParent(hOutline, hWk, hMth)
    sts = EsbOtlGetMemberInfo(hOutline, hMth, MbrInfo)
    LongAgoMth = Mid(MbrInfo.szMember, 1, InStr(1, MbrInfo.szMember, Chr$(0)) - 1)
    'Get the child of WhileAgoWk
    sts = EsbOtlGetChild(hOutline, hWk, hMbr)
    sts = EsbOtlGetMemberInfo(hOutline, hMbr, MbrInfo)
    LongAgoSun = Mid(MbrInfo.szMember, 1, InStr(1, MbrInfo.szMember, Chr$(0)) - 1)
    'Get the parent of WhileAgoMth
    sts = EsbOtlGetParent(hOutline, hMth, hQtr)
    sts = EsbOtlGetMemberInfo(hOutline, hQtr, MbrInfo)
    LongAgoQtr = Mid(MbrInfo.szMember, 1, InStr(1, MbrInfo.szMember, Chr$(0)) - 1)
    
    sts = EsbOtlCloseOutline(hOutline)
    
    SubVar.Server = Server
    SubVar.AppName = Application
    SubVar.DbName = DB
    
    'Update the XLDay in Essbase
    SubVar.VarName = "XLDay"
    SubVar.VarValue = CurrDay
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "XLDay Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "XLDay = " & CurrDay
    End If
    'Update the CurrDay in Essbase
    SubVar.VarName = "CurrDay"
    SubVar.VarValue = dbl_quote & CurrDay & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "CurrDay Substitution Variable update failed. Code = " & sts
        Call MaintErrors("CurrDay Substitution Variable update failed. Code = " & sts)
    Else
        Write #5, Date & " " & Time & " " & "CurrDay = " & CurrDay
    End If
    'Update the CurrWk in Essbase
    SubVar.VarName = "CurrWk"
    SubVar.VarValue = dbl_quote & CurrWk & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "CurrWk Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "CurrWk = " & CurrWk
    End If
    'Update the CurrMth in Essbase
    SubVar.VarName = "CurrMth"
    SubVar.VarValue = dbl_quote & CurrMth & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "CurrMth Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "CurrMth = " & CurrMth
    End If
    'Update the CurrQtr in Essbase
    SubVar.VarName = "CurrQtr"
    SubVar.VarValue = dbl_quote & CurrQtr & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "CurrQtr Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "CurrQtr = " & CurrQtr
    End If
    'Update the LastWk in Essbase
    SubVar.VarName = "LastWk"
    SubVar.VarValue = dbl_quote & LastWk & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "LastWk Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "LastWk = " & LastWk
    End If
    'Update the CurrWkSun in Essbase
    SubVar.VarName = "CurrWkSun"
    SubVar.VarValue = dbl_quote & CurrWkSun & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "CurrWkSun Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "CurrWkSun = " & CurrWkSun
    End If
    
    'Update the 4AgoSun in Essbase
    SubVar.VarName = "4AgoSun"
    SubVar.VarValue = dbl_quote & ShortAgoSun & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "4AgoSun Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "4AgoSun = " & ShortAgoSun
    End If
    'Update the 4Ago in Essbase
    SubVar.VarName = "4Ago"
    SubVar.VarValue = dbl_quote & ShortAgoWk & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "4Ago Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "4Ago = " & ShortAgoWk
    End If
    'Update the 4AgoMth in Essbase
    SubVar.VarName = "4AgoMth"
    SubVar.VarValue = dbl_quote & ShortAgoMth & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "4AgoMth Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "4AgoMth = " & ShortAgoMth
    End If
    'Update the 4AgoQtr in Essbase
    SubVar.VarName = "4AgoQtr"
    SubVar.VarValue = dbl_quote & ShortAgoQtr & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "4AgoQtr Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "4AgoQtr = " & ShortAgoQtr
    End If
    
    'Update the 8AgoSun in Essbase
    SubVar.VarName = "8AgoSun"
    SubVar.VarValue = dbl_quote & WhileAgoSun & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "8AgoSun Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "8AgoSun = " & WhileAgoSun
    End If
    'Update the 8Ago in Essbase
    SubVar.VarName = "8Ago"
    SubVar.VarValue = dbl_quote & WhileAgoWk & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "8Ago Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "8Ago = " & WhileAgoWk
    End If
    'Update the 8AgoMth in Essbase
    SubVar.VarName = "8AgoMth"
    SubVar.VarValue = dbl_quote & WhileAgoMth & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "8AgoMth Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "8AgoMth = " & WhileAgoMth
    End If
    'Update the 8AgoQtr in Essbase
    SubVar.VarName = "8AgoQtr"
    SubVar.VarValue = dbl_quote & WhileAgoQtr & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "8AgoQtr Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "8AgoQtr = " & WhileAgoQtr
    End If
    
    'Update the 12AgoSun in Essbase
    SubVar.VarName = "12AgoSun"
    SubVar.VarValue = dbl_quote & LongAgoSun & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "12AgoSun Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "12AgoSun = " & LongAgoSun
    End If
    'Update the 12Ago in Essbase
    SubVar.VarName = "12Ago"
    SubVar.VarValue = dbl_quote & LongAgoWk & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "12Ago Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "12Ago = " & LongAgoWk
    End If
    'Update the 12AgoMth in Essbase
    SubVar.VarName = "12AgoMth"
    SubVar.VarValue = dbl_quote & LongAgoMth & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "12AgoMth Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "12AgoMth = " & LongAgoMth
    End If
    'Update the 12AgoQtr in Essbase
    SubVar.VarName = "12AgoQtr"
    SubVar.VarValue = dbl_quote & LongAgoQtr & dbl_quote
    sts = EsbCreateVariable(hCtx, SubVar)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "12AgoQtr Substitution Variable update failed. Code = " & sts
    Else
        Write #5, Date & " " & Time & " " & "12AgoQtr = " & LongAgoQtr
    End If
    
proc_exit:
    Close #1
    
    Write #5, Date & " " & Time & " " & "***** Substitution Variable Maintenance Complete *****"
    Close #5
    
    Call Logout

End Sub

Private Sub Login()
    Dim sts As Long
    Dim pAccess As Integer
    Dim pAccess2 As Integer

    Dim init As ESB_INIT_T

    init.Version = ESB_API_VERSION
    sts = EsbInit(init, hInst)

    sts = EsbAutoLogin(hInst, Server, User, Psswrd, Application, DB, 0, pAccess, hCtx)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "Essbase login attempt failed!  Code = " & sts
        Call MaintErrors("Essbase login attempt failed!  Code = " & sts)
    End If

    sts = EsbSetActive(hCtx, Application, DB, pAccess2)
End Sub
 
Private Sub Logout()
    Dim sts As Long
    
    
    sts = EsbLogout(hCtx)
    sts = EsbTerm(hInst)
End Sub

Function tempDate(ByRef findDate As Date) As String
    Dim WorkLine As String
    Dim DateFile As String
    Dim caldate As Date
    Dim FiscalPeriod  As String
    
    DateFile = App.Path & "\" & "calendar.txt"
    Open DateFile For Input As #1
    
    Line Input #1, WorkLine
    
    tpos = InStr(1, WorkLine, ",")
    caldate = Left(WorkLine, tpos - 1)
    FiscalPeriod = Mid(WorkLine, tpos + 1, Len(WorkLine))
    Do Until findDate <= caldate
        prev = FiscalPeriod
        Line Input #1, WorkLine
        
        tpos = InStr(1, WorkLine, ",")
        caldate = Left(WorkLine, tpos - 1)
        FiscalPeriod = Mid(WorkLine, tpos + 1, Len(WorkLine))
    Loop
    If sysdate = caldate Then
        tempDate = FiscalPeriod
    Else
        tempDate = prev
    End If
    
    Close #1
End Function

Public Sub OTLMaint()
Dim Object As ESB_OBJDEF_T
Dim hOutline As Long
Dim hMbr As Long
Dim ProcState As ESB_PROCSTATE_T
Dim temp As String
Dim szFormula As String
Dim dbl_quote As String

    dbl_quote = """"
    Open App.Path & "\" & "DBmaint.log" For Append As #5
    
    Call Login
    
    Object.hCtx = hCtx
    Object.Type = ESB_OBJTYPE_OUTLINE
    Object.AppName = Application
    Object.DbName = DB
    Object.FileName = DB
    
    sts = EsbOtlOpenOutline(hCtx, Object, ESB_YES, ESB_YES, hOutline)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "Open Outline failed. Code = " & sts
        Call MaintErrors("Open Outline failed. Code = " & sts)
        GoTo TheEnd
    End If
    
    
    'Find Curr Sunday (alias) and rename the member
    hMbr = 0
    sts = EsbOtlFindAlias(hOutline, "Curr Sunday", "", hMbr)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "Could not find the member Curr Sunday. Code = " & sts
        Call MaintErrors("Could not find the member Curr Sunday. Code = " & sts)
        GoTo TheEnd
    End If
    temp = " "
    temp = "Week " & Mid(CurrDay, 6, InStr(6, CurrDay, " ") - 6) & " Sunday"
    sts = EsbOtlRenameMember(hOutline, hMbr, temp)
    If sts = 0 Then
        Write #5, Date & " " & Time & " " & "Successfully renamed Curr Sunday to " & temp
    Else
        Write #5, Date & " " & Time & " " & "Rename Curr Sunday Error. Code = " & sts
        GoTo TheEnd
    End If

    'Find Curr Monday (alias) and rename the member
    hMbr = 0
    sts = EsbOtlFindAlias(hOutline, "Curr Monday", "", hMbr)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "Could not find the member Curr Monday. Code = " & sts
        GoTo TheEnd
    End If
    temp = " "
    temp = "Week " & Mid(CurrDay, 6, InStr(6, CurrDay, " ") - 6) & " Monday"
    sts = EsbOtlRenameMember(hOutline, hMbr, temp)
    If sts = 0 Then
        Write #5, Date & " " & Time & " " & "Successfully renamed Curr Monday to " & temp
    Else
        Write #5, Date & " " & Time & " " & "Rename Curr Monday Error. Code = " & sts
        GoTo TheEnd
    End If

    'Find Curr Tuesday (alias) and rename the member
    hMbr = 0
    sts = EsbOtlFindAlias(hOutline, "Curr Tuesday", "", hMbr)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "Could not find the member Curr Tuesday. Code = " & sts
        GoTo TheEnd
    End If
    temp = " "
    temp = "Week " & Mid(CurrDay, 6, InStr(6, CurrDay, " ") - 6) & " Tuesday"
    sts = EsbOtlRenameMember(hOutline, hMbr, temp)
    If sts = 0 Then
        Write #5, Date & " " & Time & " " & "Successfully renamed Curr Tuesday to " & temp
    Else
        Write #5, Date & " " & Time & " " & "Rename Curr Tuesday Error. Code = " & sts
        GoTo TheEnd
    End If

    'Find Curr Wednesday (alias) and rename the member
    hMbr = 0
    sts = EsbOtlFindAlias(hOutline, "Curr Wednesday", "", hMbr)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "Could not find the member Curr Wednesday. Code = " & sts
        GoTo TheEnd
    End If
    temp = " "
    temp = "Week " & Mid(CurrDay, 6, InStr(6, CurrDay, " ") - 6) & " Wednesday"
    sts = EsbOtlRenameMember(hOutline, hMbr, temp)
    If sts = 0 Then
        Write #5, Date & " " & Time & " " & "Successfully renamed Curr Wednesday to " & temp
    Else
        Write #5, Date & " " & Time & " " & "Rename Curr Wednesday Error. Code = " & sts
        GoTo TheEnd
    End If

    'Find Curr Thursday (alias) and rename the member
    hMbr = 0
    sts = EsbOtlFindAlias(hOutline, "Curr Thursday", "", hMbr)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "Could not find the member Curr Thursday. Code = " & sts
        GoTo TheEnd
    End If
    temp = " "
    temp = "Week " & Mid(CurrDay, 6, InStr(6, CurrDay, " ") - 6) & " Thursday"
    sts = EsbOtlRenameMember(hOutline, hMbr, temp)
    If sts = 0 Then
        Write #5, Date & " " & Time & " " & "Successfully renamed Curr Thursday to " & temp
    Else
        Write #5, Date & " " & Time & " " & "Rename Curr Thursday Error. Code = " & sts
        GoTo TheEnd
    End If

    'Find Curr Friday (alias) and rename the member
    hMbr = 0
    sts = EsbOtlFindAlias(hOutline, "Curr Friday", "", hMbr)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "Could not find the member Curr Friday. Code = " & sts
        GoTo TheEnd
    End If
    temp = " "
    temp = "Week " & Mid(CurrDay, 6, InStr(6, CurrDay, " ") - 6) & " Friday"
    sts = EsbOtlRenameMember(hOutline, hMbr, temp)
    If sts = 0 Then
        Write #5, Date & " " & Time & " " & "Successfully renamed Curr Friday to " & temp
    Else
        Write #5, Date & " " & Time & " " & "Rename Curr Friday Error. Code = " & sts
        GoTo TheEnd
    End If

    'Find Curr Saturday (alias) and rename the member
    hMbr = 0
    sts = EsbOtlFindAlias(hOutline, "Curr Saturday", "", hMbr)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "Could not find the member Curr Saturday. Code = " & sts
        GoTo TheEnd
    End If
    temp = " "
    temp = "Week " & Mid(CurrDay, 6, InStr(6, CurrDay, " ") - 6) & " Saturday"
    sts = EsbOtlRenameMember(hOutline, hMbr, temp)
    If sts = 0 Then
        Write #5, Date & " " & Time & " " & "Successfully renamed Curr Saturday to " & temp
    Else
        Write #5, Date & " " & Time & " " & "Rename Curr Saturday Error. Code = " & sts
        GoTo TheEnd
    End If
                    
    sts = EsbOtlFindMember(hOutline, "Last Week", hMbr)
    If sts = 0 And hMbr <> 0 Then
       szFormula = dbl_quote & LastWk & dbl_quote & ";"
       sts = EsbOtlSetMemberFormula(hOutline, hMbr, szFormula)
    End If
                    
'****************** This section has been added Sept 2004 for 2 new variables:  *****************
'******************                  4-Weeks  &  13-Weeks                       *****************
    sts = EsbOtlFindMember(hOutline, "13-Weeks", hMbr)
    If sts = 0 And hMbr <> 0 Then
       szFormula = "IF (@ISMBR(" & dbl_quote & "Last Year" & dbl_quote & "))" & vbCrLf & twkformly & "ELSE" & vbCrLf & twkformcy & "ENDIF"
       sts = EsbOtlSetMemberFormula(hOutline, hMbr, szFormula)
    End If
    sts = EsbOtlFindMember(hOutline, "4-Weeks", hMbr)
    If sts = 0 And hMbr <> 0 Then
       szFormula = "IF (@ISMBR(" & dbl_quote & "Last Year" & dbl_quote & "))" & vbCrLf & fwkformly & "ELSE" & vbCrLf & fwkformcy & "ENDIF"
       sts = EsbOtlSetMemberFormula(hOutline, hMbr, szFormula)
    End If
                    
    sts = EsbOtlWriteOutline(hOutline, Object)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "WriteOutline Error. Code = " & sts
        Call MaintErrors("WriteOutline Error. Code = " & sts)
        GoTo TheEnd
    End If
    
    sts = EsbOtlRestructure(hCtx, ESB_DOR_ALLDATA)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "Restructure Error. Code = " & sts
        Call MaintErrors("Restructure Error. Code = " & sts)
        GoTo TheEnd
    End If
    sts = EsbGetProcessState(hCtx, ProcState)
    Do While ProcState.State <> ESB_STATE_DONE
      sts = EsbGetProcessState(hCtx, ProcState)
    Loop

TheEnd:
    sts = EsbUnlockObject(hCtx, Object.Type, Application, DB, DB)
    sts = EsbOtlCloseOutline(hOutline)

    Call Logout
        
    Write #5, Date & " " & Time & " " & "***** Total Year Dimension maintenance complete *****"
    Close #5

End Sub

Public Sub DoRestructure()
'This procedure will perform a dense restructure
Dim Object As ESB_OBJDEF_T
Dim hOutline As Long
Dim ProcState As ESB_PROCSTATE_T
Dim MbrInfo As ESB_MBRINFO_T
    
    Open App.Path & "\" & "DBmaint.log" For Append As #5
    Write #5, Date & " " & Time & " " & "***** Restructure Start *****"
    
    Call Login
    
    Object.hCtx = hCtx
    Object.Type = ESB_OBJTYPE_OUTLINE
    Object.AppName = Application
    Object.DbName = DB
    Object.FileName = DB
    
    sts = EsbOtlOpenOutline(hCtx, Object, ESB_YES, ESB_YES, hOutline)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "Open Outline failed. Code = " & sts
        Call MaintErrors("Open Outline failed. Code = " & sts)
        GoTo TheEnd
    End If
    
    sts = EsbOtlFindMember(hOutline, "xxx", hMbr)
    'If xxx is in the outline, delete it - otherwise add it to force a restructure
    If hMbr <> 0 Then
        sts = EsbOtlDeleteMember(hOutline, hMbr)
    Else
        sts = EsbOtlFindMember(hOutline, "LY %", hMember)
        MbrInfo.szMember = "xxx"
        MbrInfo.usConsolidation = ESB_UCALC_NOOP
        MbrInfo.usShare = ESB_SHARE_NEVER
        sts = EsbOtlAddMember(hOutline, MbrInfo, ESB_NULL, hMember, hMbr)
    End If

    sts = EsbOtlWriteOutline(hOutline, Object)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "WriteOutline Error. Code = " & sts
        Call MaintErrors("WriteOutline Error. Code = " & sts)
        GoTo TheEnd
    End If
    
    sts = EsbOtlRestructure(hCtx, ESB_DOR_ALLDATA)
    If sts <> 0 Then
        Write #5, Date & " " & Time & " " & "Restructure Error. Code = " & sts
        Call MaintErrors("Restructure Error. Code = " & sts)
        GoTo TheEnd
    End If
    sts = EsbGetProcessState(hCtx, ProcState)
    Do While ProcState.State <> ESB_STATE_DONE
      sts = EsbGetProcessState(hCtx, ProcState)
    Loop

TheEnd:
    sts = EsbUnlockObject(hCtx, Object.Type, Application, DB, DB)
    sts = EsbOtlCloseOutline(hOutline)

    Call Logout
        
    Write #5, Date & " " & Time & " " & "***** Restructure Complete *****"
    Close #5

End Sub

Public Sub MaintErrors(Message As String)
    Open App.Path & "\" & "MaintErr.Err" For Append As #7
    Write #7, Date & " " & Time & " " & Message
    Close #7
End Sub

Sub DisconnectAllUsers()
  Dim Count As Integer
  Dim n As Integer
  Dim UserInfo As ESB_USERINFO_T
  Dim FirstTime As Boolean
  
  Open App.Path & "\" & "DBmaint.log" For Append As #5
  Write #5, Date & " " & Time & " " & "***** Disconnect Users Start *****"

  Call Login
  FirstTime = True
  
  sts = EsbListConnections(hCtx, Count)
  For n = 1 To Count
    sts = EsbGetNextItem(hCtx, ESB_USERINFO_TYPE, UserInfo)
    
    sts = EsbLogoutUser(hCtx, UserInfo.LoginId)
    If sts = 0 Then
        Write #5, Date & " " & Time & " Disconnecting User: " & Mid$(UserInfo.Name, 1, InStr(1, UserInfo.Name, Chr$(0)) - 1)
    ElseIf sts = 1051020 Then
        'Cannot log myself out!
    Else
        If FirstTime Then Open App.Path & "\" & "dbConnections.log" For Output As #6
        Write #6, Date & " " & Time & " Could NOT disconnect user: " & Mid$(UserInfo.Name, 1, InStr(1, UserInfo.Name, Chr$(0)) - 1) & " Reason code: " & sts
        FirstTime = False
    End If
  Next n
  
  Write #5, Date & " " & Time & " " & "***** Disconnect Users End *****"
  
  Call Logout
  
  Close #6
  Close #5
End Sub

