using System;
using System.Data;   //by fja
using System.Collections.Generic;
using System.Net.Mail;
using System.Data.SqlClient; 


namespace HTError
{
	/// <summary>
	/// Holds a collection of Log Levels
	/// </summary>
	public class LogLevelCollection : System.Collections.Generic.List<LogLevel>
	{
	}

	/// <summary>
	/// Holds a collection of email addresses, and provides helepr methods for converting that collection 
    /// to/from a single delimited string
	/// </summary>
	public class EmailRecipientCollection : System.Collections.Generic.List<string>
	{
		private const string EMAIL_ADDRESS_DELIMITER = ";";

		/// <summary>
		/// Sets this list equal to the passed-in semicolon-delimited email address list
		/// Any existing contents will be cleared
		/// </summary>
		/// <param name="strEmailAddressList">semicolon-delimited email address list</param>
		public void ParseString(string strEmailAddressList)
		{
			this.Clear();
			if (!String.IsNullOrEmpty(strEmailAddressList))
			{
				this.AddRange(strEmailAddressList.Split(EMAIL_ADDRESS_DELIMITER.ToCharArray()));
			}
		}

		/// <summary>
		/// Converts contents into a semicolon-delimited email address list in a single string, suitable for use in
		/// an email's To: or CC: fields
		/// </summary>
		/// <returns>semicolon-delimited email address string</returns>
		public override string ToString()
		{
			System.Text.StringBuilder EmailString = new System.Text.StringBuilder();
			foreach (string strEmail in this)
			{
				if (EmailString.Length > 0)
				{
					EmailString.Append(EMAIL_ADDRESS_DELIMITER);
				}
				EmailString.Append(strEmail);
			}
			return EmailString.ToString();
		}
	}

	/// <summary>
	/// Enumerates the different message severity levels
	/// </summary>
	public enum LogLevel
	{
		/// <summary>
		/// Debug message severity should be used for messages that should only be recorded in a debugging environment
		/// </summary>
		Debug = 0,
		/// <summary>
		/// Information message severity should be used for messages that contain non-critical information
		/// </summary>
		Information = 1,
		/// <summary>
		/// Warning message severity should be used for messages which may indicate a problem, but not an actual failure
		/// </summary>
		Warning = 2,
		/// <summary>
		/// Notify message severity should be used for messages that contain non-error information that needs to be distributed via email
		/// </summary>
		Notify = 3,
		/// <summary>
		/// Error message severity should be used for all exception and failure messages
		/// </summary>
		Error = 4
	}

    
    
	/// <summary>
	/// Provides a flexible, configurable error logging and notification system
	/// </summary>
   
	public class ErrorHandler : IHTError 
	{
		private const int HD_AVAILABLE_OK_PERCENT = 10;
		private const int RAM_AVAILABLE_OK_PERCENT = 10;
		private const long HD_SPACE_MEGABYTE = 1048576;
		private const ulong RAM_MEGABYTE = 1048576;
		private const string DATETIME_FORMAT_PATTERN = "MM/dd/yyyy hh:mm:ss tt";

       

		private class EnvironmentInfo
		{
			private string m_strDriveSpaceSummary;
			private string m_strDriveSpaceDetail;
			private string m_strNetworkInfoSummary;
			private string m_strNetworkInfoDetail;
			private string m_strAvailableRamSummary;
			private string m_strAvailableRamDetail;
			private string m_strCurrentUserSummary;
			private string m_strCurrentUserDetail;
           

			public string Summary
			{
				get
				{
					return "Environment summary: " + m_strCurrentUserSummary + m_strDriveSpaceSummary + m_strAvailableRamSummary + m_strNetworkInfoSummary;
				}
			}

			public string Detail
			{
				get
				{
					return "Environment detail:" + System.Environment.NewLine + m_strCurrentUserDetail + m_strDriveSpaceDetail + m_strAvailableRamDetail + m_strNetworkInfoDetail;
				}
			}

			public EnvironmentInfo()
			{
				Reset();
			}

			public void TakeSnapshot(bool bIncludeDriveSpace, bool bIncludeNetworkDrives, bool bIncludeRam, bool bIncludeNetwork, bool bIncludeCurrentUser)
			{
				Reset();

				if (bIncludeDriveSpace)
				{
					try
					{
						System.Text.StringBuilder DriveSpaceSummaryBuilder = new System.Text.StringBuilder();
						System.Text.StringBuilder DriveSpaceDetailBuilder = new System.Text.StringBuilder();

						foreach (System.IO.DriveInfo drive in System.IO.DriveInfo.GetDrives())
						{
							if ((drive.IsReady) && (drive.DriveType == System.IO.DriveType.Fixed || ( drive.DriveType == System.IO.DriveType.Network && bIncludeNetworkDrives)))
							{
								DriveSpaceSummaryBuilder.AppendFormat("Drive {0} is ", drive.Name);
								if (drive.TotalFreeSpace * 100 / drive.TotalSize >= HD_AVAILABLE_OK_PERCENT)
								{
									DriveSpaceSummaryBuilder.Append("OK. ");
								}
								else
								{
									DriveSpaceSummaryBuilder.AppendFormat("low (less than {0}% available). ", HD_AVAILABLE_OK_PERCENT);
								}

								DriveSpaceDetailBuilder.AppendFormat(
									"Drive {0} has {1} MB available out of {2} MB total." + System.Environment.NewLine,
									drive.Name,
									drive.TotalFreeSpace / HD_SPACE_MEGABYTE,
									drive.TotalSize / HD_SPACE_MEGABYTE);
							}
						}

						m_strDriveSpaceSummary = DriveSpaceSummaryBuilder.ToString();
						m_strDriveSpaceDetail = DriveSpaceDetailBuilder.ToString();
					}
					catch (System.IO.IOException DriveSpaceIOEx)
					{
						// If we get an error attempting to fetch drive space info, just write it to the console
						Console.WriteLine("Couldn't get available/total drive space info with IO error: " + DriveSpaceIOEx.ToString());
					}
					catch (System.UnauthorizedAccessException DriveSpaceUnauthorizedEx)
					{
						// If we get an error attempting to fetch drive space info, just write it to the console
						Console.WriteLine("Couldn't get available/total drive space info with unauthorized access error: " + DriveSpaceUnauthorizedEx.ToString());
					}
				}
				if (bIncludeNetwork)
				{
					try
					{
						if (System.Net.NetworkInformation.NetworkInterface.GetIsNetworkAvailable())
						{
							m_strNetworkInfoSummary = "Network is OK. ";
						}
						else
						{
							m_strNetworkInfoSummary = "Network is Unavailable. ";
						}

						System.Text.StringBuilder NetworkInfoDetailBuilder = new System.Text.StringBuilder();

						foreach (System.Net.NetworkInformation.NetworkInterface NetInterface in System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces())
						{
							NetworkInfoDetailBuilder.AppendFormat("Network Interface {0} status is {1}. " + System.Environment.NewLine, NetInterface.Name, NetInterface.OperationalStatus);
						}

						m_strNetworkInfoDetail = NetworkInfoDetailBuilder.ToString();
					}
					catch (System.Net.NetworkInformation.NetworkInformationException NetworkInfoEx)
					{
						// If we get an error attempting to fetch network info, just write it to the console
						Console.WriteLine("Couldn't get network status info with network error: " + NetworkInfoEx.ToString());
					}
				}
				if (bIncludeRam)
				{
					try
					{
						Microsoft.VisualBasic.Devices.ComputerInfo MyComputerInfo = new Microsoft.VisualBasic.Devices.ComputerInfo();
						ulong ulAvailablePhysicalRam = MyComputerInfo.AvailablePhysicalMemory;
						ulong ulAvailableVirtualRam = MyComputerInfo.AvailableVirtualMemory;
						ulong ulTotalPhysicalRam = MyComputerInfo.TotalPhysicalMemory;
						ulong ulTotalVirtualRam = MyComputerInfo.TotalVirtualMemory;

						string strPhysicalRamStatus;
						string strVirtualRamStatus;

						if (ulAvailablePhysicalRam * 100 / ulTotalPhysicalRam >= RAM_AVAILABLE_OK_PERCENT)
						{
							strPhysicalRamStatus = "OK";
						}
						else
						{
							strPhysicalRamStatus = "low (less than " + RAM_AVAILABLE_OK_PERCENT + "% available)";
						}

						if (ulAvailableVirtualRam * 100 / ulTotalVirtualRam >= RAM_AVAILABLE_OK_PERCENT)
						{
							strVirtualRamStatus = "OK";
						}
						else
						{
							strVirtualRamStatus = "low (less than " + RAM_AVAILABLE_OK_PERCENT + "% available)";
						}

						m_strAvailableRamSummary = "Physical memory is " + strPhysicalRamStatus + ". Virtual memory is " + strVirtualRamStatus + ". ";

						m_strAvailableRamDetail = "Physical Ram: " + ulAvailablePhysicalRam / RAM_MEGABYTE + " MB available out of "
							+ ulTotalPhysicalRam / RAM_MEGABYTE + " MB total." + System.Environment.NewLine + "Virtual Ram: "
							+ ulAvailableVirtualRam / RAM_MEGABYTE + " MB available out of " + ulTotalVirtualRam / RAM_MEGABYTE + " MB total." + System.Environment.NewLine;
					}
					catch (Exception RamInfoEx)
					{
						// If we get an error attempting to fetch RAM info, write to the console and rethrow
						Console.WriteLine("Couldn't get available/total RAM info with error: " + RamInfoEx.ToString());
						throw;
					}
				}
				if (bIncludeCurrentUser)
				{
					try
					{
                       
						string strCurrentUser = System.Environment.UserName;
						m_strCurrentUserSummary = "Current user: " + strCurrentUser + ". ";
						m_strCurrentUserDetail = "Current user: " + strCurrentUser + "." + System.Environment.NewLine;
					}
					catch (Exception UserNameEx)
					{
						// If we get an error attempting to fetch user name, write to the console and rethrow
						Console.WriteLine("Couldn't get user name with error: " + UserNameEx.ToString());
						throw;
					}
				}
			}

           
			private void Reset()
			{
				m_strDriveSpaceSummary = "";
				m_strDriveSpaceDetail = "";
				m_strNetworkInfoSummary = "";
				m_strNetworkInfoDetail = "";
				m_strAvailableRamSummary = "";
				m_strAvailableRamDetail = "";
				m_strCurrentUserSummary = "";
				m_strCurrentUserDetail = "";
			}
		}

		#region Fields
		private string m_strEmailServer;
		private string m_strProductionFilePath;
		private string m_strDebugFilePath;
       
    //    public string ConnectionString = "Data Source=CASQL05DEV;Initial Catalog=HTERROR;User ID=sa;Password=hottopic;";
      //  private string m_strConnectionString = "Data Source=CASQL05DEV;Initial Catalog=HTERROR;User ID=sa;Password=hottopic;";   
        private string m_strConnectionString ;   
       
        private string m_strAppDescription ;   //by fja

		private LogLevelCollection m_LogLevelCollection = new LogLevelCollection();
		private bool m_bSendMailOnWarning = true;
		private bool m_bSendMailOnError = true;
		private bool m_bAlsoLogToConsole;
        private bool m_bEnableLogToDB = true; //fja

		private string m_strEmailSender;
		private EmailRecipientCollection m_NotifyRecipientCollection = new EmailRecipientCollection();
		private MailPriority m_NotifyEmailPriority = MailPriority.Normal;
		private EmailRecipientCollection m_ErrorRecipientCollection = new EmailRecipientCollection();
		private MailPriority m_ErrorEmailPriority = MailPriority.High;
		private bool m_bIncludeDriveSpaceInfo = true;
		private bool m_bIncludeNetworkDrives;
		private bool m_bIncludeRamInfo = true;
		private bool m_bIncludeNetworkStatus = true;
		private bool m_bEnableDebugLog = true;
		private bool m_bEnableProductionLog = true;
      
		private bool m_bIncludeCurrentUser = true;
		private bool m_bEnableEmail;
		private bool m_bAllowExceptionThrowing;
       

		#endregion //Fields
		
		#region Properties
		/// <summary>
		/// Name or IP address of the SMTP server to use for sending email
		/// </summary>
		public string EmailServer
		{
			get { return m_strEmailServer; }
			set { m_strEmailServer = value; }
		}
		/// <summary>
		/// Full filename and path to the production (summary) log file
		/// </summary>
		public string ProductionFilePath
		{
			get { return m_strProductionFilePath; }
			set { m_strProductionFilePath = value; }
		}
		/// <summary>
		/// Full filename and path to the debug (detailed) log file
		/// </summary>
		public string DebugFilePath
		{
			get { return m_strDebugFilePath; }
			set { m_strDebugFilePath = value; }
		}
        //by fja
        /// <summary>
        /// Connection string to SQL server HTERROR database
        /// </summary>
        public string connectionString
        {
            get { return m_strConnectionString; }
            set { m_strConnectionString = value; }
        }

    
        /// <summary>
        /// Table name of debug (detailed) log file
        /// </summary>
   //     public string DebugDB
   //     {
   //         get { return m_strDebugDB; }
   //         set { m_strDebugDB = value; }
   //     }
        /// <summary>
        /// Application name
        /// </summary>
        public string AppDescription
        {
            get { return m_strAppDescription; }
            set { m_strAppDescription = value; }
        }
        // by fja
        /// <summary>
        /// Create the production log table name      
        /// </summary>
        public string ProdLogTableName
        {
            get
            {
                return "Prod" + m_strAppDescription + "Log";
            }
        }

        // by fja
        /// <summary>
        /// Create the Debug log table name      
        /// </summary>
    //    public string DebugLogTableName
    //    {
    //        get
    //        {
    //            return "Debug" + m_strAppDescription.Trim + "Log";
    //        }
    //    }
     
		/// <summary>
		/// Collection of message severities which will be logged/emailed. Attempting to log a message of a severity not
		/// in this collection will do nothing.
		/// </summary>
		public LogLevelCollection ListOfLogLevels
		{
			get { return m_LogLevelCollection; }
		}
		/// <summary>
		/// If true, warning messages will also be emailed to the ListOfErrorRecipients (defaults to true)
		/// </summary>
		public bool SendMailOnWarning
		{
			get { return m_bSendMailOnWarning; }
			set { m_bSendMailOnWarning = value; }
		}
		/// <summary>
		/// If true, error message will be emailed to the ListOfErrorRecipients (defaults to true)
		/// </summary>
		public bool SendMailOnError
		{
			get { return m_bSendMailOnError; }
			set { m_bSendMailOnError = value; }
		}
		/// <summary>
		/// If true, detailed error messages will also be written to the console. (defaults to false)
		/// </summary>
		public bool AlsoLogToConsole
		{
			get { return m_bAlsoLogToConsole; }
			set { m_bAlsoLogToConsole = value; }
		}
		/// <summary>
		/// Email address to use in From: field of all sent email
		/// </summary>
		public string EmailSender
		{
			get { return m_strEmailSender; }
			set { m_strEmailSender = value; }
		}
		/// <summary>
		/// Collection of email addresses of recipients of notification emails
		/// </summary>
		public EmailRecipientCollection ListOfNotifyRecipients
		{
			get { return m_NotifyRecipientCollection; }
		}
		/// <summary>
		/// Priority setting used on notification mails (defaults to Normal)
		/// </summary>
		public MailPriority NotifyEmailPriority
		{
			get { return m_NotifyEmailPriority; }
			set { m_NotifyEmailPriority = value; }
		}
		/// <summary>
		/// Collection of email addresses of recipients of warning/error emails
		/// </summary>
		public EmailRecipientCollection ListOfErrorRecipients
		{
			get { return m_ErrorRecipientCollection; }
		}
		/// <summary>
		/// Priority setting used on error/warning mails (defaults to High)
		/// </summary>
		public MailPriority ErrorEmailPriority
		{
			get { return m_ErrorEmailPriority; }
			set { m_ErrorEmailPriority = value; }
		}
		/// <summary>
		/// If true, information about available space on all local drives will be included in messages (defaults to true)
		/// </summary>
		public bool IncludeDriveSpaceInfo
		{
			get { return m_bIncludeDriveSpaceInfo; }
			set { m_bIncludeDriveSpaceInfo = value; }
		}
		/// <summary>
		/// If true, network drive info will be included with local drive info (defaults to false)
		/// </summary>
		public bool IncludeNetworkDrives
		{
			get { return m_bIncludeNetworkDrives; }
			set { m_bIncludeNetworkDrives = value; }
		}
		/// <summary>
		/// If true, information about available physical and virtual RAM will be included in messages (defaults to true)
		/// </summary>
		public bool IncludeRamInfo
		{
			get { return m_bIncludeRamInfo; }
			set { m_bIncludeRamInfo = value; }
		}
		/// <summary>
		/// If true, the name of the executing current user account will be included in messages (defaults to true)
		/// </summary>
		public bool IncludeCurrentUser
		{
			get { return m_bIncludeCurrentUser; }
			set { m_bIncludeCurrentUser = value; }
		}
		/// <summary>
		/// If true, information about network availability will be included in messages (defaults to true)
		/// </summary>
		public bool IncludeNetworkStatus
		{
			get { return m_bIncludeNetworkStatus; }
			set { m_bIncludeNetworkStatus = value; }
		}
		/// <summary>
		/// If true, details of messages will be logged to the debug log file.
		/// </summary>
		public bool EnableDebugLog
		{
			get { return m_bEnableDebugLog; }
			set { m_bEnableDebugLog = value; }
		}
		/// <summary>
		/// If true, summaries of messages will be logged to the production log file.
		/// </summary>
		public bool EnableProductionLog
		{
			get { return m_bEnableProductionLog; }
			set { m_bEnableProductionLog = value; }
		}
       // by fja
        /// <summary>
        /// If true, summaries of messages will be logged to database file.
        /// </summary>
        public bool EnableLogToDB
        {
            get { return m_bEnableLogToDB; }
            set { m_bEnableLogToDB = value; }
        }

		/// <summary>
		/// If true, email is allowed to be sent, and always will be on notification messages.
		/// </summary>
		public bool EnableEmail
		{
			get { return m_bEnableEmail; }
			set { m_bEnableEmail = value; }
		}
		/// <summary>
		/// If false, all unexpected exceptions while logging will be caught and logged to the console, then disposed of.
		/// If true, unexpected exceptions while logging will be re-thrown. (defaults to false)
		/// </summary>
		public bool AllowExceptionThrowing
		{
			get { return m_bAllowExceptionThrowing; }
			set { m_bAllowExceptionThrowing = value; }
		}
		#endregion //Properties

		#region Construction
		/// <summary>
		/// Creates error handler without initialization
		/// </summary>
		public ErrorHandler()
		{
		}

		/// <summary>
		/// Initializes error handler to not use email, only logging to the production and debug files
		/// </summary>
		/// <param name="strProductionFilePath">Full path and filename to the Production, summarized log</param>
		/// <param name="strDebugFilePath">Full path and filename to the Debug, detailed log</param>
		/// <param name="minimumLogLevel">Minimum message severity to be logged; all message of this level or more severe will be logged</param>
        ///  <parm name = "strAppDesc"> </parm> Application Name where for database logging </param> 
		public ErrorHandler(string strProductionFilePath, string strDebugFilePath, LogLevel minimumLogLevel, string strAppDesc)
		{
			m_strProductionFilePath = strProductionFilePath;
			m_strDebugFilePath = strDebugFilePath;
			SetMinimumLogLevel(minimumLogLevel);
            m_strAppDescription = strAppDesc; // fja
            m_strConnectionString = connectionString;
                        

		}

		/// <summary>
		/// Initializes error handler to not use email, only logging to the production and debug files
		/// </summary>
		/// <param name="strProductionFilePath">Full path and filename to the Production, summarized log</param>
		/// <param name="strDebugFilePath">Full path and filename to the Debug, detailed log</param>
		/// <param name="enabledLoggingTypes">Collection of message severity levels to log; only levels explicitly in this collection will be logged</param>
       ///  <parm name = "strAppDesc"> </parm> Application Name where for database logging </param> 
        public ErrorHandler(string strProductionFilePath, string strDebugFilePath, LogLevelCollection enabledLoggingTypes, string strAppDesc)
		{
			m_strProductionFilePath = strProductionFilePath;
			m_strDebugFilePath = strDebugFilePath;
			m_LogLevelCollection.Clear();
			m_LogLevelCollection.AddRange(enabledLoggingTypes);
            m_strAppDescription = strAppDesc; // fja
            m_strConnectionString = connectionString;
		}

		/// <summary>
		/// Initializes error handler to log to given debug and production files, and to allow sending of email
		/// </summary>
		/// <param name="strProductionFilePath">Full path and filename to the Production, summarized log</param>
		/// <param name="strDebugFilePath">Full path and filename to the Debug, detailed log</param>
		/// <param name="strEmailServer">Name or IP address of the SMTP server machine</param>
		/// <param name="strEmailSender">Email address used in the From: field on all emails</param>
		/// <param name="strEmailRecipients">Semi-colon delimited list of email addresses to receieve any email sent</param>
		/// <param name="minimumLogLevel">Minimum message severity to be logged; all message of this level or more severe will be logged</param>
		public ErrorHandler(string strProductionFilePath, string strDebugFilePath, string strEmailServer, string strEmailSender, string strEmailRecipients, LogLevel minimumLogLevel, string strAppDesc)
			: this(strProductionFilePath, strDebugFilePath, minimumLogLevel, strAppDesc)
		{
			m_strEmailServer = strEmailServer;
			m_strEmailSender = strEmailSender;
			m_NotifyRecipientCollection.ParseString(strEmailRecipients);
			m_ErrorRecipientCollection.ParseString(strEmailRecipients);
			m_bEnableEmail = true;
            m_strAppDescription = strAppDesc; // fja
            m_strConnectionString = connectionString;

		}

		/// <summary>
		/// Initializes error handler to log to given debug and production files, and to allow sending of email
		/// </summary>
		/// <param name="strProductionFilePath">Full path and filename to the Production, summarized log</param>
		/// <param name="strDebugFilePath">Full path and filename to the Debug, detailed log</param>
		/// <param name="strEmailServer">Name or IP address of the SMTP server machine</param>
		/// <param name="strEmailSender">Email address used in the From: field on all emails</param>
		/// <param name="strEmailRecipients">Semi-colon delimited list of email addresses to receieve any email sent</param>
		/// <param name="enabledLoggingTypes">Collection of message severity levels to log; only levels explicitly in this collection will be logged</param>
        public ErrorHandler(string strProductionFilePath, string strDebugFilePath, string strEmailServer, string strEmailSender, string strEmailRecipients, LogLevelCollection enabledLoggingTypes, string strAppDesc)
			: this(strProductionFilePath, strDebugFilePath, enabledLoggingTypes,strAppDesc)
		{
			m_strEmailServer = strEmailServer;
			m_strEmailSender = strEmailSender;
			m_NotifyRecipientCollection.ParseString(strEmailRecipients);
			m_ErrorRecipientCollection.ParseString(strEmailRecipients);
			m_bEnableEmail = true;
            m_strAppDescription = strAppDesc; // fja
            m_strConnectionString = connectionString;
		}

		/// <summary>
		/// Initializes error handler to log to given debug and production files, and to allow sending of email
		/// </summary>
		/// <param name="strProductionFilePath">Full path and filename to the Production, summarized log</param>
		/// <param name="strDebugFilePath">Full path and filename to the Debug, detailed log</param>
		/// <param name="strEmailServer">Name or IP address of the SMTP server machine</param>
		/// <param name="strEmailSender">Email address used in the From: field on all emails</param>
		/// <param name="strNotifyEmailRecipients">Semi-colon delimited list of email addresses to which all notification email will be sent</param>
		/// <param name="strErrorEmailRecipients">Semi-colon delimited list of email addresses to which all error/warning email will be sent</param>
		/// <param name="minimumLogLevel">Minimum message severity to be logged; all message of this level or more severe will be logged</param>
        public ErrorHandler(string strProductionFilePath, string strDebugFilePath, string strEmailServer, string strEmailSender, string strNotifyEmailRecipients, string strErrorEmailRecipients, LogLevel minimumLogLevel, string strAppDesc)
			: this(strProductionFilePath, strDebugFilePath, minimumLogLevel, strAppDesc)
		{
			m_strEmailServer = strEmailServer;
			m_strEmailSender = strEmailSender;
			m_NotifyRecipientCollection.ParseString(strNotifyEmailRecipients);
			m_ErrorRecipientCollection.ParseString(strErrorEmailRecipients);
			m_bEnableEmail = true;
            m_strAppDescription = strAppDesc; // fja
            m_strConnectionString = connectionString;
		}

		/// <summary>
		/// Initializes error handler to log to given debug and production files, and to allow sending of email
		/// </summary>
		/// <param name="strProductionFilePath">Full path and filename to the Production, summarized log</param>
		/// <param name="strDebugFilePath">Full path and filename to the Debug, detailed log</param>
		/// <param name="strEmailServer">Name or IP address of the SMTP server machine</param>
		/// <param name="strEmailSender">Email address used in the From: field on all emails</param>
		/// <param name="strNotifyEmailRecipients">Semi-colon delimited list of email addresses to which all notification email will be sent</param>
		/// <param name="strErrorEmailRecipients">Semi-colon delimited list of email addresses to which all error/warning email will be sent</param>
		/// <param name="enabledLoggingTypes">Collection of message severity levels to log; only levels explicitly in this collection will be logged</param>
        public ErrorHandler(string strProductionFilePath, string strDebugFilePath, string strEmailServer, string strEmailSender, string strNotifyEmailRecipients, string strErrorEmailRecipients, LogLevelCollection enabledLoggingTypes, string strAppDesc)
			: this(strProductionFilePath, strDebugFilePath, enabledLoggingTypes, strAppDesc)
		{
			m_strEmailServer = strEmailServer;
			m_strEmailSender = strEmailSender;
			m_NotifyRecipientCollection.ParseString(strNotifyEmailRecipients);
			m_ErrorRecipientCollection.ParseString(strErrorEmailRecipients);
			m_bEnableEmail = true;
            m_strAppDescription = strAppDesc; // fja
            m_strConnectionString = connectionString;
		}

		#endregion //Construction

		#region Public Methods
		/// <summary>
		/// Logs the given string in all enabled logs if the debug logging level is enabled
		/// </summary>
		/// <param name="strMessage">Message to log</param>
		public void LogDebug(string strMessage)
		{
            
			LogMessage(strMessage, LogLevel.Debug);
		}

		/// <summary>
		/// Logs the given string in all enabled logs if the information logging level is enabled
		/// </summary>
		/// <param name="strMessage">Message to log</param>
		public void LogInformation(string strMessage)
		{
			LogMessage(strMessage, LogLevel.Information);
		}

		/// <summary>
		/// Logs the given info in all enabled logs if the warning logging level is enabled
		/// Sends an email to ListOfErrorRecipients if SendMailOnWarning and EnableEmail are true
		/// </summary>
		/// <param name="strMessage">Message to log</param>
		public void LogWarning(string strMessage)
		{
			LogMessage(strMessage, LogLevel.Warning);
		}

		/// <summary>
		/// Logs the given info in all enabled logs if the notification logging level is enabled
		/// Sends an email to ListOfNotifyRecipients if EnableEmail is true
		/// </summary>
		/// <param name="strMessage">Message to log (also email body)</param>
		public void LogNotify(string strMessage)
		{
			LogNotify("", strMessage);
		}

		/// <summary>
		/// Logs the given info in all enabled logs if the notification logging level is enabled
		/// Sends an email to ListOfNotifyRecipients if EnableEmail is true
		/// </summary>
		/// <param name="strSubject">Used as the subject of the email sent. If null or empty string, a standard subject is created.</param>
		/// <param name="strMessage">Message to log (also email body)</param>
		public void LogNotify(string strSubject, string strMessage)
		{
			LogMessage(strSubject, strMessage, LogLevel.Notify);
		}

		/// <summary>
		/// Logs the given info in all enabled logs if the error logging level is enabled
		/// Sends an email to ListOfErrorRecipients if SendMailOnError and EnableEmail are true
		/// </summary>
		/// <param name="exception">An exception to log</param>
		public void LogError(System.Exception exception)
		{
			if (exception != null)
			{
				LogMessage("", exception.Message, exception.ToString(), LogLevel.Error);
			}
			else
			{
				LogError("No exception passed to LogError().");
			}
		}

		/// <summary>
		/// Logs the given info in all enabled logs if the error logging level is enabled
		/// Sends an email to ListOfErrorRecipients if SendMailOnError and EnableEmail are true
		/// </summary>
		/// <param name="exception">An exception to log</param>
		/// <param name="strExtraInfo">Additional info added to the log</param>
		public void LogError(System.Exception exception, string strExtraInfo)
		{
			if (String.IsNullOrEmpty(strExtraInfo))
			{
				LogError(exception);
			}
			else
			{
				if (exception != null)
				{
					LogMessage("", exception.Message + "Additional info:" + strExtraInfo,
						exception.ToString() + System.Environment.NewLine + "Additional info:" + System.Environment.NewLine + strExtraInfo,
						LogLevel.Error);
				}
				else
				{
					LogError("No exception passed to LogError()." + System.Environment.NewLine + "Additional info:" + System.Environment.NewLine + strExtraInfo);
				}
			}
		}

		/// <summary>
		/// Logs the given info in all enabled logs if the error logging level is enabled
		/// </summary>
		/// <param name="strMessage">Message to log</param>
		public void LogError(string strMessage)
		{
			LogMessage(strMessage, LogLevel.Error);
		}

		/// <summary>
		/// Logs the given info in all enabled logs if the specified log level is enabled
		/// Will send email to appropriate recipients if the appropriate options are enabled
		/// </summary>
		/// <param name="strMessage">Message to log (also email body)</param>
		/// <param name="messageSeverity">Severity level of message</param>
		public void LogMessage(string strMessage, LogLevel messageSeverity)
		{
			LogMessage("", strMessage, messageSeverity);
		}

		/// <summary>
		/// Logs the given info in all enabled logs if the specified log level is enabled
		/// Will send email to appropriate recipients if the appropriate options are enabled
		/// </summary>
		/// <param name="strSubject">If email is sent, this is used as the subject. If null or empty string, a standard subject is created.</param>
		/// <param name="strMessage">Message to log (also email body)</param>
		/// <param name="messageSeverity">Severity level of message</param>
		public void LogMessage(string strSubject, string strMessage, LogLevel messageSeverity)
		{
			LogMessage(strSubject, strMessage, strMessage, messageSeverity);
		}

		/// <summary>
		/// Logs the given info in all enabled logs if the specified log level is enabled
		/// Will send email to appropriate recipients if the appropriate options are enabled
		/// </summary>
		/// <param name="strSubject">If email is sent, this is used as the subject. If null or empty string, a standard subject is created.</param>
		/// <param name="strSummary">Message to log in Production log (also email body)</param>
		/// <param name="strDetail">Message to log in Debug log</param>
		/// <param name="messageSeverity">Severity level of message</param>
		public void LogMessage(string strSubject, string strSummary, string strDetail, LogLevel messageSeverity)
		{
           
			if (IsValid() && m_LogLevelCollection.Contains(messageSeverity))
			{
				try
				{
					EnvironmentInfo CurrentEnvironment = new EnvironmentInfo();
					CurrentEnvironment.TakeSnapshot(m_bIncludeDriveSpaceInfo, m_bIncludeNetworkDrives, m_bIncludeRamInfo, m_bIncludeNetworkStatus, m_bIncludeCurrentUser);
					SendMail(strSubject, strSummary + System.Environment.NewLine + CurrentEnvironment.Summary, messageSeverity);
					LogToFiles(strSummary, strDetail, CurrentEnvironment.Summary, CurrentEnvironment.Detail, messageSeverity, AppDescription);
					LogToConsole(strDetail + System.Environment.NewLine + CurrentEnvironment.Detail, messageSeverity);
                   // fja logtodb(strSummary, strDetail, currentEnvironment.summary, CurrentEnvironment.detail, messageSeverity);
                    
				}
				catch (SystemException Ex)
				{
					// If we get an error attempting to log, just write it to the console
					Console.WriteLine("Couldn't log message with subject: \"" + strSubject + "\" summary: \"" + strSummary + "\" and detail: \"" + strDetail + "\" with error: " + Ex.ToString());
					if (m_bAllowExceptionThrowing)
					{
						throw;
					}
				}
			}
		}

		/// <summary>
		/// Verifies that the state of the error handler is valid. e.g., if emailing is enabled, ensures EmailServer is not a null string or empty.
		/// Does NOT verify that the specified resources are actually usable (if files can be written, or if SMTP server is valid, etc.)
		/// Note that an error handler with all logging options (debug log, production log, email) disabled is in a valid state
		/// </summary>
		/// <returns>True if the error handler's state is valid, false otherwise</returns>
		public bool IsValid()
		{
			bool bIsValid = true;

			if (m_bEnableDebugLog && System.String.IsNullOrEmpty(m_strDebugFilePath))
			{
				bIsValid = false;
			}
			if (m_bEnableProductionLog && System.String.IsNullOrEmpty(m_strProductionFilePath))
			{
				bIsValid = false;
			}
           // if (m_bEnableLogToDB && System.String.IsNullOrEmpty(m_strConnectionString))    // by fja
           // {
           //     bIsValid=false;
           // }
			if (m_bEnableEmail)
			{
				if (System.String.IsNullOrEmpty(m_strEmailSender))
				{
					bIsValid = false;
				}
				if (System.String.IsNullOrEmpty(m_strEmailServer))
				{
					bIsValid = false;
				}
				if (m_NotifyRecipientCollection.Count == 0 && m_LogLevelCollection.Contains(LogLevel.Notify))
				{
					bIsValid = false;
				}
				if (m_ErrorRecipientCollection.Count == 0 && m_LogLevelCollection.Contains(LogLevel.Error))
				{
					bIsValid = false;
				}
			}

			return bIsValid;
		}
        
		/// <summary>
		/// Converts this object into a string representing the object as xml
		/// </summary>
		/// <returns>xml representation of this object as a string</returns>
		public string SerializeToXml()
		{
			System.Xml.Serialization.XmlSerializer Serializer = new System.Xml.Serialization.XmlSerializer(this.GetType());
			using (System.IO.StringWriter StringStream = new System.IO.StringWriter(System.Globalization.CultureInfo.CurrentCulture))
			{
				Serializer.Serialize(StringStream, this);
				return StringStream.ToString();
			}
		}
		/// <summary>
		/// Creates an error handler object based on the supplied xml representation
		/// </summary>
		/// <param name="strXmlObjectRepresentation">xml representation an error handler object</param>
		/// <returns>Error Handler created from xml representation; null if there are any issues</returns>
		public static ErrorHandler DeserializeFromXml(string strXmlObjectRepresentation)
		{
			System.Xml.Serialization.XmlSerializer Serializer = new System.Xml.Serialization.XmlSerializer(typeof(ErrorHandler));
			using (System.IO.StringReader StringStream = new System.IO.StringReader(strXmlObjectRepresentation))
			{
				ErrorHandler DeserializedErrorHandler;
				try
				{
					DeserializedErrorHandler = Serializer.Deserialize(StringStream) as ErrorHandler;
				}
				catch (InvalidOperationException invalidOperationEx)
				{
					// Not a valid error handler xml representation, write error info to console
					Console.WriteLine("Error while deserializing Error Handler xml representation, details: " + invalidOperationEx.ToString());
					DeserializedErrorHandler = null;
				}
				return DeserializedErrorHandler;
			}
		}
		#endregion //Public Methods

        

		#region Private Methods
		private void SetMinimumLogLevel(LogLevel minimumLogLevel)
		{
			m_LogLevelCollection.Clear();
			for (LogLevel currentLogLevel = LogLevel.Debug; currentLogLevel <= LogLevel.Error; currentLogLevel++)
			{
				if (currentLogLevel >= minimumLogLevel)
				{
					m_LogLevelCollection.Add(currentLogLevel);
                   
				}
			}
		}

		private void SendMail(string strSubject, string strMessage, LogLevel messageSeverity)
		{
            
			bool bDoSendMail = false;
			EmailRecipientCollection recipientListToUse = null;
			MailPriority PriorityToUse = MailPriority.Normal;
			switch (messageSeverity)
			{
				case LogLevel.Notify:
					bDoSendMail = m_bEnableEmail;
					PriorityToUse = m_NotifyEmailPriority;
					recipientListToUse = m_NotifyRecipientCollection;
					break;

				case LogLevel.Error:
					bDoSendMail = m_bEnableEmail && m_bSendMailOnError;
					PriorityToUse = m_ErrorEmailPriority;
					recipientListToUse = m_ErrorRecipientCollection;
					break;

				case LogLevel.Warning:
					bDoSendMail = m_bEnableEmail && m_bSendMailOnWarning;
					PriorityToUse = m_ErrorEmailPriority;
					recipientListToUse = m_ErrorRecipientCollection;
					break;
			}
			if (bDoSendMail)
			{
				try
				{
					MailMessage Mail = new MailMessage();
					Mail.From = new MailAddress(m_strEmailSender);
					foreach (string strEmailAddress in recipientListToUse)
					{
						Mail.To.Add(new MailAddress(strEmailAddress));
					}
					Mail.Subject = StandardizeEmailSubject(strSubject, messageSeverity);
					Mail.Body = strMessage;
					Mail.Priority = PriorityToUse;
					SmtpClient mailClient = new SmtpClient(m_strEmailServer);
                    // fja test SmtpClient mailClient1 = new SmtpClient(m_strC
					mailClient.Send(Mail);
				}
				catch (System.Net.Mail.SmtpException SmtpEx)
				{
					// If we get an error attempting to send mail, just write it to the console
					Console.WriteLine("Couldn't send email using server: " + m_strEmailServer + " message: \"" + strMessage + "\" with smtp error: " + SmtpEx.ToString());
				}
			}
		}

		// appdescr parm added by FJA
        private void LogToFiles(string strSummary, string strDetail, string strEnvironmentSummary, string strEnvironmentDetail, LogLevel messageSeverity, string strAppDesc)
		{
          
			if (m_bEnableProductionLog)
			{
				if (String.IsNullOrEmpty(strSummary))
				{
					LogToFile(m_strProductionFilePath, "No message specified. " + strEnvironmentSummary, true, messageSeverity);

                    // fja (add logtodb)for prod table
                    if (m_bEnableLogToDB)
                    {
                        LogToDB_Prod("No message specified. ", strEnvironmentSummary, messageSeverity, strAppDesc);
                        
                    }
                }
				else
				{
					LogToFile(m_strProductionFilePath, strSummary + " " + strEnvironmentSummary, true, messageSeverity);
		
                    // fja (add logtodb) for prod table
                    if (m_bEnableLogToDB)
                    {
                        LogToDB_Prod(strSummary, strEnvironmentSummary, messageSeverity, strAppDesc);
                    }
               }
			}
			if (m_bEnableDebugLog)
			{
				if (String.IsNullOrEmpty(strDetail))
				{
					LogToFile(m_strDebugFilePath, "No message specified." + System.Environment.NewLine + strEnvironmentDetail + System.Environment.NewLine + System.Environment.NewLine, false, messageSeverity);
                    
                    // fja (add logtodb) for debug table
                    if (m_bEnableLogToDB)
                    {
                        LogToDB_Debug("No message specified.", strEnvironmentDetail, messageSeverity, strAppDesc);
                    }
                }

				else
				{
					LogToFile(m_strDebugFilePath, strDetail + System.Environment.NewLine + strEnvironmentDetail + System.Environment.NewLine + System.Environment.NewLine, false, messageSeverity);
                  // fja (add logtodb) for debug table
                    if (m_bEnableLogToDB)
                    {
                        LogToDB_Debug(strDetail, strEnvironmentDetail, messageSeverity, strAppDesc);
                    }
                }

			}
		}

		private static void LogToFile(string strFileName, string strMessage, bool bRemoveNewLines, LogLevel messageSeverity)
		{
		    
            string strFinalMessage = "";
           
			try
			{
				strFinalMessage = FormatMessage(strMessage, messageSeverity);
				if (bRemoveNewLines)
				{
					// Replace both line-terminating characters separately, to ensure file looks the same regardless of reader
					strFinalMessage = strFinalMessage.Replace("\r", " ").Replace("\n", " ");
				}

				// Ensure the message ends with a newline (especially needed if we just removed them all)
				if (strFinalMessage.EndsWith(System.Environment.NewLine))
				{
					System.IO.File.AppendAllText(strFileName, strFinalMessage);
				}
				else
				{
					System.IO.File.AppendAllText(strFileName, strFinalMessage + System.Environment.NewLine);
				}

			}
			catch (System.IO.IOException IOEx)
			{
				// If we get an error attempting to write, just write it to the console
				Console.WriteLine("Couldn't write to file: " + strFileName + " message: \"" + strFinalMessage + "\" with IO error: " + IOEx.ToString());
			}
			catch (System.Security.SecurityException SecurityEx)
			{
				// If we get an error attempting to write, just write it to the console
				Console.WriteLine("Couldn't write to file: " + strFileName + " message: \"" + strFinalMessage + "\" with security error: " + SecurityEx.ToString());
			}
			catch (System.UnauthorizedAccessException UnauthorizedAccessEx)
			{
				// If we get an error attempting to write, just write it to the console
				Console.WriteLine("Couldn't write to file: " + strFileName + " message: \"" + strFinalMessage + "\" with unauthorized access error: " + UnauthorizedAccessEx.ToString());
			}
		}
 
        // by fja, LogToDB method
        private static void LogToDB_Prod( string strSummary, String strEnvironmentSummary , LogLevel messageSeverity, string strAppDesc)
        
        {
           
            //string strConnectionString = "Data Source=CASQL05DEV;Initial Catalog=HTERROR;User ID=sa;Password=hottopic;";
            string strFinalMessage = "";
            string ProdTable = ("Prod" + strAppDesc + "Log");           
         
            try
            {
                string strMessage = strSummary; // by fja
               strFinalMessage = FormatMessage(strMessage, messageSeverity);
               
               // DateTime strPostTime = System.DateTime.Now.ToString(DATETIME_FORMAT_PATTERN, System.Globalization.CultureInfo.CurrentCulture);
               DateTime strPostDate = DateTime.Now;
              // DateTime logdate = DateTime.Today;
               // String strDB_Message = strMessage;
                LogLevel strLog_level_severity = messageSeverity;
               // String strAppDescription = strAppDesc;
                string strCurrentUser = System.Environment.UserName;
              
                int strAppID = -1;  
             
                
                // Make sure AppDesc is in Applications table 
                
                if (!Htbusiness.B_isAppPresent(strAppDesc))
                {
                    //insert app and get id
                    strAppID = HtDataAccess.InsertAndGetAppID(strAppDesc, strPostDate, strCurrentUser);  
                    // create prod log table
                    HtDataAccess.CreateProdLogTable(ProdTable, strAppID);              
                }
                else
                {

                    // get id and isProdlogPresent
                    strAppID = HtDataAccess.GetAppID(strAppDesc);
                    // create prod log table
                    if (!Htbusiness.B_DoesTableExist(ProdTable))                     
                    {
                        HtDataAccess.CreateProdLogTable(ProdTable, strAppID);
                    }
                }

               // ProductionLog_Insert(ProdTable, strPostDate, messageSeverity, strSummary, strCurrentUser, strEnvironmentSummary, strAppID);
              HtDataAccess.ProdLogInsert(ProdTable, strPostDate, messageSeverity, strSummary, strCurrentUser, strEnvironmentSummary, strAppID);

            }
            catch (System.IO.IOException IOEx)
            {
                // If we get an error attempting to write, just write it to the console
                Console.WriteLine("Couldn't write to DB: " + " message: \"" + strFinalMessage + "\" with IO error: " + IOEx.ToString());
            }
            catch (System.Security.SecurityException SecurityEx)
            {
                // If we get an error attempting to write, just write it to the console
                Console.WriteLine("Couldn't write to DB: " + " message: \"" + strFinalMessage + "\" with security error: " + SecurityEx.ToString());
            }
            catch (System.UnauthorizedAccessException UnauthorizedAccessEx)
            {
                // If we get an error attempting to write, just write it to the console
                Console.WriteLine("Couldn't write to DB: "  + " message: \"" + strFinalMessage + "\" with unauthorized access error: " + UnauthorizedAccessEx.ToString());
            }
        }
        private static void LogToDB_Debug(string strDetail, String strEnvironmentDetail, LogLevel messageSeverity, string strAppDesc)
        {
            
            
        //    string strConnectionString = "Data Source=CASQL05DEV;Initial Catalog=HTERROR;User ID=sa;Password=hottopic;";
            string strFinalMessage = "";
            string DebugTable = ("Debug" + strAppDesc + "Log");

            try
            {
                string strMessage = strDetail; // by fja
                strFinalMessage = FormatMessage(strMessage, messageSeverity);

                // DateTime strPostTime = System.DateTime.Now.ToString(DATETIME_FORMAT_PATTERN, System.Globalization.CultureInfo.CurrentCulture);
                DateTime strPostDate = DateTime.Now;
                // String strDB_Message = strMessage;
                // LogLevel strLog_level_severity = messageSeverity;
                // String strAppDescription = strAppDesc;
                string strCurrentUser = System.Environment.UserName;
               
                int strAppID = -1;

                // Make sure AppDesc is in Applications table 

                if (!Htbusiness.B_isAppPresent(strAppDesc))
                {
                    //insert app and get id
                    strAppID = HtDataAccess.InsertAndGetAppID(strAppDesc, strPostDate, strCurrentUser);
                   // create debug log table for the specific app

                    HtDataAccess.CreateDebugLogTable(DebugTable, strAppID);
                       
                }
                else
                {
                    // get id 
                        strAppID = HtDataAccess.GetAppID(strAppDesc);
                    
                    if (!Htbusiness.B_DoesTableExist(DebugTable))
                        {
                            HtDataAccess.CreateDebugLogTable(DebugTable, strAppID);
                        }
                }

                // ready for "logging" add to productionlog or debuglog                      
                //DebugLog_Insert(DebugTable, strPostDate, messageSeverity, strDetail, strCurrentUser, strEnvironmentDetail, strAppID);
                HtDataAccess.DebugLogInsert(DebugTable, strPostDate, messageSeverity, strDetail, strCurrentUser, strEnvironmentDetail, strAppID);
            }
            catch (System.IO.IOException IOEx)
            {
                // If we get an error attempting to write, just write it to the console
                Console.WriteLine("Couldn't write to DB: " + " message: \"" + strFinalMessage + "\" with IO error: " + IOEx.ToString());
            }
            catch (System.Security.SecurityException SecurityEx)
            {
                // If we get an error attempting to write, just write it to the console
                Console.WriteLine("Couldn't write to DB: " + " message: \"" + strFinalMessage + "\" with security error: " + SecurityEx.ToString());
            }
            catch (System.UnauthorizedAccessException UnauthorizedAccessEx)
            {
                // If we get an error attempting to write, just write it to the console
                Console.WriteLine("Couldn't write to DB: " + " message: \"" + strFinalMessage + "\" with unauthorized access error: " + UnauthorizedAccessEx.ToString());
            }
        }

		private void LogToConsole(string strMessage, LogLevel messageSeverity)
		{
			if (m_bAlsoLogToConsole)
			{
				Console.WriteLine(FormatMessage(strMessage, messageSeverity));
			}
		}

		private static string StandardizeEmailSubject(string strBaseSubject, LogLevel mailType)
		{
			// Need to make sure we don't try to call methods on a null string
			if (strBaseSubject == null)
			{
				strBaseSubject = "";
			}

			System.Text.StringBuilder standardSubjectBuilder = new System.Text.StringBuilder();
			if (!(strBaseSubject.Trim().StartsWith(mailType.ToString(), StringComparison.CurrentCultureIgnoreCase)))
			{
				standardSubjectBuilder.Append(mailType);
				standardSubjectBuilder.Append(" - ");
			}
			
			if (String.IsNullOrEmpty(strBaseSubject))
			{
				standardSubjectBuilder.Append("from application ");
				standardSubjectBuilder.Append(System.Diagnostics.Process.GetCurrentProcess().ProcessName);
			}
			else
			{
				standardSubjectBuilder.Append(strBaseSubject);
			}

			if (!(strBaseSubject.Trim().EndsWith(System.Environment.MachineName, StringComparison.CurrentCultureIgnoreCase)))
			{
				standardSubjectBuilder.Append(" - on ");
				standardSubjectBuilder.Append(System.Environment.MachineName);
			}
			return standardSubjectBuilder.ToString();
		}

		private static string FormatMessage(string strMessage, LogLevel messageSeverity)
		{
			return System.DateTime.Now.ToString(DATETIME_FORMAT_PATTERN, System.Globalization.CultureInfo.CurrentCulture) + 
                " " + messageSeverity + ": " + strMessage;
		}


        #endregion //Private Methods


    }
}
