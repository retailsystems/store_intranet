﻿using System;
namespace HTError
{
	/// <summary>
	/// Provides a flexible, configurable error logging and notification system
	/// </summary>
	public interface IHTError
	{
		/// <summary>
		/// Full filename and path to the debug (detailed) log file
		/// </summary>
		string DebugFilePath { get; set; }
		/// <summary>
		/// Full filename and path to the production (summary) log file
		/// </summary>
		string ProductionFilePath { get; set; }
		/// <summary>
		/// Email address to use in From: field of all sent email
		/// </summary>
		string EmailSender { get; set; }
		/// <summary>
		/// Name or IP address of the SMTP server to use for sending email
		/// </summary>
		string EmailServer { get; set; }
        // by fja
        /// </summary>
        /// Holds the connection string to DB SQL server
        /// </summary> 
        string connectionString { get; set;}
        // by fja
        /// </summary>
        /// Holds the Application Name for logging to DB
        /// </summary> 
        string AppDescription { get; set;}
		/// <summary>
		/// Collection of message severities which will be logged/emailed. Attempting to log a message of a severity not
		/// in this collection will do nothing.
		/// </summary>
		LogLevelCollection ListOfLogLevels { get; }
		/// <summary>
		/// Collection of email addresses of recipients of warning/error emails
		/// </summary>
		EmailRecipientCollection ListOfErrorRecipients { get; }
		/// <summary>
		/// Collection of email addresses of recipients of notification emails
		/// </summary>
		EmailRecipientCollection ListOfNotifyRecipients { get; }
		/// <summary>
		/// Priority setting used on error/warning mails (defaults to High)
		/// </summary>
		System.Net.Mail.MailPriority ErrorEmailPriority { get; set; }
		/// <summary>
		/// Priority setting used on notification mails (defaults to Normal)
		/// </summary>
		System.Net.Mail.MailPriority NotifyEmailPriority { get; set; }
		/// <summary>
		/// If true, details of messages will be logged to the debug log file.
		/// </summary>
		bool EnableDebugLog { get; set; }
		/// <summary>
		/// If true, summaries of messages will be logged to the production log file.
		/// </summary>
		bool EnableProductionLog { get; set; }
		/// <summary>
		/// If true, email is allowed to be sent, and always will be on notification messages.
		/// </summary>
		bool EnableEmail { get; set; }
		/// <summary>
		/// If true, error message will be emailed to the ListOfErrorRecipients (defaults to true)
		/// </summary>
		bool SendMailOnError { get; set; }
		/// <summary>
		/// If true, warning messages will also be emailed to the ListOfErrorRecipients (defaults to true)
		/// </summary>
		bool SendMailOnWarning { get; set; }
		/// <summary>
		/// If false, all unexpected exceptions while logging will be caught and logged to the console, then disposed of.
		/// If true, unexpected exceptions while logging will be re-thrown. (defaults to false)
		/// </summary>
		bool AllowExceptionThrowing { get; set; }
		/// <summary>
		/// If true, detailed error messages will also be written to the console. (defaults to false)
		/// </summary>
		bool AlsoLogToConsole { get; set; }
        /// by fja
        /// </summary>
        /// If true, will enable logging to DB 
        /// </summary>
        bool EnableLogToDB { get; set;}

		/// <summary>
		/// If true, information about available space on all local drives will be included in messages (defaults to true)
		/// </summary>
		bool IncludeDriveSpaceInfo { get; set; }
		/// <summary>
		/// If true, network drive info will be includd with local drive info (defaults to false)
		/// </summary>
		bool IncludeNetworkDrives { get; set; }
		/// <summary>
		/// If true, information about network availability will be included in messages (defaults to true)
		/// </summary>
		bool IncludeNetworkStatus { get; set; }
		/// <summary>
		/// If true, information about available physical and virtual RAM will be included in messages (defaults to true)
		/// </summary>
		bool IncludeRamInfo { get; set; }
		/// <summary>
		/// If true, the name of the executing current user account will be included in messages (defaults to true)
		/// </summary>
		bool IncludeCurrentUser { get; set; }

		/// <summary>
		/// Verifies that the state of the error handler is valid. e.g., if emailing is enabled, ensures EmailServer is not a null string or empty.
		/// Does NOT verify that the specified resources are actually usable (if files can be written, or if SMTP server is valid, etc.)
		/// Note that an error handler with all logging options (debug log, production log, email) disabled is in a valid state
		/// </summary>
		/// <returns>True if the error handler's state is valid, false otherwise</returns>
		bool IsValid();
		/// <summary>
		/// Logs the given string in all enabled logs if the debug logging level is enabled
		/// </summary>
		/// <param name="strMessage">Message to log</param>
		void LogDebug(string strMessage);
		/// <summary>
		/// Logs the given info in all enabled logs if the error logging level is enabled
		/// </summary>
		/// <param name="strMessage">Message to log</param>
		void LogError(string strMessage);
		/// <summary>
		/// Logs the given info in all enabled logs if the error logging level is enabled
		/// Sends an email to ListOfErrorRecipients if SendMailOnError and EnableEmail are true
		/// </summary>
		/// <param name="exception">An exception to log</param>
		void LogError(Exception exception);
		/// <summary>
		/// Logs the given info in all enabled logs if the error logging level is enabled
		/// Sends an email to ListOfErrorRecipients if SendMailOnError and EnableEmail are true
		/// </summary>
		/// <param name="exception">An exception to log</param>
		/// <param name="strExtraInfo">Additional info added to the log</param>
		void LogError(Exception exception, string strExtraInfo);
		/// <summary>
		/// Logs the given string in all enabled logs if the information logging level is enabled
		/// </summary>
		/// <param name="strMessage">Message to log</param>
		void LogInformation(string strMessage);
		/// <summary>
		/// Logs the given info in all enabled logs if the notification logging level is enabled
		/// Sends an email to ListOfNotifyRecipients if EnableEmail is true
		/// </summary>
		/// <param name="strMessage">Message to log (also email body)</param>
		void LogNotify(string strMessage);
		/// <summary>
		/// Logs the given info in all enabled logs if the notification logging level is enabled
		/// Sends an email to ListOfNotifyRecipients if EnableEmail is true
		/// </summary>
		/// <param name="strSubject">Used as the subject of the email sent. If null or empty string, a standard subject is created.</param>
		/// <param name="strMessage">Message to log (also email body)</param>
		void LogNotify(string strSubject, string strMessage);
		/// <summary>
		/// Logs the given info in all enabled logs if the warning logging level is enabled
		/// Sends an email to ListOfErrorRecipients if SendMailOnWarning and EnableEmail are true
		/// </summary>
		/// <param name="strMessage">Message to log</param>
		void LogWarning(string strMessage);
		/// <summary>
		/// Logs the given info in all enabled logs if the specified log level is enabled
		/// Will send email to appropriate recipients if the appropriate options are enabled
		/// </summary>
		/// <param name="strMessage">Message to log (also email body)</param>
		/// <param name="messageSeverity">Severity level of message</param>
		void LogMessage(string strMessage, LogLevel messageSeverity);
		/// <summary>
		/// Logs the given info in all enabled logs if the specified log level is enabled
		/// Will send email to appropriate recipients if the appropriate options are enabled
		/// </summary>
		/// <param name="strSubject">If email is sent, this is used as the subject. If null or empty string, a standard subject is created.</param>
		/// <param name="strMessage">Message to log (also email body)</param>
		/// <param name="messageSeverity">Severity level of message</param>
		void LogMessage(string strSubject, string strMessage, LogLevel messageSeverity);
		/// <summary>
		/// Logs the given info in all enabled logs if the specified log level is enabled
		/// Will send email to appropriate recipients if the appropriate options are enabled
		/// </summary>
		/// <param name="strSubject">If email is sent, this is used as the subject. If null or empty string, a standard subject is created.</param>
		/// <param name="strSummary">Message to log in Production log (also email body)</param>
		/// <param name="strDetail">Message to log in Debug log</param>
		/// <param name="messageSeverity">Severity level of message</param>
		void LogMessage(string strSubject, string strSummary, string strDetail, LogLevel messageSeverity);
		/// <summary>
		/// Converts this object into a string representing the object as xml
		/// </summary>
		/// <returns>xml representation of this object as a string</returns>
		string SerializeToXml();
	}
}
