using System;
using System.Collections.Generic;
using System.Text;
using SSIS = Microsoft.SqlServer.Dts.Runtime;

namespace HTErrorSampleApp
{
	class HTErrorSampleApp
	{
		static void Main(string[] args)
		{
			// NOTE: for this project to work properly, you MUST have the current version of HTError.dll in your .NET 2.0
			// framework directory. (Usually, C:\WINDOWS\Microsoft.NET\Framework\v2.0.50727 or C:\WINNT\Microsoft.NET\Framework\v2.0.50727)
			// You also must register it in your GAC (Global Assembly Cache). The easiest way to do so is just to drag the HTError.dll file
			// to your GAC folder, usually either C:\Windows\Assembly or C:\WINNT\Assembly.

			HTError.IHTError ErrorHandler;
			// In real code, get this info from a config file
			ErrorHandler = new HTError.ErrorHandler("C:\\Production.log", "C:\\Debug.log", "filter.hottopic.com", "HTErrorTest@hottopic.com", "fdeasis@hottopic.com", HTError.LogLevel.Debug, "test8");
			ErrorHandler.AlsoLogToConsole = true;
			ErrorHandler.ErrorEmailPriority = System.Net.Mail.MailPriority.High;
			ErrorHandler.SendMailOnError = true;
			ErrorHandler.SendMailOnWarning = true;
            ErrorHandler.EnableLogToDB = true;
            ErrorHandler.EnableProductionLog = true;
            ErrorHandler.EnableDebugLog = true;          
       //     ErrorHandler.ConnectionString = "Data Source=CASQL05DEV;Initial Catalog=HTERROR;User ID=sa;Password=hottopic;";

			// Object is now fully initialized/configured and ready for use!

			// Optionally, you can validate that the configuration is valid before continuing; this will ensure the configuration is
			// internally consistent, and that it will be able to log to the specified files. (has permission, folders exist, etc.)
			if (!ErrorHandler.IsValid())
			{
				// Do something here if logging is a strict requirement, or if you just want to let the user or scheduler know that
				// logging will not function properly
			}

			RunExtractFoo(ErrorHandler);

			ErrorHandler.LogInformation("Sample job complete!");
		}

		static void RunExtractFoo(HTError.IHTError ErrorHandler)
		{
			ErrorHandler.LogInformation("Starting Extract Foo");

			#region Config values, should come from file or user
			int iMaxAttempts = 3;
			int iSleepSeconds = 5;
			// Use these two config values to run locally
			//string strSSISPackage = "..\\..\\..\\HTErrorSample\\HTErrorSamplePackage.dtsx";
			//string strConfigFile = "..\\..\\..\\HTErrorSample\\HTErrorSample.dtsConfig";
			// Use these two to run on a server with SSIS installed, putting the app and the dts config and xml file in the same folder
			string strSSISPackage = "HTErrorSamplePackage.dtsx";
			string strConfigFile = "HTErrorSample.dtsConfig";
			#endregion

			bool bExtractSucceeded = false;

			try
			{
				SSIS.Application SSISApp = new SSIS.Application();
				SSIS.Package SSISPackage = SSISApp.LoadPackage(strSSISPackage, null);
				SSISPackage.ImportConfigurationFile(strConfigFile);

				// This passes the (already configured) Error handler object to the SSIS package, letting the package use it to log errors as well
				SSISPackage.Variables["ErrorHandler"].Value = ErrorHandler;
				// Any other configuration values can be passed to the package similarly, assigning them to the correct package variable
				SSISPackage.Variables["SampleErrorMessage"].Value = "I'm an error message from C#";
				// Example of how to access variables whose scope is confined to a specific task (or task container)
				((SSIS.TaskHost)SSISPackage.Executables["Task that causes an error"]).Variables["SampleErrorCode"].Value = 3;

				SSIS.DTSExecResult execResult = SSISPackage.Execute();
				if (execResult == SSIS.DTSExecResult.Success)
				{
					bExtractSucceeded = true;
				}
				else
				{
					ErrorHandler.LogError("Extract Foo failed with DTSExecResult: " + execResult);
					bExtractSucceeded = false;
				}
			}
			catch (System.Exception ex)
			{
				bExtractSucceeded = false;
				ErrorHandler.LogError(ex);
			}

			if (bExtractSucceeded)
			{
				ErrorHandler.LogNotify("Extract Foo Complete!");
			}
		}
	}
}
