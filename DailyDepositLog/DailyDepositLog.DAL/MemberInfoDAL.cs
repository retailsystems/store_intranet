﻿using DailyDeposit.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyDepositLog.DAL
{
    public class MemberInfoDAL : Connection
    {
        string conString;
        public MemberInfoDAL(string connectionString)
        {
            conString = connectionString;
        }
        public List<String> GetMemberNames(int storeNumber)
        {
            try
            {
                List<string> memberNames = new List<string>();
                using (SqlConnection con = GetConnection(conString))
                {
                    using (SqlCommand cmd = new SqlCommand("Ssp_getemployeefullnamebystorenum", con))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@StoreNum", storeNumber);
                        con.Open();
                        SqlDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            memberNames.Add(dr["FullName"].ToString());
                        }
                    }

                }
                return memberNames;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        
    }
}
