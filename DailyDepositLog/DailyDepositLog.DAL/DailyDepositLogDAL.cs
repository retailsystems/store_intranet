﻿using DailyDeposit.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DDL.Helper;
using System.Web;

namespace DailyDepositLog.DAL
{
    public class DailyDepositLogDAL : Connection
    {
        string conString;
        HttpContext context;
        public DailyDepositLogDAL(string connectionString)
        {
            conString = connectionString;
        }
        public List<DailyDepositLogModel> GetDailyDepositLogs(int storeNumber)
        {
            try
            {
                List<DailyDepositLogModel> dailyDepositLogModels = new List<DailyDepositLogModel>();
                using (SqlConnection con = GetConnection(conString))
                {
                    using (SqlCommand cmd = new SqlCommand("GetDailyDepositLog", con))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@storeNumber", storeNumber);
                        con.Open();
                        SqlDataReader dr = cmd.ExecuteReader();
                        while (dr.Read())
                        {
                            DailyDepositLogModel DDLM = new DailyDepositLogModel();
                            DDLM.ID = Convert.ToInt32(dr["ID"].ToString());
                            DDLM.ManagerName = dr["ManagerName"].ToString();
                            DDLM.WitnessName = dr["WitnessName"].ToString();
                            DDLM.DepositBagNo = dr["DepositBagNo"].ToString();
                            DDLM.DepositAmount = Convert.ToDecimal(dr["DepositAmount"].ToString());
                            DDLM.IsDepositSealed = Convert.ToBoolean(dr["IsDepositSealed"].ToString());
                            DDLM.SaleDate = Convert.ToDateTime(dr["SaleDate"].ToString());
                            DDLM.StoreNumber = Convert.ToInt32(dr["StoreNumber"].ToString());
                            DDLM.EmployeeId = dr["EmployeeId"].ToString();
                            DDLM.DepositOverOrShort = Convert.ToDecimal(dr["DepositOverOrShort"].ToString());
                            DDLM.SubmittedDateTime = Convert.ToDateTime(dr["SubmittedDateTime"].ToString());
                            if (dr["DailySale"].ToString().Length >0)
                            {
                                DDLM.DailySales = Convert.ToDecimal(dr["DailySale"].ToString());
                            }
                            
                            dailyDepositLogModels.Add(DDLM);
                        }
                    }

                }
                return dailyDepositLogModels;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public int AddDailyDepositLogs(DailyDepositLogModel model)
        {
            try
            {
                using (SqlConnection con = GetConnection(conString))
                {
                    using (SqlCommand cmd = new SqlCommand("AddDailyDepositLog", con))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ManagerName", model.ManagerName);
                        cmd.Parameters.AddWithValue("@WitnessName", model.WitnessName);
                        cmd.Parameters.AddWithValue("@DepositBagNo",model.DepositBagNo);
                        cmd.Parameters.AddWithValue("@DepositAmount",model.DepositAmount);
                        cmd.Parameters.AddWithValue("@IsDepositSealed",model.IsDepositSealed);
                        cmd.Parameters.AddWithValue("@SaleDate", model.SaleDate.ToString("dd MMM yyyy"));
                        cmd.Parameters.AddWithValue("@StoreNumber",model.StoreNumber);
                        cmd.Parameters.AddWithValue("@EmployeeId", model.EmployeeId);
                        cmd.Parameters.AddWithValue("@DepositOverOrShort", model.DepositOverOrShort);
                        cmd.Parameters.AddWithValue("@SubmittedDateTime", model.SubmittedDateTime= DateTime.Now);
                        cmd.Parameters.AddWithValue("@DailySale", model.DailySales);
                        con.Open();
                        int result = cmd.ExecuteNonQuery();
                    }
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int ValidateEmployee(LoginViewModel model)
        {
             int returnResult;
            try
            {
                //string cookie = context.Request.Cookies["StoreNum"].Value;
                //string Storenumber = cookie!= null
                using (SqlConnection con = GetConnection(conString))
                {
                    using (SqlCommand cmd = new SqlCommand("ssp_LoginByEmpIDAndStoreNum", con))
                    {
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@EmployeeID", model.EmployeeID);
                        cmd.Parameters.AddWithValue("@StoreNumber", model.StoreNumber);
                        cmd.Parameters.Add("@retValue", SqlDbType.Int);
                        cmd.Parameters["@retValue"].Direction = ParameterDirection.Output;
                                             
                        con.Open();
                        SqlDataReader dr = cmd.ExecuteReader();
                        if (!dr.HasRows)
                        {
                            return 1;
                        }
                        else
                        {
                            dr.Read();
                            //string jobCode = dr.GetFieldValue<string>(6).Trim();
                            //string StoreNum = dr.GetFieldValue<Int16>(7).ToString();
                            string jobCode = dr["jobCode"].ToString();
                            string StoreNum = dr["StoreNum"].ToString();
                            //dr["DepositBagNo"].ToString();

                            if ((!string.IsNullOrEmpty(StoreNum.ToString())))
                            {
                                if (model.StoreNumber == Convert.ToInt32(StoreNum) && ApplicationVariable.JobCodeAllowedToLogin.Any(c => string.Equals(c, jobCode, StringComparison.InvariantCultureIgnoreCase)))
                                    return 0;
                                else if (ApplicationVariable.JobCodeAllowedToLogin.Any(c => string.Equals(c, jobCode, StringComparison.InvariantCultureIgnoreCase)))
                                {
                                    return 0;
                                }
                            }
                            else if(string.IsNullOrEmpty(StoreNum.ToString()) && ApplicationVariable.JobCodeAllowedToLogin.Any(c => string.Equals(c, jobCode, StringComparison.InvariantCultureIgnoreCase)))
                            {
                                return 0;
                            }
                            
                        }
                            return 0;
                        //int result = cmd.ExecuteNonQuery();
                        //int test = (int)cmd.Parameters["@retValue"].Value;
                        // returnResult = test;
                    }
                    return returnResult;
                }
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
