﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DailyDepositLog.DAL
{
    public class Connection
    {
        public SqlConnection GetConnection(string consting)
        {
            var connString = ConfigurationManager.ConnectionStrings["DDLConnectionString"].ConnectionString;
            return new SqlConnection(consting);
        }
    }
}
