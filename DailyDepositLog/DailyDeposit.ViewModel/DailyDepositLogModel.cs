﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace DailyDeposit.ViewModel
{
    public class DailyDepositLogModel
    {

        public int ID { get; set; }
        public decimal DepositAmount { get; set; }
        [Required(ErrorMessage ="Manager name is required")]
        public string ManagerName { get; set; }
        [Required(ErrorMessage = "Deposit Bag Number is required")]
        public string DepositBagNo { get; set; }
        public string WitnessName { get; set; }
        public bool IsDepositSealed { get; set; }
        public DateTime SaleDate { get; set; }
        public int StoreNumber { get; set; }
        public string EmployeeId { get; set; }
        public decimal DepositOverOrShort { get; set; }
        public DateTime SubmittedDateTime { get; set; }
        public decimal DailySales { get; set; }
    }
    public class DailyDepositLogViewModel
    {
        public List<DailyDepositLogModel> DailyDepositLogs { get; set; }
        public List<string> ManagerNames { get; set; }
        public int IncludedDays { get; set; }
    }
    public class DailyDepositLogCreateViewModel
    {
        public DailyDepositLogModel DailyDepositLog { get; set; }
        public List<string> ManagerNames { get; set; }
        public bool IsUpdate { get; set; }
        public int IncludedDays { get; set; }
        //public List<string> saledate { get; set; }
    }
    //public class DailyDepositLogSaleDateModel
    //{

    //    public List<string> saledate { get; set; }
    //}
    public class ResponseResult
    {
        //status 1=OK, 2=Not Found,3=Error
        public int Status { get; set; }
        public string ErrorMessage { get; set; }
        public object Data { get; set; }
    }
}