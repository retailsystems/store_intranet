﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyDeposit.ViewModel
{
   public class LoginViewModel
    {
        public string EmployeeID { get; set; }
        public int StoreNumber { get; set; }
    }
}
