﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DailyDeposit.ViewModel
{
    public class SessionModel
    {
        public string EmployeeId { get; set; }
        public int StoreNumber { get; set; }
    }
}
