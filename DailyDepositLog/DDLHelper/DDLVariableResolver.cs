﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace DDL.Helper
{
    public class DDLVariableResolver : ConfigurationSection
    {
        [ConfigurationProperty("brandName")]
        public string BrandName
        {
            get { return (string)this["brandName"]; }

        }
        [ConfigurationProperty("loginPage")]
        public LoginPage LoginPage
        {
            get { return (LoginPage)this["loginPage"]; }

        }
    }
    public class LoginPage : ConfigurationElement
    {
        [ConfigurationProperty("jobCodesAllowedToLogin")]
        public CustomeElement<string> JobCodesAllowedToLogin
        {
            get { return (CustomeElement<string>)this["jobCodesAllowedToLogin"]; }
            set { this["jobCodesAllowedToLogin"] = value; }
        }
    }
    public class CustomeElement<T> : ConfigurationElement
    {
        [ConfigurationProperty("value")]
        public T Value
        {
            get { return (T)this["value"]; }
            set { this["value"] = value; }
        }
    }
}