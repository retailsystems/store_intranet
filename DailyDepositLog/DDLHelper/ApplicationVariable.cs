﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace DDL.Helper
{
   public static class ApplicationVariable
    {
        public static DDLVariableResolver Config { get; set; }
        public static List<string> JobCodeAllowedToLogin { get; set; }
        public static void LoadAll()
        {
            
             Config = ConfigurationManager.GetSection("DDL") as DDLVariableResolver;
            if (Config != null)
            {
                JobCodeAllowedToLogin = Config.LoginPage.JobCodesAllowedToLogin.Value
                        .Split(',', ';')
                        .Select(c => c.Trim())
                        .ToList();
            }
        }
       


    }
}
