﻿using DailyDeposit.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DailyDepositLog.UI
{
    public class Utility
    {
        HttpContextBase context;
        public Utility(HttpContextBase _context)
        {
            this.context = _context;
        }
        public bool IsLoggedIn()
        {
            try
            {
                if (this.context.Session["EmployeeInfo"] == null)
                {
                    return false;
                }
                SessionModel sm = (SessionModel)this.context.Session["EmployeeInfo"];
                if (string.IsNullOrEmpty(sm.EmployeeId) || sm.StoreNumber < 1)
                    return false;
                return true;
            }
            catch (Exception ex)
            {

                throw ex;
             }
        }
    }
}