﻿using DailyDeposit.ViewModel;
using DailyDepositLog.DAL;
using System;
using System.Configuration;
using System.Web.Mvc;
using System.Linq;
namespace DailyDepositLog.UI.Controllers
{
    public class DailyDepositLogController : Controller
    {
        Utility _utility;
        // GET: DepositLog
        public ActionResult Index()
        {
            _utility = new Utility(this.HttpContext);
            if (!_utility.IsLoggedIn())
            {
                return RedirectToAction("Login", "Login");
            }
            else
            {
                SessionModel sm = (SessionModel)Session["EmployeeInfo"];
                DailyDepositLogDAL dailyDepositLogDAL = new DailyDepositLogDAL(ConfigurationManager.ConnectionStrings["DDLConnectionString"].ConnectionString);
                MemberInfoDAL memberInfoDAL = new MemberInfoDAL(ConfigurationManager.ConnectionStrings["HTSConnectionString"].ConnectionString);
                var dailyDepositLogList = dailyDepositLogDAL.GetDailyDepositLogs(sm.StoreNumber);
                DailyDepositLogViewModel DDVM = new DailyDepositLogViewModel();
                DDVM.IncludedDays = Convert.ToInt16(ConfigurationManager.AppSettings["IncludedDays"]);
                DDVM.DailyDepositLogs = dailyDepositLogList;
                DDVM.ManagerNames = memberInfoDAL.GetMemberNames(sm.StoreNumber);
                return View(DDVM);
            }
        }
        public JsonResult GetFormData(string date)
        {
            ResponseResult result = new ResponseResult() { Status=1};
            SessionModel sm = (SessionModel)Session["EmployeeInfo"];
            DailyDepositLogDAL dailyDepositLogDAL = new DailyDepositLogDAL(ConfigurationManager.ConnectionStrings["DDLConnectionString"].ConnectionString);
            MemberInfoDAL memberInfoDAL = new MemberInfoDAL(ConfigurationManager.ConnectionStrings["HTSConnectionString"].ConnectionString);
            var dailyDepositLogList = dailyDepositLogDAL.GetDailyDepositLogs(sm.StoreNumber);
            
            var dailyDepositLog = dailyDepositLogList.Where(o => o.SaleDate == Convert.ToDateTime(date)).FirstOrDefault();
            if (dailyDepositLog != null)
            {
                result.Data = dailyDepositLog;
            }
            else {
                result.Status = 2;
            }


            return Json(result, JsonRequestBehavior.AllowGet);
        }
        // POST: DepositLog/Create
        [HttpPost]
        public ActionResult Create(DailyDepositLogCreateViewModel model)
        {
            try
            {
                _utility = new Utility(this.HttpContext);
                if (!_utility.IsLoggedIn())
                {
                    return RedirectToAction("Login", "Login");
                }
                else
                {
                    SessionModel sm = (SessionModel)Session["EmployeeInfo"];
                    TimeSpan restrictedTime = new TimeSpan(3, 0, 0);
                    if (DateTime.Now.TimeOfDay < restrictedTime)
                    {
                        model.DailyDepositLog.SaleDate = DateTime.Now.AddDays(-1);
                    }
                    else
                    {
                        model.DailyDepositLog.SaleDate = DateTime.Now;
                    }
                    DailyDepositLogDAL dailyDepositLogDAL = new DailyDepositLogDAL(ConfigurationManager.ConnectionStrings["DDLConnectionString"].ConnectionString);
                    model.DailyDepositLog.EmployeeId = sm.EmployeeId;
                    model.DailyDepositLog.StoreNumber = sm.StoreNumber;
                    model.DailyDepositLog.SaleDate = Convert.ToDateTime(Request.Form["SaleDate"]);
                    var result = dailyDepositLogDAL.AddDailyDepositLogs(model.DailyDepositLog);
                    //}
                    return RedirectToAction("Index");
                }
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("NotSaved", "Data not saved successfully");
                return View(model);
            }
        }
        // POST: DepositLog/Edit/5
        [HttpPost]
        public ActionResult Update(DailyDepositLogCreateViewModel model)
        {
            try
            {
                _utility = new Utility(this.HttpContext);
                if (!_utility.IsLoggedIn())
                {
                    return RedirectToAction("Login", "Login");
                }
                else
                {
                    SessionModel sm = (SessionModel)Session["EmployeeInfo"];
                    TimeSpan restrictedTime = new TimeSpan(3, 0, 0);
                    if (DateTime.Now.TimeOfDay < restrictedTime)
                    {
                        model.DailyDepositLog.SaleDate = DateTime.Now.AddDays(-1);
                    }
                    else
                    {
                        model.DailyDepositLog.SaleDate = DateTime.Now;
                    }
                    DailyDepositLogDAL dailyDepositLogDAL = new DailyDepositLogDAL(ConfigurationManager.ConnectionStrings["DDLConnectionString"].ConnectionString);
                    model.DailyDepositLog.EmployeeId = sm.EmployeeId;
                    model.DailyDepositLog.StoreNumber = sm.StoreNumber;
                    var result = dailyDepositLogDAL.AddDailyDepositLogs(model.DailyDepositLog);
                    //}
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }
        
    }
}
