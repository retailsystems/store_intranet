﻿using DailyDeposit.ViewModel;
using DailyDepositLog.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DailyDepositLog.UI.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult Login()
        {
            Session.Clear();
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel loginModel)
        {
            if (ModelState.IsValid)
            {

                DailyDepositLogDAL dailyDepositLogDAL = new DailyDepositLogDAL(ConfigurationManager.ConnectionStrings["HTSConnectionString"].ConnectionString);
                string store = Request.QueryString["store_num"];
                if(!string.IsNullOrEmpty(store))
                { loginModel.StoreNumber = Convert.ToInt32(store); }

                
                int Result = dailyDepositLogDAL.ValidateEmployee(loginModel);
                var msg = string.Empty;
                if (Result == 1)
                {
                    msg = "EmployeeID not found or doesn't belong to the store";
                }
                else
                {
                    
                    SessionModel sm = new SessionModel() { EmployeeId=loginModel.EmployeeID,StoreNumber=loginModel.StoreNumber};
                    Session["EmployeeInfo"] = sm;
                    return RedirectToAction("Index","DailyDepositLog");
                }

                ModelState.AddModelError("EmployeeNotFound", msg);
            }
            return View(loginModel);
        }
    }
}